$(function() {
    $('.choose-size li').bind('click', function(e) {
        $(this).parent().find('input').val($(this).find('a').attr('data-value'));
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

    $('.btn-add-to-cart').bind('click', function(e) {
        var item_id = $(this).attr('data-item-id');
        var qty = 1;
        var variantsInputs = $('.order-item-variant');
        var variants = [];
        
        for(var i = 0; i < variantsInputs.length; i++)
        {
            variants.push($(variantsInputs[i]).val());
        }
        cart_add(item_id, qty, variants, function() {
            $('#addedToCart').modal();
        });

        e.preventDefault();
    });

    $('.remove-item').on('click', function(e) {});
});

function update_cart() {
    $.ajax({
        'url': '/cart/index',
        'success': function(html) {
            $('.cart-table').replaceWith(html);
        }
    });
}

function remove_from_cart(link) {
    var item_id = $(link).attr('data-item-id');
    $.ajax({
        'url': $(link).attr('href'),
        'dataType': 'json',
        'success': function() {
            cart_refresh_widget();
            update_cart();
        }
    });

    return false;
}