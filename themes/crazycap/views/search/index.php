<?php
$this->pageTitle = Options::get('site_name') . ' - результаты поиск по запросу "' . $query->query . '"'; 
?>
<div class="row">
	<div class="col-md-12">
		<h3>Результаты поиска <small>Товаров: <?php echo count($query->searchQueryResults); ?></small></h3>
	</div>
</div>
<div class="row">
	<?php
	foreach ($query->searchQueryResults as $result) 
	{
		$item = $result->catalogItem;		
		$this->widget('ItemWidget', array('item' => $item));
	}
	?>
</div>