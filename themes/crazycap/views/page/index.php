<div class="row">
	<div class="col-md-3">
		<ul class="nav nav-pills nav-stacked">
			<?php foreach ($rootPages as $rootPage) { ?>
			<li class="disabled"><a href="#"><?php echo $rootPage->title; ?></a></li>
			<?php foreach ($rootPage->pages as $subPage) { ?>
			<li><?php echo CHtml::link($subPage->title, array('page/index', 'slug' => $subPage->slug)); ?></li>
			<?php } 
		} ?>
	</ul>
</div>
<div class="col-md-9">
	<h3><?php echo $page->title; ?></h3>
	<?php echo $page->document->html; ?>
</div>
</div>