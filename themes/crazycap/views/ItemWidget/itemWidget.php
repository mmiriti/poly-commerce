<?php
/** @var $this ItemWidget */
$category_id = 0;

if($this->category == null)
{
	if(count($this->item->catalogCategories) > 0)
	{
		$category_id = $this->item->catalogCategories[0]->id;
	}
}else{
	$category_id = $this->category->id;
}
?>
<div class="col-md-15 col-sm-3 col-xs-6 catalog-item">
	<a href="<?php echo Yii::app()->createUrl('catalog/item', array('id' => $this->item->id, 'parent' => $category_id)); ?>">
		<?php 
		if(count($this->item->galleryItems))
		{
			echo CHtml::image($this->item->galleryItems[0]->file->thumb_url, $this->item->name, array('class' => 'img-responsive'));
		}

		echo CHtml::link($this->item->name, array('catalog/item', 'id' => $this->item->id, 'parent' => $category_id)); 
		?>
	</a>
	<span class="price"><?php echo $this->item->readable_price; ?></span>
</div>