<?php
$galleryItems = array();

$structureItem = StructureItem::model()->findByAttributes(array('slug' => 'main'));
$this->pageTitle = $structureItem->name;

if ($structureItem != null) {
    foreach ($structureItem->structureBlocks as $block) {
        if ($block->type == 'gallery') {
            $galleryItems = $block->galleryItems;
        }
    }
}
?>
<div class="row hidden-xs hidden-sm">
    <div class="col-md-12">
        <div id="carousel-main" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php
                $active = true;
                $n = 0;
                foreach ($galleryItems as $item) {
                    ?>
                    <li data-target="#carousel-main" data-slide-to="<?php echo $n; ?>"
                        class="<?php if ($active) echo 'active'; ?>"></li>
                    <?php
                    $active = false;
                    $n++;
                }
                ?>
            </ol>
            <div class="carousel-inner">
                <?php
                $active = true;
                foreach ($galleryItems as $item) {
                    ?>
                    <div class="item<?php if ($active) echo ' active'; ?>">
                        <img src="<?php echo $item->file->url; ?>" alt="">
                    </div>
                    <?php
                    $active = false;
                }
                ?>
            </div>
            <a class="left carousel-control" href="#carousel-main" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-main" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
</div>
<div class="row features hidden-xs hidden-sm">
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-3"><img src="/themes/crazycap/img/delivery.png" alt=""></div>
            <div class="col-md-9">удобная доставка</div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-3"><img src="/themes/crazycap/img/self-service.png" alt=""></div>
            <div class="col-md-9">пункты самовывоза</div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-3"><img src="/themes/crazycap/img/updates.png" alt=""></div>
            <div class="col-md-9">постоянное обновление</div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-3"><img src="/themes/crazycap/img/24h.png" alt=""></div>
            <div class="col-md-9">круглосуточно работаем</div>
        </div>
    </div>
</div>
<div class="row catalog">
    <h3>Новинки</h3>

    <div class="col-md-12">
        <div class="row">
            <?php
            $featured = CatalogItem::findByParameter('featured', 1);
            foreach ($featured as $f) {
                if (count($f->catalogCategories) > 0) {
                    $this->widget('ItemWidget', array('item' => $f));
                }
            }
            ?>
        </div>
    </div>
</div>