<?php
/** @var $this Controller */
/** @var $structure StructureItem */

$this->pageTitle = $structure->name;

$rootPages = StructureItem::model()->findAllByAttributes(array('parent_id' => null));
?>
<div class="row">
    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked">
            <?php
            foreach ($rootPages as $rootPage) {
                if (count($rootPage->structureItems)) {
                    ?>
                    <li class="disabled"><a href="#"><?php echo $rootPage->name; ?></a></li>
                    <?php foreach ($rootPage->structureItems as $subPage) { ?>
                        <li><?php echo CHtml::link($subPage->name, array('site/structure', 'slug' => $subPage->slug)); ?></li>
                    <?php
                    }
                }
            }
            ?>
        </ul>
    </div>
    <div class="col-md-9">
        <?php
        foreach ($structure->structureBlocks as $block) {
            ?>
            <h3><?php echo $block->title; ?></h3>
            <?php
            switch ($block->type) {
                case 'text':
                    echo $block->data;
                    break;
            }

            ?>
        <?php
        }
        ?>
    </div>
</div>