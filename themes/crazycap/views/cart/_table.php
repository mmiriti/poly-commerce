<table class="table table-hover table-stripped cart-table">
	<thead>
		<tr>
			<th>Товар</th>
			<th></th>
			<th>Стоимость</th>
			<th>Кол-во</th>
			<th>Итого</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$total = 0;
		foreach ($items as $item_data)
		{
			$item = $item_data['item'];
			$subtotal = ($item->calc_price * intval($item_data['qty']));
			$total += $subtotal;
			?>
			<tr id="cart-item-row-<?php echo $item->id; ?>">					
				<td><?php echo CHtml::link(CHtml::image($item->galleryItems[0]->file->thumb_url, '', array('height' => '80')), array('catalog/item', 'id' => $item->id, 'parent' => $item->catalogCategories[0]->id)); ?></td>
				<td>
                    <p><strong><?php echo $item->name; ?></strong></p>
                    <?php
                    foreach($item_data['variants'] as $variant){
                        ?><p><?php echo $variant->option->name; ?>: <?php echo $variant->string; ?></p><?php
                    }
                    ?>
                </td>
				<td><?php echo $item->getReadable_price(); ?></td>
				<td>
					<table>
						<tr>
							<td><?php echo $item_data['qty']; ?></td>
							<td>
								<span class="cart-item-inc-dec">
									<a href="#" title="Добавить" onclick="cart_inc(<?php echo $item->id; ?>, 1, function() { update_cart(); cart_refresh_widget(); }); return false;"><span class="glyphicon glyphicon-chevron-up"></span></a>
									<a href="#" title="Убрать" onclick="cart_dec(<?php echo $item->id; ?>, 1, function() { update_cart(); cart_refresh_widget(); }); return false;"><span class="glyphicon glyphicon-chevron-down"></span></a>
								</span>
							</td>
						</tr>
					</table>
				</td>
				<td><?php echo number_format($subtotal) . ($item->currency == null ? '' : ' ' . $item->currency->sign); ?></td>
				<td>
					<?php
					echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('remove', 'item_id' => $item->id), array('class' => 'remove-item', 'title' => 'Убрать из корзины', 'data-item-id' => $item->id, 'onclick' => 'return remove_from_cart(this);'));
					?>
				</td>
			</tr>
			<?php
		}
		?>							
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="purple"><strong class="pull-right">Итого: </strong></td>
			<td class="purple"><?php echo number_format($total); ?> руб.</td>
			<td></td>
		</tr>
	</tbody>
</table>