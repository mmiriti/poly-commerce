<div class="row">
	<div class="col-md-12">
		<h3>Корзина</h3>
	</div>
</div>
<?php echo CHtml::beginForm(array('cart/checkout'), 'post', array('class' => 'order-form')); ?>
	<div class="row">
		<div class="col-md-2">
			Телефон
		</div>
		<div class="col-md-6">
			<input type="text" class="form-control" name="CustomerData[phone]" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			Имя
		</div>
		<div class="col-md-6">
			<input type="text" class="form-control" name="CustomerData[name]" required>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			Электронная почта
		</div>
		<div class="col-md-6">
			<input type="email" class="form-control" name="User[email]" required>
		</div>
		<div class="col-md-4">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="CustomerData[subscribe]"> Подписаться на новости и скидки
				</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">Комментарий</div>
		<div class="col-md-10">
			<textarea name="OrderData[comment]" id="" cols="30" rows="4" class="form-control"></textarea>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-md-offset-2">
			<button class="btn btn-warning" type="submit">ОТПРАВИТЬ ЗАКАЗ</button>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<?php $this->renderPartial('_table', array('items' => $items)); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?php echo CHtml::link('&lt; Вернуться в каталог', array('catalog/index')); ?>
		</div>
		<div class="col-md-6">
			<button class="btn btn-warning pull-right">Отправить заказ</button>
		</div>
	</div>
</form>