<?php
/** @var $this CatalogController */
/** @var $item CatalogItem */
/** @var $category CatalogCategory */

$this->pageTitle = 'Каталог - ' . $category->name . ' - ' . $item->name;
?>
<script>
    $(function () {
        $('.item-gallery-item').bind('click', function () {
            var imageID = $(this).attr('data-image-id');
            $('.gallery-big img').hide();
            $('.gallery-big img#item-image-big-' + imageID).show();
        });

        $('.item-image-big').elevateZoom({scrollZoom: true});
    });
</script>
<div class="row">
    <div class="col-md-12">
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => $this->breadcrumbs,
                'separator' => ' > '
            )
        );
        ?>
    </div>
</div>

<style>
    .gallery-big img {
        display: none;
    }

    .gallery-big img:first-child {
        display: block;
    }
</style>
<div class="row">
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12 gallery-big">
                <?php
                foreach ($item->galleryItems as $galleryItem) {
                    echo CHtml::image($galleryItem->file->thumb_url, '', array('class' => 'img-responsive thumbnail item-image-big', 'id' => 'item-image-big-' . $galleryItem->id, 'data-zoom-image' => $galleryItem->file->url));
                }
                ?>
            </div>
        </div>
        <div class="row">
            <?php
            foreach ($item->galleryItems as $galleryItem) {
                ?>
                <div class="col-md-4 col-xs-4">
                    <?php echo CHtml::image($galleryItem->file->thumb_url, '', array('class' => 'img-responsive thumbnail item-gallery-item', 'data-image-id' => $galleryItem->id)); ?>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <div class="col-md-9 item-full-description">
        <div class="row">
            <div class="col-md-12">
                <h3><?php echo $item->name; ?> - <span><?php echo $item->readable_price; ?></span></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                foreach ($item->group->parameters as $param) {
                    if ($param->slug != 'featured') {
                        ?>
                        <p><?php echo $param->name; ?>: <?php echo $item->parameterValue($param->id); ?></p>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
        <?php foreach ($item->group->options as $option) { ?>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $option->name; ?>
                </div>
            </div>
            <div class="row size-add-to-cart">
                <div class="col-md-5">
                    <ul class="nav nav-pills choose-size">
                        <?php
                        $first = true;
                        foreach ($option->itemOptionVariants as $variant) {
                            if ($item->isVariantAvailable($variant->id)) {
                                if($first)
                                {
                                    ?>
                                    <input type="hidden" class="order-item-variant" value="<?php echo $variant->id; ?>">
                                    <?php
                                }
                                ?>
                                <li<?php if($first) { $first = false; echo ' class="active"'; } ?>><a href="#"
                                       data-value="<?php echo $variant->id; ?>"><?php echo $variant->string; ?></a>
                                </li>
                            <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="col-md-7">
                    <a href="#" class="btn btn-warning btn-add-to-cart" data-item-id="<?php echo $item->id; ?>">ДОБАВИТЬ
                        В КОРЗИНУ</a>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <?php if ($item->document != null) echo $item->document->html; ?>
            </div>
        </div>
    </div>
</div>