<?php
$this->pageTitle = 'Каталог - ' . $category->name;
?>
<div class="row">
	<div class="col-md-12">
		<?php
		$this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'separator' => ' > '
			)
		);
		?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<h3><?php echo $category->name; ?> <small>Товаров: <?php echo count($items); ?></small></h3>
	</div>
</div>
<div class="row">
	<?php
	foreach ($items as $item) 
	{
		$this->widget('ItemWidget', array('item' => $item, 'category' => $category->isNewRecord ? null : $category));
	}
	?>
</div>