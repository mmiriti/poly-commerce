<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="639d52d9e44ba80c">
    <title><?php echo Options::get('site_name') . ' - ' . $this->pageTitle; ?></title>
    <link rel="stylesheet" href="/themes/crazycap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/themes/crazycap/css/main.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="/themes/crazycap/js/jquery-1.11.0.min.js"></script>
    <script src="/themes/crazycap/js/bootstrap.min.js"></script>
    <script src="/js/cart.js"></script>
    <script src="/themes/crazycap/js/general.js"></script>
    <script type="text/javascript" src="/themes/crazycap/js/jquery.elevateZoom-3.0.8.min.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52649412-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter25511975 = new Ya.Metrika({id:25511975,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/25511975" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>
<div class="modal fade" id="addedToCart" tabindex="-1" role="dialog" aria-labelledby="addedToCartLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="addedToCartLabel">Добавлено!</h4>
            </div>
            <div class="modal-body">
                Товар добавлен в <?php echo CHtml::link('корзину', array('cart/index')); ?>!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="loginModalLabel">Войти</h4>
            </div>
            <div class="modal-body">
                <form action="">
                    <?php
                    $newUser = new User('login');
                    $this->widget('FormGroup', array('model' => $newUser, 'fieldName' => 'email'));
                    $this->widget('FormGroup', array('model' => $newUser, 'fieldName' => 'password'));
                    ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">ВОЙТИ</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="callbackModal" tabindex="-1" role="dialog" aria-labelledby="callbackModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            echo CHtml::beginForm(array('form/post'));
            ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="callbackModalLabel">Заказать звонок</h4>
            </div>
            <div class="modal-body">
                <?php
                $form = Form::model()->findByAttributes(array('slug' => 'callbacks'));
                if ($form != null) {
                    foreach ($form->formFields as $field) {
                        ?>
                        <div class="form-group">
                            <label for=""><?php echo $field->name; ?></label>
                            <?php echo CHtml::textField('FormField[' . $form->slug . '][' . $field->slug . ']', '', array('class' => 'form-control')); ?>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Заказать</button>
            </div>
            <?php
            echo CHtml::endForm();
            ?>
        </div>
    </div>
</div>

<div class="hipsters visible-lg">
    <img src="/themes/crazycap/img/left.png" alt="" id="hipsterLeft">
    <img src="/themes/crazycap/img/right.png" alt="" id="hipsterRight">
</div>

<div class="container">
    <header>
        <div class="row">
            <div class="col-md-3">
                <a href="/">
                    <img src="/themes/crazycap/img/logo.png" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="col-md-12 phone">
                        <div class="row">
                            <div class="col-md-12">
                                <i class="fa fa-phone"></i>
                                <span><?php echo Options::get('phone_main', '+7 (912) 123-45-67', 'Номер телефона в шапке'); ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="#callbackModal" data-toggle="modal" data-target="#callbackModal">Заказать
                                    звонок</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php echo CHtml::beginForm(array('search/index'), 'get'); ?>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Поиск товара" name="q"
                                   value="<?php echo Yii::app()->request->getParam('q', ''); ?>">
								<span class="input-group-btn">
									<button class="btn btn-default search-button" type="submit">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
								</span>
                        </div>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-md-offset-1">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        if (0) {
                            ?>
                            <button class="btn btn-warning btn-enter" data-toggle="modal" data-target="#loginModal">
                                ВОЙТИ
                            </button>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <?php $this->widget('CartWidget'); ?>
            </div>

        </div>
    </header>
    <div class="row">
        <div class="col-md-12 catalog-list">
            <?php
            $rootCategories = CatalogCategory::model()->findAllByAttributes(array('parent_id' => null));

            $items = array();

            foreach ($rootCategories as $cat) {
                echo CHtml::link(str_replace(" ", "&nbsp;", $cat->name), array('catalog/index', 'category_id' => $cat->id)) . ' ';
            }
            ?>
        </div>
    </div>
    <?php echo $content; ?>
</div>
<footer>
    <div class="jumbotron footer">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-xs-6">
                    <div class="row">
                        <div class="col-md-3 social-buttons">
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <a href="<?php echo Options::get('footer_link_soc_vk', '#', 'Ссылка на страницу ВКонтакте'); ?>"><img
                                            src="/themes/crazycap/img/soc-vk.png" alt=""></a>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <a href="<?php echo Options::get('footer_link_soc_ok', '#', 'Ссылка на страницу в Одноклассники'); ?>"><img
                                            src="/themes/crazycap/img/soc-ok.png" alt=""></a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <a href="<?php echo Options::get('footer_link_soc_fb', '#', 'Ссылка на страницу в Facebook'); ?>"><img
                                            src="/themes/crazycap/img/soc-fb.png" alt=""></a>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <a href="<?php echo Options::get('footer_link_soc_tw', '#', 'Ссылка на страницу в Twitter'); ?>"><img
                                            src="/themes/crazycap/img/soc-tw.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>Подписка</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    Вы можете подписаться на последние обновления, различные акции и скидки
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Введите ваш E-mail">
												<span class="input-group-btn">
													<button class="btn btn-default" type="button">&gt;</button>
												</span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-xs-6">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <strong>Каталог товаров</strong>
                            <?php
                            $rootCategories = CatalogCategory::model()->findAllByAttributes(array('parent_id' => null));

                            $items = array();

                            foreach ($rootCategories as $cat) {
                                $items[] = array('label' => $cat->name, 'url' => array('catalog/index', 'category_id' => $cat->id));
                            }

                            $this->widget('zii.widgets.CMenu', array(
                                    'items' => $items,
                                )
                            );
                            ?>
                        </div>
                        <?php
                        foreach (StructureItem::model()->findAllByAttributes(array('parent_id' => null)) as $rootPage) {
                            /** @var $rootPage StructureItem */
                            if (count($rootPage->structureItems) > 0) {
                                ?>
                                <div class="col-md-4 col-xs-12">
                                    <strong><?php echo $rootPage->name; ?></strong>
                                    <ul>
                                        <?php foreach ($rootPage->structureItems as $subPage) { ?>
                                            <li><?php echo CHtml::link($subPage->name, array('site/structure', 'slug' => $subPage->slug)); ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Copyright &copy; 2014 CrazyCap | <?php if(($content = @file_get_contents('http://miriti.github.io/sign.inc.html')) !== FALSE) echo $content; ?> <br>
                    Интернет-магазин прикольных шляп и кепок. Все права защищены.
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>