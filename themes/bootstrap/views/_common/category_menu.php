<?php
$rootCategories = CatalogCategory::model()->findAllByAttributes(array('parent_id' => null));

$items = array();

foreach ($rootCategories as $cat) 
{
	$items[] = array('label' => $cat->name, 'url' => array('catalog/index', 'category_id' => $cat->id));	
}

$this->widget('zii.widgets.CMenu', array(
	'items'=>$items,
	'htmlOptions' => array('class' => 'nav nav-pills nav-justified')
	)
);
?>