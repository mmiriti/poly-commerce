<!doctype html>
<html>
<head>
	<meta charset="utf-8"> 
	<title><?php echo $this->pageTitle; ?></title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<?php echo $content; ?>
		<hr>
		<footer>
			<?php echo CHtml::link('Poly Commerce', 'http://poly-commerce.ru/'); ?>
		</footer>
	</div>	
</body>
</html>