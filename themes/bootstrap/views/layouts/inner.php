<!doctype html>
<html>
<head>
	<meta charset="utf-8"> 
	<title><?php echo $this->pageTitle; ?></title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-inverse" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><?php echo Options::get('site_name'); ?></a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php $this->widget('zii.widgets.CMenu', array(
						'items' => $this->menu,
						'htmlOptions' => array('class' => 'nav navbar-nav')
					)
					); ?>
				</div>
			</div>
		</nav>
		<div class="jumbotron">
			<h1><?php echo Options::get('site_name'); ?></h1>
		</div>
		<?php $this->renderPartial('//_common/category_menu'); ?>
		<hr>
		<?php echo $content; ?>
		<hr>
		<footer>
			<?php echo CHtml::link('Poly Ploy', 'http://polyploy.com/'); ?>
		</footer>
	</div>	
</body>
</html>