<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
			<?php

			foreach ($category->breadcrumbs() as $b) 
			{
				?>
				<li><?php echo CHtml::link($b->name, array('catalog/index', 'category_id' => $b->id)); ?></li>
				<?php
			}
			?>
		</ol>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-md-3">
		<ul class="nav nav-pills nav-stacked">
			<?php
			foreach ($category->catalogCategories as $sub_category) 
			{
				?>
				<li><?php echo CHtml::link($sub_category->name . '<span class="badge pull-right">' . count($sub_category->items) . '</span>', array('catalog/index', 'category_id' => $sub_category->id)); ?></li>
				<?php
			}
			?>		
		</ul>
	</div>
	<div class="col-md-9">
		<div class="row">
			<?php
			if(count($category->items) > 0)
			{
				foreach ($category->items as $item) 
				{
					?>
					<div class="col-md-4">
						<div class="thumbnail">
							<?php echo CHtml::link(CHtml::image($item->galleryItems[0]->file->url, ''), array('catalog/item', 'id' => $item->id, 'parent' => $category->id)); ?>
							<p><?php echo $item->name; ?></p>
						</div>
					</div>
					<?php
				}
			}else{
				?>
				<h2>Нет товаров :(</h2>
				<?php	
			}
			?>
		</div>
	</div>
</div>