<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
			<?php
			foreach ($category->breadcrumbs() as $b) 
			{
				?>
				<li><?php echo CHtml::link($b->name, array('catalog/index', 'category_id' => $b->id)); ?></li>
				<?php
			}
			?>
			<li><?php echo $item->name; ?></li>
		</ol>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<h2><?php echo $item->name; ?></h2>
	</div>
</div>