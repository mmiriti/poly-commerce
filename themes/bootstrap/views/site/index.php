<div class="jumbotron">
    <h1><i><?php echo Options::get('site_name'); ?></i></h1>

    <p>Добро пожаловать в наш магазин!</p>

    <p>Для доступа в систему управления добавьте <code>/ctrl</code> к адресу сайта. Например вот
        так: <?php echo CHtml::link('http://' . $_SERVER['HTTP_HOST'] . '/ctrl', 'http://' . $_SERVER['HTTP_HOST'] . '/ctrl'); ?>
    </p>

    <p>Используйте следующие данные для доступа в систему управления:</p>

    <p>
    <dl>
        <dt>Имя пользователя</dt>
        <dd>admin@domain.com</dd>
        <dt>Пароль</dt>
        <dd>adminpassword</dd>
    </dl>
    </p>
</div>
<?php $this->renderPartial('//_common/category_menu'); ?>