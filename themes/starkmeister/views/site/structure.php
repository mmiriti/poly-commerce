<?php
/** @var $structure StructureItem */

$this->pageTitle = $structure->name;

foreach ($structure->structureBlocks as $block) {
    ?>
    <div class="block-title"><?php echo $block->title; ?></div>
    <div class="group">
        <?php
        switch ($block->type) {
            case 'text':
                echo $block->data;
                break;

            case 'news':
                ?>
                <ul class="last-news">
                    <?php
                    $limit = isset($block->udata['news_count']) ? intval($block->udata['news_count']) : 10;
                    $news = NewsItem::model()->findAll(array('limit' => $limit, 'order' => 'publish_date_time desc'));

                    foreach ($news as $item) {
                        ?>
                        <li>
                            <div
                                class="date"><?php echo Yii::app()->dateFormatter->format('d MMM y г.', strtotime($item->publish_date_time)); ?></div>
                            <?php echo CHtml::link($item->title, array('news/read', 'id' => $item->id), array('class' => 'title')); ?>
                        </li>
                    <?php
                    }

                    ?>
                </ul>
                <?php

                break;

            case 'gallery':
                ?>
                <div class="row">
                    <?php
                    foreach ($block->galleryItems as $galleryItem) {
                        /* @var $galleryItem StructureBlockGalleryItem */
                        ?>
                        <div class="col-md-4">
                            <?php echo CHtml::link(CHtml::image($galleryItem->file->url, '', array('class' => 'thumbnail')), $galleryItem->file->url); ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                break;

        }
        ?>
    </div>
<?php
}