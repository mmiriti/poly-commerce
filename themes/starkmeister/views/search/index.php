<?php
/* @var $this SearchController */
/* @var $query SearchQuery */
/* @var $items CActiveDataProvider */
/* @var $page int */
/* @var $itemsPerPage int */

$this->pageTitle = 'результаты поиска по запросу &laquo;' . $query->query . '&raquo;';
?>
<div class="col-lg-12">
    <h1>Результаты поиска по запросу &laquo;<?php echo $query->query; ?>&raquo;</h1>

    <div class="catalog-container">
        <?php
        if ($items->itemCount > 0) {
            echo $this->renderPartial('/catalog/_table', array('items' => $items, 'action' => array('search/index', 'query_id' => $query->id), 'page' => $page, 'category_id' => null, 'itemsPerPage' => $itemsPerPage));
        } else {
            ?>
            <div class="alert alert-danger">
                Ничего не найдено по запросу &laquo;<?php echo $query->query; ?>&raquo;
            </div>
        <?php
        }
        ?>
    </div>
</div>