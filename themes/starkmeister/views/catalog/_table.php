<?php
/* @var $this Controller */
/* @var $action array */
/* @var $page int */
/* @var $items CActiveDataProvider */
?>
<script>
    $(function () {
        $('.addtocart').bind('click', function (e) {
            e.preventDefault();
            var itemID = $(this).attr('data-item-id');
            var qty = $('#quantity-' + itemID).val();

            cart_add(itemID, qty, null);
            $('#addToCartModal').modal();
        });
    });
</script>
<!-- Modal -->
<div class="modal fade" id="addToCartModal" tabindex="-1" role="dialog" aria-labelledby="addToCartModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Закрыть</span></button>
                <h4 class="modal-title" id="addToCartModalLabel">Добавлено</h4>
            </div>
            <div class="modal-body">
                Товар добавлен в вашу
                в <?php echo CHtml::link('<span class="glyphicon glyphicon-shopping-cart"></span> Корзину', array('cart/index')); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="catalog-container">
    <?php
    if (Yii::app()->user->isGuest) {
        ?>
        <div class="alert alert-info">
            Для осуществления заказа <?php echo CHtml::link('зарегистрируйтесь', array('/user/registration/index')); ?>
            и/или <?php echo CHtml::link('авторизируйтесь', array('/user/auth/login')); ?>
        </div>
    <?php
    }
    ?>
    <table class="catalog">
        <thead>
        <tr>
            <th>Уникальный № производителя</th>
            <th>Артикул OEM</th>
            <th width="40px">Фото</th>
            <th>Наименование</th>
            <th>Производитель</th>
            <th width="80px">Цена, руб.</th>
            <th>Наличие</th>
            <th>Доставка</th>
            <?php
            if (!Yii::app()->user->isGuest) {
                ?>
                <th width="80px">Купить шт.</th>
                <th><!-- <a href="#" class="addtocartall"></a> --></th>
            <?php
            }
            ?>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($items->data as $item) {
            /* @var $item CatalogItem */
            ?>
            <tr>
                <td><?php echo CHtml::link($item->getAttribute('supplier_code'), array('catalog/item', 'id' => $item->id)) ?></td>
                <td><?php echo CHtml::link($item->getAttribute('oem_code'), array('catalog/item', 'id' => $item->id)) ?></td>
                <td><?php
                    if (count($item->galleryItems) > 0) {
                        echo CHtml::image($item->galleryItems[0]->file->thumb_url, $item->name, array('height' => '20'));
                    }
                    ?></td>
                <td><?php echo $item->name; ?>
                </td>
                <td><?php
                    if (count($item->catalogCategories) > 0) {
                        echo CHtml::link($item->catalogCategories[0]->name, array('catalog/index', 'category_id' => $item->catalogCategories[0]->id));
                    }
                    ?></td>
                <td><?php
                    if (($item->price == null) || ($item->price->value == 0)) {
                        echo 'не указана';
                    } else {
                        echo number_format($item->readPrice('RUR'), 2, ',', '');
                    }
                    ?></td>
                <td><?php
                    $count = intval($item->parameterValue('count_warehouse'));
                    if ($count == 0) {
                        echo 'нет в наличии';
                    } else {
                        if ($count <= 10) {
                            echo $count;
                        } else {
                            echo '>10';
                        }
                    }
                    ?></td>
                <td></td>
                <?php
                if (!Yii::app()->user->isGuest) {
                    ?>
                    <td>
                        <button class="qty-minus">-</button>
                        <input type="text" class="qty" value="1" id="quantity-<?php echo $item->id; ?>">
                        <button class="qty-plus">+</button>
                        <div class="clear"></div>
                    </td>
                    <td><a href="#" class="addtocart" data-item-id="<?php echo $item->id; ?>"
                           title="Добавить в корзину"></a>
                    </td>
                <?php
                }
                ?>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>


</div>

<div class="paging">
    <ul class="pages-list">
        <li>Страница:</li>
        <?php
        if ($items->pagination->currentPage > 0) {
            echo "<li>" . CHtml::link('<<', $action + array('category_id' => $category_id, 'page' => ($items->pagination->currentPage - 1), 'itemsPerPage' => $itemsPerPage)) . "</li>";
        }

        $start_page = $items->pagination->currentPage - 2;

        if ($start_page < 0) {
            $start_page = 0;
        }

        if ($start_page > 0) {
            echo "<li>" . CHtml::link("1", $action + array('category_id' => $category_id, 'page' => 0, 'itemsPerPage' => $itemsPerPage)) . "</li>";
            echo "<li>...</li>";
        }

        for ($p = $start_page; $p < $start_page + 5; $p++) {
            if ($p < $items->pagination->pageCount) {
                if ($p != $items->pagination->currentPage) {
                    echo "<li>" . CHtml::link(($p + 1), $action + array('category_id' => $category_id, 'page' => $p, 'itemsPerPage' => $itemsPerPage)) . "</li>";
                } else {
                    echo "<li>" . ($p + 1) . "</li>";
                }
            } else {
                break;
            }
        }

        if ($p < $items->pagination->pageCount - 1) {
            echo "<li>...</li>";
            echo "<li>" . CHtml::link($items->pagination->pageCount, $action + array('category_id' => $category_id, 'page' => $items->pagination->pageCount - 1, 'itemsPerPage' => $itemsPerPage)) . "</li>";
        }

        if (($items->pagination->currentPage + 1) < $items->pagination->pageCount) {
            echo "<li>" . CHtml::link('>>', $action + array('category_id' => $category_id, 'page' => ($items->pagination->currentPage + 1), 'itemsPerPage' => $itemsPerPage)) . "</li>";
        }
        ?>
    </ul>

    <ul class="sort">
        <li>Показывать по:</li>
        <?php
        $cnts = array(10, 25, 100);

        foreach ($cnts as $c) {
            ?>
            <li><?php
                if ($c == $itemsPerPage) {
                    echo $c;
                } else {
                    echo CHtml::link($c, $action + array('category_id' => $category_id, 'page' => $page, 'itemsPerPage' => $c));
                }
                ?></li>
        <?php
        }
        ?>
    </ul>
    <div class="clear"></div>