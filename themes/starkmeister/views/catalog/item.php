<?php
/* @var $this CatalogController */
/* @var $item CatalogItem */
/* @var $category CatalogCategory */

$this->pageTitle = 'Каталог - ' . $item->name;
?>
<div class="row">
    <div class="col-lg-12">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li><?php echo CHtml::link('Каталог', array('catalog/index')); ?></li>
            <li><?php echo $item->name; ?></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-12"><h1><?php echo $item->name; ?></h1></div>
</div>
<div class="row">
    <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 product-gallery">
        <?php
        if (count($item->galleryItems) > 0) {
            ?>
            <div class="image" id="largeimage"><img src="img/gallery-example-1.png" alt="" class="img-responsive"></div>
            <ul class="thumbs">
                <li><img src="img/gallery-example-1.png" alt="" class="img-responsive"></li>
                <li><img src="img/gallery-example-2.png" alt="" class="img-responsive"></li>
                <li><img src="img/gallery-example-1.png" alt="" class="img-responsive"></li>
                <li><img src="img/gallery-example-2.png" alt="" class="img-responsive"></li>
            </ul>
        <?php
        } else {
            ?>
            <div class="image" id="largeimage">
                <p>Нет изображения</p>
            </div>
        <?php
        }
        ?>
    </div>
    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 product-info">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">Артикул:</div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><?php echo $item->getAttribute('supplier_code'); ?></div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">Артикул ОЕМ:</div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><?php echo $item->getAttribute('oem_code'); ?></div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">Наименование:</div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><?php echo $item->name; ?></div>
        </div>
        <?php
        if (count($item->catalogCategories) > 0) {
            ?>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">Производитель:</div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><a
                        href="#"><?php echo $item->catalogCategories[0]->name; ?></a></div>
            </div>
        <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">Наличие:</div>
            <div
                class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><?php echo number_format($item->parameterValue('count_warehouse')); ?>
                шт.
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">Цена за штуку:</div>
            <div
                class="col-lg-8 col-md-8 col-sm-8 col-xs-6"><?php echo number_format($item->readPrice('RUR'), 2, ',', '\''); ?>
                руб.
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">Заказать:</div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                <button class="qty-minus">-</button>
                <input type="text" class="qty" value="1">
                <button class="qty-plus">+</button>
                <div class="clear"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12"><input type="submit" value="Купить" class="order"></div>
        </div>
    </div>
</div>

<!--
<div class="row">
    <div class="col-lg-12">
        <h2>Аналоги для «<?php echo $item->name; ?>»</h2>

        <div class="catalog-container">
            <table class="catalog">
                <thead>
                <tr>
                    <th>Уникальный № производителя</th>
                    <th>Артикул OEM</th>
                    <th width="40px">Фото</th>
                    <th>Наименование</th>
                    <th>Производитель</th>
                    <th width="80px">Цена, руб.</th>
                    <th>Наличие</th>
                    <th>Доставка</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><a href="">SM11.3830</a></td>
                    <td><a href="#">4410701133</a></td>
                    <td><img src="img/item-example-small.png" alt=""></td>
                    <td>Комплект топливопроводов правый MB SK/MK/NG-Series Eng.OM401LA Euro I,OM441A+LA, (motor
                        521630-)
                    </td>
                    <td><a href="#">Stark Meister</a></td>
                    <td>833,23</td>
                    <td>19 шт.</td>
                    <td>1 дн.</td>

                </tr>
                <tr>
                    <td><a href="">SM11.3830</a></td>
                    <td><a href="#">4410701133</a></td>
                    <td><img src="img/item-example-small.png" alt=""></td>
                    <td>Комплект топливопроводов правый MB SK/MK/NG-Series Eng.OM401LA Euro I,OM441A+LA, (motor
                        521630-)
                    </td>
                    <td><a href="#">Stark Meister</a></td>
                    <td>833,23</td>
                    <td>19 шт.</td>
                    <td>1 дн.</td>

                </tr>
                <tr>
                    <td><a href="">SM11.3830</a></td>
                    <td><a href="#">4410701133</a></td>
                    <td><img src="img/item-example-small.png" alt=""></td>
                    <td>Комплект топливопроводов правый MB SK/MK/NG-Series Eng.OM401LA Euro I,OM441A+LA, (motor
                        521630-)
                    </td>
                    <td><a href="#">Stark Meister</a></td>
                    <td>833,23</td>
                    <td>19 шт.</td>
                    <td>1 дн.</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="sum">Показано <span>7</span> из <span>7</span></div>
    </div>
</div>
-->