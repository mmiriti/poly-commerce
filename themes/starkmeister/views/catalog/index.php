<?php
/* @var $category_id integer */
/* @var $this CatalogController */
/* @var $items CActiveDataProvider */
/* @var $category CatalogCategory */
/* @var $page integer */
/* @var $itemsPerPage integer */

$this->pageTitle = 'Каталог';
?>
<div class="row">
    <div class="col-lg-12">
        <ul class="breadcrumbs">

        </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h1>Каталог</h1>
        <?php
        $category_id = $category == null ? null : $category->id;
        $this->renderPartial('_table', array('items' => $items, 'category_id' => $category_id, 'action' => array('catalog/index'), 'page' => $page, 'itemsPerPage' => $itemsPerPage));
        ?>
    </div>
</div>
</div>