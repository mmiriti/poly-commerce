<?php
/* @var $this CartController */
/* @var $items array[] */
?>
<script>
    $(function () {
        $.each($('.qty-control'), function (i, obj) {
            var itemID = $(obj).attr('data-item-id');

            var qtyInput = $(obj).find('input.qty');

            $(qtyInput).bind('change', function (e) {
                $.ajax({
                    'url': '<?php echo Yii::app()->createUrl('cart/setQty') ?>?item_id=' + itemID + '&qty=' + $(this).val()
                });
            });

            $(obj).find('.qty-minus').bind('click', function (e) {
                e.preventDefault();

                if ($(qtyInput).val() > 1) {
                    var qty = parseInt($(qtyInput).val()) - 1;
                    $(qtyInput).val(qty);

                    $.ajax({
                        'url': '<?php echo Yii::app()->createUrl('cart/dec') ?>?item_id=' + itemID
                    });
                }
            });

            $(obj).find('.qty-plus').bind('click', function (e) {
                e.preventDefault();

                var qty = parseInt($(qtyInput).val()) + 1;
                $(qtyInput).val(qty);

                $.ajax({
                    'url': '<?php echo Yii::app()->createUrl('cart/inc') ?>?item_id=' + itemID
                });
            });
        });
    });
</script>
<table class="catalog">
    <thead>
    <tr>
        <th>Уникальный № производителя</th>
        <th>Артикул OEM</th>
        <th width="40px">Фото</th>
        <th>Наименование</th>
        <th>Производитель</th>
        <th width="80px">Цена, руб.</th>
        <th>Наличие</th>
        <th>Доставка</th>
        <th width="80px">Купить шт.</th>
        <td></td>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($items as $item) {
        ?>
        <tr id="item-<?php echo $item['item']->id; ?>">
            <td><a href="#"><?php echo $item['item']->parameterValue('supplier_code'); ?></a></td>
            <td><a href="#"><?php echo $item['item']->parameterValue('oem_code'); ?></a></td>
            <td><?php
                if (count($item['item']->galleryItems) > 0) {
                    echo CHtml::image($item['item']->galleryItems[0]->file->thumb_url, $item['item']->name, array('height' => '20'));
                }
                ?></td>
            <td><?php echo $item['item']->name; ?></td>
            <td><a href="#"><?php echo $item['item']->parameterValue('brand'); ?></a></td>
            <td><?php echo number_format(floatval($item['item']->readPrice('RUR')) * $item['qty']); ?> руб.</td>
            <td><?php echo $item['qty']; ?> шт.</td>
            <td>1 дн.</td>
            <td class="qty-control" data-item-id="<?php echo $item['item']->id; ?>">
                <button class="qty-minus">-</button>
                <input type="text" class="qty" value="<?php echo $item['qty']; ?>">
                <button class="qty-plus">+</button>
                <div class="clear"></div>
            </td>
            <td><?php echo CHtml::link('', array('remove', 'item_id' => $item['item']->id), array('class' => 'delete')); ?></td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>