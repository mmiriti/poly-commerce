<?php
/* @var $this CartController */
/* @var $items CatalogItem[] */

$this->pageTitle = 'корзина';
?>
<div class="row">
    <div class="col-lg-12">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li>Корзина товаров</li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h1>Корзина товаров</h1>

        <div class="catalog-container">
            <?php $this->renderPartial('_table', array('items' => $items)); ?>
        </div>

        <div class="sum">Всего к оплате: <span><?php
                $result = 0.0;
                foreach ($items as $i) {
                    $result += floatval($i['item']->readPrice('RUR')) * $i['qty'];
                }

                echo number_format($result);
                ?> руб.</span></div>
        <?php
        if (count($items) > 0) {
            echo CHtml::beginForm(array('checkout'));
            ?>
            <div class="button">
                <button class="order" onclick="window.location.reload();">Пересчитать</button>
                <input type="submit" value="Оформить заказ" class="order">
            </div>
            <?php
            echo CHtml::endForm();
        }
        ?>
    </div>
</div>