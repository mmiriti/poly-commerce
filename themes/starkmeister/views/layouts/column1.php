<?php
/** @var $this Controller */
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="mailru-verification" content="5e1d0ab15f6b6274">
    <title><?php echo Options::get('site_name') . ' - ' . $this->pageTitle; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/themes/starkmeister/css/bootstrap.css" rel="stylesheet">
    <link href="/themes/starkmeister/css/style.css" rel="stylesheet">
    <link href="/themes/starkmeister/css/responsive.css" rel="stylesheet">

    <script src="/themes/starkmeister/js/jquery-2.1.1.min.js"></script>
    <script src="/themes/starkmeister/js/bootstrap.min.js"></script>
    <script src="/js/cart.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <?php if ($_SERVER['HTTP_HOST'] != 'starkmeister.localhost') { ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-7378130-24', 'auto');
            ga('send', 'pageview');
        </script>
    <?php } ?>
</head>

<body>
<div class="container">
    <!-- TOP -->
    <div class="row">
        <div class="col-lg-12">
            <div class="top">
                <a href="/" class="logo"></a>
            </div>
        </div>
    </div>

    <!-- NAV -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <nav>
                <?php
                $menuItems = Menu::getItems('main_menu');

                $menuWidgetItems = array();
                foreach ($menuItems as $item) {
                    /** @var $item MenuItem */
                    $menuWidgetItems[] = array('url' => $item->route, 'label' => $item->title);
                }

                $this->widget('zii.widgets.CMenu', array(
                    'items' => $menuWidgetItems
                ));

                ?>
                <div class="search">
                    <?php echo CHtml::beginForm(array('/search/index'), 'get'); ?>
                    <input type="text" placeholder="Поиск по сайту" name="q">
                    <input type="submit" value=""/>

                    <div class="clear"></div>
                    <?php echo CHtml::endForm(); ?>
                </div>
            </nav>
        </div>
    </div>
    <?php
    if (isset($this->commonBlocks['main-slider'])) {
        $galleryItems = $this->commonBlocks['main-slider']->galleryItems;
        ?>
        <!-- MAIN SLIDER -->
        <div class="row hidden-xs hidden-sm">
            <div class="col-lg-12">
                <div class="group">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            $active = true;
                            $n = 0;
                            foreach ($galleryItems as $galleryItem) {
                                /** @var $galleryItem StructureBlockGalleryItem */
                                echo CHtml::tag('li', array('data-target' => '#carousel-example-generic', 'data-slide-to' => $n++, 'class' => ($active ? 'active' : '')));
                                $active = false;
                            }
                            ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php
                            $active = true;
                            foreach ($galleryItems as $galleryItem) {
                                /** @var $galleryItem StructureBlockGalleryItem */
                                ?>
                                <div class="item<?php if ($active) echo ' active'; ?>">
                                    <img src="<?php echo $galleryItem->file->url; ?>" alt="">

                                    <div class="carousel-caption">
                                    </div>
                                </div>
                                <?php
                                $active = false;
                            }
                            ?>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button"
                           data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
    ?>

    <!-- CONTENT -->
    <div class="row content">
        <div class="col-lg-12">

            <div class="row">
                <div class="col-md-12">
                    <?php echo $content; ?>
                </div>
            </div>

        </div>
    </div>

    <!-- FOOTER -->
    <div class="row">
        <div class="col-lg-12">
            <footer>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="menu-title">О компании</div>
                        <ul>
                            <li><a href="/">Главная</a></li>
                        </ul>
                    </div>
                </div>
            </footer>

            <div class="copyrights">
                Все права защишены © 2014<br>
                Любое копирование информации с сайта запрещено.<br>
                <a href="http://poly-commerce.ru/">Poly Commerce - решение для интернет-магазина</a> | <a
                    href="http://miriti.ru/">miriti.ru</a>
            </div>

            <div>
                <!--LiveInternet counter-->
                <script type="text/javascript"><!--
                    document.write("<a href='//www.liveinternet.ru/click' " +
                        "target=_blank><img src='//counter.yadro.ru/hit?t14.2;r" +
                        escape(document.referrer) + ((typeof(screen) == "undefined") ? "" :
                        ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                            screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                        ";" + Math.random() +
                        "' alt='' title='LiveInternet: показано число просмотров за 24" +
                        " часа, посетителей за 24 часа и за сегодня' " +
                        "border='0' width='88' height='31'><\/a>")
                    //--></script>
                <!--/LiveInternet-->

                <!-- begin of Top100 code -->

                <script id="top100Counter" type="text/javascript"
                        src="http://counter.rambler.ru/top100.jcn?3041490"></script>
                <noscript>
                    <a href="http://top100.rambler.ru/navi/3041490/">
                        <img src="http://counter.rambler.ru/top100.cnt?3041490" alt="Rambler's Top100" border="0"/>
                    </a>

                </noscript>
                <!-- end of Top100 code -->

            </div>
        </div>
    </div>
</div>
<?php if ($_SERVER['HTTP_HOST'] != 'starkmeister.localhost') { ?>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function () {
            var widget_id = 'Yy1RuYYoPX';
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//code.jivosite.com/script/widget/' + widget_id;
            var ss = document.getElementsByTagName('script')[0];
            ss.parentNode.insertBefore(s, ss);
        })();</script>
    <!-- {/literal} END JIVOSITE CODE -->
<?php } ?>
</body>
</html>