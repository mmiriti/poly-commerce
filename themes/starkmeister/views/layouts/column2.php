<?php
/** @var $this Controller */

$this->beginContent('//layouts/column1');

?>
    <!-- LEFT SIDEBAR -->
    <div class="row">
        <div class="col-lg-3 new-column">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6">
                    <div class="block-title">Поиск товаров</div>
                    <?php
                    echo CHtml::beginForm(array('search/index'), 'get', array('class' => 'item-search'));
                    ?>
                    <input type="text" placeholder="Название или артикул товара" name="q"
                           value="<?php echo isset($_GET['q']) ? $_GET['q'] : ''; ?>">
                    <label>
                        <input type="checkbox" name="options[field][name]" checked> <span>в названии товара</span>
                    </label>
                    <label>
                        <input type="checkbox" name="options[field][supplier_code]" checked> <span>по артикулу</span>
                    </label>
                    <label>
                        <input type="checkbox" name="options[field][oem_code]" checked> <span>по артиклу OEM</span>
                    </label>
                    <label>
                        <input type="checkbox" name="options[param][count_warehouse]" value=">0"> <span>искать товары в наличии</span>
                    </label>
                    <input type="submit" value="Искать">

                    <div class="clear"></div>
                    <?php
                    echo CHtml::endForm();
                    ?>
                </div>
            </div>
            <div class="row">
                <?php
                if (Yii::app()->user->isGuest) {
                    ?>
                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div class="block-title no-border">Личный кабинет</div>
                        <?php
                        $loginUser = new User('login');

                        if (isset($_POST['User'])) {
                            $loginUser->attributes = $_POST['User'];
                        }

                        echo CHtml::beginForm(array('/user/auth/login'), 'post', array('class' => 'login-form'));
                        echo CHtml::activeTextField($loginUser, 'email', array('placeholder' => 'E-mail'));
                        echo CHtml::activePasswordField($loginUser, 'password', array('placeholder' => 'Пароль'));
                        ?>
                        <input type="submit" value="Вход">

                        <div class="clear"></div>
                        <div class="links">
                            <a href="<?php echo Yii::app()->createUrl('/user/registration/index'); ?>">Регистрация</a>
                            <a href="<?php echo Yii::app()->createUrl('/user/passwordRecovery/index'); ?>">Восстановить
                                пароль</a>
                        </div>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                <?php
                } else {
                    ?>
                    <div class="col-lg-12 col-md-6 col-sm-6 user-widget">
                        <div class="block-title"><?php echo User::current()->fullName; ?></div>
                        <div class="clear"></div>
                        <div class="links">
                            <?php echo CHtml::link('<span class="glyphicon glyphicon-shopping-cart"></span> Корзина (' . CartHelper::getCount() . ')', array('/cart/index')); ?>
                            <br>
                            <?php echo CHtml::link('<span class="glyphicon glyphicon-user"></span> Личный кабинет', array('/user/home/index')); ?>
                            <br>

                            <?php
                            if (User::current()->userGroup->role == 'admin') {
                                ?>
                                <?php echo CHtml::link('<strong><span class="glyphicon glyphicon-cog"></span> Панель администратора</strong>', array('/ctrl')); ?>
                                <br>
                            <?php
                            }
                            ?>
                            <?php echo CHtml::link('<span class="glyphicon glyphicon-log-out"></span> Выход', array('/user/auth/logout')); ?>
                            <br>
                        </div>
                        <div class="clear"></div>
                    </div>
                <?php
                }
                ?>
            </div>


            <div class="row">
                <?php
                $emailForm = Form::model()->findByAttributes(array('slug' => 'subscribe'));

                if ($emailForm != null) {
                    ?>
                    <div class="col-lg-12">
                        <div class="block-title">Подписка на новости</div>
                        <?php
                        echo CHtml::beginForm(array('form/post'), 'post', array('class' => 'subscribe-form'));
                        ?>
                        <input type="text" placeholder="почта@mail.ru" name="FormField[subscribe][email]">
                        <input type="submit" value="Подписаться">
                        </form>
                    </div>
                <?php
                }

                if (isset($this->commonBlocks['contacts'])) {
                    ?>
                    <div class="col-lg-12">
                        <?php echo $this->commonBlocks['contacts']->data; ?>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>

        <!-- MIDDLE CONTENT -->
        <div class="col-lg-9 new-column">
            <?php echo $content; ?>
        </div>
    </div>
<?php $this->endContent(); ?>