<?php $this->beginContent('//layouts/column1'); ?>
    <!-- LEFT SIDEBAR -->
    <div class="row">
        <div class="col-lg-3 new-column">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6">
                    <div class="block-title">Поиск товаров</div>
                    <form class="item-search">
                        <input type="text" placeholder="Название или id товара"/>
                        <label>
                            <input type="checkbox"/> <span>по артикулу</span>
                        </label>
                        <label>
                            <input type="checkbox"/> <span>в названии товара</span>
                        </label>
                        <label>
                            <input type="checkbox"/> <span>искать товары в наличии</span>
                        </label>
                        <input type="submit" value="Искать"/>

                        <div class="clear"></div>
                    </form>
                </div>

                <div class="col-lg-12 col-md-6 col-sm-6">
                    <div class="block-title no-border">Личный кабинет</div>
                    <form class="login-form">
                        <input type="text" placeholder="Логин"/>
                        <input type="password" placeholder="Пароль"/>
                        <input type="submit" value="Вход"/>

                        <div class="clear"></div>
                        <div class="links">
                            <a href="#">Регистрация</a>
                            <a href="#">Восстановить пароль</a>
                        </div>
                    </form>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <div class="block-title">Подписка на новости</div>
                    <form class="subscribe-form">
                        <input type="text" placeholder="почта@mail.ru"/>
                        <input type="submit" value="Подписаться"/>
                    </form>
                </div>

                <div class="col-lg-12">
                    <div class="block-title">Контакты</div>
                    <div class="map">
                        <script type="text/javascript" charset="utf-8"
                                src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=eRjd_MnF0WjkcRs84hreINvxpGWL3I9y&width=100%&height=200"></script>
                    </div>
                    <div class="phone">+7 (495) 239 25 47</div>
                    <div class="phone">+7 (926) 058 88 00</div>
                    <a href="#" class="email">starkmeister@gmail.ru</a>
                </div>
            </div>
        </div>

        <!-- MIDDLE CONTENT -->
        <div class="col-lg-6 new-column">
            <?php echo $content; ?>
        </div>

        <!-- RIGHT SIDEBAR -->
        <div class="col-lg-3 new-column">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-title">Новые товары</div>
                    <ul class="new-items">
                        <li>
                            <div class="image">
                                <img src="/themes/starkmeister/img/new-items/example.png" alt=""/>
                            </div>
                            <a href="#" class="title">
                                Сцепление Iveco Daily
                                SM509.344.344
                            </a>
                        </li>
                        <li>
                            <div class="image">
                                <img src="/themes/starkmeister/img/new-items/example.png" alt=""/>
                            </div>
                            <a href="#" class="title">
                                Тормоза Iveco Daily
                                SM509.344.344
                            </a>
                        </li>
                    </ul>
                    <div class="sliderlink">
                        <a href="#">Перейти в каталог »</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="block-title">Каталоги</div>
                    <ul class="catalog-items">
                        <li>
                            <div class="image">
                                <img src="/themes/starkmeister/img/catalog-items/example.png" alt=""/>
                            </div>
                            <a href="#" class="title">
                                Запчасти DAF<br/>
                                для серии 95FX
                            </a>
                        </li>
                        <li>
                            <div class="image">
                                <img src="/themes/starkmeister/img/catalog-items/example.png" alt=""/>
                            </div>
                            <a href="#" class="title">
                                Запчасти DAF<br/>
                                для серии 95FX
                            </a>
                        </li>
                    </ul>
                    <div class="sliderlink">
                        <a href="#">Все каталоги »</a>
                    </div>
                </div>
            </div>
            <div class="row ads">
                <div class="col-lg-12">
                    <a href="#" class="group">
                        <img src="/themes/starkmeister/img/ads-1.png"/>
                    </a>
                    <a href="#" class="group">
                        <img src="/themes/starkmeister/img/ads-2.png"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>