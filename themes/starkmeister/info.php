<?php
return array(
    'name' => 'Starkmeister',
    'layouts' => array(
        'column1' => 'Одна колонка',
        'column2' => 'Две колонки',
        'column3' => 'Три колонки',
    ),
    'defaultLayout' => 'column2'
);