$(function () {
    $('textarea.wysiwyg').ckeditor();
    $('input.date').datepicker({
        dateFormat: "yy-mm-dd"
    });
});