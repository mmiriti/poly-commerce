$(function () {

    $.each($('.folding-header'), function (index, sel) {
        var hideAreaSelector = $(sel).attr('data-folding-selector');
        $(hideAreaSelector).hide();

        $(sel).find('a').bind('click', function (e) {
            e.preventDefault();
            $(hideAreaSelector).slideToggle(250);
        });
    });

    $('.file-drop-area').on('dragover', function (e) {
        $(this).addClass('drag-hover');
        e.preventDefault();
        e.stopPropagation();
    });

    $('.file-drop-area').on('dragleave', function (e) {
        $(this).removeClass('drag-hover');
        e.preventDefault();
        e.stopPropagation();
    });

    $('.file-drop-area').on('dragover', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    $('.file-drop-area').on('dragenter', function (e) {
        e.preventDefault();
        e.stopPropagation();
    });

    var uploadInProgress = false;

    $('#galleryClassicUploadFiles').bind('click', function (e) {
        e.preventDefault();
        if (uploadInProgress)return;

        var itemID = $(this).attr('data-item-id');

        var files = $('#galleryClassicFiles');
        files = files[0].files; // /O

        if (files.length == 0) {
            alert("Не выбрано ни одного файла");
            return;
        }

        $(this).attr('disabled', 'disabled');

        var filesLoaded = 0;
        var filesToLoad = files.length;

        var btn = this;

        for (var i = 0; i < files.length; i++) {
            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function (e) {
                if (e.target.readyState == 4) {
                    if (e.target.status == 200) {
                        filesLoaded++;
                        if (filesLoaded == filesToLoad) {
                            uploadInProgress = false;
                            $(btn).removeAttr('disabled');
                            $('#galleryClassicUploadStatus').html('');
                            updateGallery(itemID);
                        } else {
                            $('#galleryClassicUploadStatus').html('Загружено ' + filesLoaded + '/' + filesToLoad);
                        }
                    }
                }
            };

            xhr.open('POST', '/ctrl/itemGallery/upload/item_id/' + itemID);

            var fd = new FormData();
            fd.append("image", files[i]);

            xhr.send(fd);
        }
    });

    $('.file-drop-area').bind('drop', function (e) {
        if (uploadInProgress) return;
        if (e.originalEvent.dataTransfer) {
            if (e.originalEvent.dataTransfer.files.length) {
                e.preventDefault();
                e.stopPropagation();

                uploadInProgress = true;

                $(uploadContainer).html('Загрузка...');

                var filesToLoad = e.originalEvent.dataTransfer.files.length;
                var filesLoaded = 0;

                for (var i = e.originalEvent.dataTransfer.files.length - 1; i >= 0; i--) {
                    var file = e.originalEvent.dataTransfer.files[i];
                    var uploadContainer = $(this);
                    var itemID = $(this).attr('data-item-id');
                    var xhr = new XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function (e) {
                    }, false);

                    xhr.onreadystatechange = function (e) {
                        if (e.target.readyState == 4) {
                            if (e.target.status == 200) {
                                filesLoaded++;
                                $(uploadContainer).html('Загрузка... (' + filesLoaded + '/' + filesToLoad + ')');
                                if (filesLoaded == filesToLoad) {
                                    uploadInProgress = false;
                                    $(uploadContainer).html('Готово!');
                                    $(uploadContainer).removeClass('drag-hover');
                                    updateGallery(itemID);
                                }
                            }
                        }
                    };

                    xhr.open('POST', '/ctrl/itemGallery/upload/item_id/' + itemID);

                    var fd = new FormData();
                    fd.append("image", file);
                    xhr.send(fd);
                }
                ;
            }
        }
    });
});

function updateGallery(id) {
    $('#item-gallery-' + id).load('/ctrl/itemGallery/index/item_id/' + id);
}

function deleteImage(item_id, id) {
    $.ajax({
        url: '/ctrl/itemGallery/delete/gallery_item_id/' + id,
        success: function () {
            updateGallery(item_id);
        },
        error: function () {
            updateGallery(item_id);
        }
    });
    return false;
}