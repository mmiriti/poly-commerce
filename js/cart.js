function cart_add(item_id, qty, variants, callback) {
    $.ajax({
        'url': '/cart/add?item_id=' + item_id + '&qty=' + qty,
        'type': 'post',
        'data': {
            'variants': variants
        },
        'dataType': 'json',
        'success': function (jData) {
            cart_refresh_widget();

            if (typeof callback === "function") {
                callback.call(this);
            }
        }
    });
}

function cart_inc(item_id, qty, callback) {
    if (typeof qty === "undefined") {
        qty = 1;
    }

    $.ajax({
        'url': '/cart/inc?item_id=' + item_id + '&qty=' + qty,
        'success': function () {
            if (typeof callback === "function") {
                callback.call(this);
            }
        }
    });
}

function cart_dec(item_id, qty, callback) {
    if (typeof qty === "undefined") {
        qty = 1;
    }

    $.ajax({
        'url': '/cart/dec?item_id=' + item_id + '&qty=' + qty,
        'success': function () {
            if (typeof callback === "function") {
                callback.call(this);
            }
        }
    });
}

function cart_refresh_widget() {
    if ($('.cart-widget').length != 0) {
        $.ajax({
            'url': '/cart/widget',
            'type': 'get',
            'success': function (html) {
                $('.cart-widget').replaceWith(html);
            }
        });
    }
}

$(function () {

});