<?php

class ManagerController extends CController
{
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('manager'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }
} 