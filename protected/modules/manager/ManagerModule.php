<?php

class ManagerModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'manager.models.*',
			'manager.components.*',
            'manager.widgets.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
