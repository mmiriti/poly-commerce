<?php

class RegistrationController extends UserController
{
    public function actionIndex()
    {
        $user = new User('reg');

        $customerGroup = UserGroup::model()->findByAttributes(array('role' => 'customer'));

        if ($customerGroup != null) {
            $user->user_group_id = $customerGroup->id;
        }

        $customerForm = new CustomerForm();

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if (isset($_POST['CustomerForm'])) {
                $customerForm->attributes = $_POST['CustomerForm'];
            }

            $usesValid = $user->validate();
            $customerValid = $customerForm->validate();

            if ($usesValid && $customerValid) {
                if ($user->save()) {
                    $customer = new Customer();
                    $customer->user_id = $user->id;

                    if ($customer->save()) {
                        $customerForm->customer_id = $customer->id;
                        if ($customerForm->save()) {
                            $this->redirect(array('success'));
                        }
                    }
                }
            }
        }

        $this->render('index', array(
                'user' => $user,
                'customerForm' => $customerForm,
            )
        );
    }

    public function actionSuccess()
    {
        $this->render('success');
    }
}
