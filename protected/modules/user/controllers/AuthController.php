<?php

class AuthController extends UserController
{
    public function actionLogin()
    {
        $user = new User('login');

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if ($user->validate()) {
                if ($user->login()) {
                    if ($user->userGroup->role == 'admin') {
                        $this->redirect(array('/ctrl/default/index'));
                    } else {
                        $this->redirect(array('/user/home'));
                    }
                }
            }
        }

        $this->render('login', array(
                'user' => $user
            )
        );
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(array('/user/auth/login'));
    }
}
