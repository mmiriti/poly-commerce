<?php

class HomeController extends UserController
{
    public $homeMenu = array(
        array('url' => array('/user/home/index'), 'label' => 'Регистрационные данные'),
        array('url' => array('/user/home/orders'), 'label' => 'Заказы'),
        array('url' => array('/user/home/searches'), 'label' => 'История поиска'),
    );

    public function actionIndex()
    {
        $user = User::current();

        if ($user->customer == null) { // TODO Dirty
            $customer = new Customer();
            $customer->user_id = $user->id;
            $customer->save();

            $user = User::current();
        }

        $customerForm = new CustomerForm();
        $customerForm->customer_id = $user->customer->id;

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if (isset($_POST['CustomerForm'])) {
                $customerForm->attributes = $_POST['CustomerForm'];
            }

            if ($user->save() && $customerForm->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('index', array(
            'user' => $user,
            'customerForm' => $customerForm,
        ));
    }

    public function actionOrders()
    {
        $ordersCriteria = new CDbCriteria();
        $ordersCriteria->compare('customer_id', User::current()->customer->id);
        $ordersCriteria->order = 'create_time desc';

        $orders = new CActiveDataProvider(Order::model(), array(
            'criteria' => $ordersCriteria
        ));

        $this->render('orders', array(
            'orders' => $orders,
        ));
    }

    public function actionSearches()
    {
        $searchesCriteria = new CDbCriteria();
        $searchesCriteria->compare('user_id', User::current()->id);
        $searchesCriteria->order = 'search_time desc';

        $searches = new CActiveDataProvider(SearchQueryUser::model(), array(
            'criteria' => $searchesCriteria,
        ));

        $this->render('searches', array(
            'searches' => $searches,
        ));
    }
}