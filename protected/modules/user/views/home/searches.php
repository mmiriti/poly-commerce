<?php
/* @var $this HomeController */
/* @var $searches CActiveDataProvider */
$this->pageTitle = 'Личный кабинет пользователя - история поиска';
?>
    <h1>Личный кабинет пользователя</h1>
<?php
$this->renderPartial('_menu');

if ($searches->itemCount == 0) {
    ?>
    <div class="alert alert-info">
        В вашей истории нет ни одного поиска
    </div>
<?php
} else {
    ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Дата</th>
            <th>Запрос</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($searches->data as $search) {
            /* @var $search SearchQueryUser */
            ?>
            <tr>
                <td><?php echo Yii::app()->dateFormatter->format("d MMM, H:mm", strtotime($search->search_time)); ?></td>
                <td><?php echo CHtml::link($search->query->query, array('/search/index', 'q' => $search->query->query, 'options' => unserialize($search->query->options))); ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php

}