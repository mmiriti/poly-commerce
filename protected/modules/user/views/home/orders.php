<?php
/* @var $this HomeController */
/* @var $orders CActiveDataProvider */
$this->pageTitle = 'Личный кабинет пользователя - заказы';
?>
    <h1>Личный кабинет пользователя</h1>
<?php
$this->renderPartial('_menu');

if ($orders->itemCount == 0) {
    ?>
    <div class="alert alert-info">
        Вы не сделали ни одного заказа. <?php echo CHtml::link('Перейти в каталог', array('/catalog/index')); ?>
    </div>
<?php
} else {
    ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Номер заказа</th>
            <th>Дата заказа</th>
            <th>Состав заказа</th>
            <th>Сумма</th>
            <th>Статус</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($orders->data as $order) {
            /* @var $order Order */
            ?>
            <tr>
                <td><?php echo $order->id; ?></td>
                <td><?php echo $order->create_time; ?></td>
                <td>
                    <ul>
                        <?php
                        foreach ($order->orderItems as $orderItem) {
                            echo '<li>' . $orderItem->item->name . ' <span class="badge">' . $orderItem->quantity . '</span></li>';
                        }
                        ?>
                    </ul>
                </td>
                <td><?php echo number_format($order->calcTotal('RUR')); ?> руб.</td>
                <td><?php echo $order->status->name; ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php
}