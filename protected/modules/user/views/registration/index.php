<?php
/* @var $this UserController */
/* @var $user User */
/* @var $customerForm CustomerForm */
?>
<h1>Регистрация пользователя</h1>
<h2>Основные данные</h2>
<?php
echo CHtml::beginForm();
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'name'));
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'email', 'type' => 'email'));
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'password', 'type' => 'password'));
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'password_again', 'type' => 'password'));
?>
<h2>Дополнительные данные</h2>
<?php

if ($customerForm->hasErrors()) {
    ?>
    <div class="alert alert-danger">
        <?php echo CHtml::errorSummary($customerForm); ?>
    </div>
<?php
}

foreach ($customerForm->getCustomerDataFields() as $dataField) {
    $this->widget('FormGroup', array('model' => $customerForm, 'fieldName' => $dataField->slug, 'type' => $dataField->inputType, 'listData' => $dataField->optionsList, 'hint' => $dataField->hint));
}

$this->widget('FormButton', array('title' => 'Регистрация'));
echo CHtml::endForm();
?>
