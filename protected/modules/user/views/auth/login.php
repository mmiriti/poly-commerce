<?php
/* @var $user User */
/* @var $this AuthController */
?>
<h1>Авторизация пользователя</h1>
<?php
echo CHtml::beginForm();
if($user->hasErrors()) {
    ?>
    <div class="alert alert-danger">
        <?php
        echo CHtml::errorSummary($user);
        ?>
    </div>
<?php
}
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'email', 'type' => 'email'));
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'password', 'type' => 'password'));
$this->widget('FormButton', array('title' => 'Войти'));
echo Chtml::endForm();
?>