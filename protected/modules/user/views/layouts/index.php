<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $this->pageTitle; ?></title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <script src="/js/jquery-2.1.1.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-2"></div>
        <div class="col-xs-10">
            <?php echo $content; ?>
        </div>
    </div>
</div>
</body>
</html>