<?php

class UserController extends Controller
{
    public $layout = '/layouts/index';

    public function init()
    {
        parent::init();
        $this->defaultAction = 'index';
    }
}