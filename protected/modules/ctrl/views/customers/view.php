<?php
/* @var $this CustomersController */
/* @var $customer Customer */
?>
<div class="container">
    <h1><?php echo $customer->user->fullName; ?></h1>

    <div class="well">
        <?php echo CHtml::link('Назад', array('index'), array('class' => 'btn btn-danger')); ?>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>Поле</th>
            <th>Значение</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach (CustomerForm::getCustomerDataFields() as $field) {
            ?>
            <tr>
                <td><?php echo $field->name; ?></td>
                <td><?php echo $customer->getField($field->slug); ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</div>