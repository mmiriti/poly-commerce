<?php
/* @var $this CustomersController */
/* @var $customer Customer */
?>
<div class="container">
    <h1>Индивидуальные наценки для <?php echo $customer->user->fullName; ?></h1>

    <div class="well">
        <?php echo CHtml::link('Назад', array('index'), array('class' => 'btn btn-danger')); ?>
    </div>
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo CHtml::beginForm('', 'post', array('class' => "form-horizontal", 'role' => "form")); ?>
                    <div class="form-group">
                        <label for="#" class="col-sm-6 control-label">Наценка на все товары</label>

                        <div class="col-sm-6">
                            <?php echo CHtml::activeTextField($customer, 'surcharge', array('class' => 'form-control input-lg')); ?>
                        </div>
                    </div>
                    <h2>Наценка на категории товаров</h2>
                    <?php
                    $criteria = new CDbCriteria();
                    $criteria->order = 'name';

                    $categories = CatalogCategory::model()->findAll($criteria);

                    foreach ($categories as $category) {
                        $value = CustomerCategorySurcharge::model()->findByAttributes(array('customer_id' => $customer->id, 'category_id' => $category->id));

                        ?>
                        <div class="form-group">
                            <label class="col-sm-6 control-label" for="#"><?php echo $category->name; ?></label>

                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="CategorySurcharge[<?php echo $category->id; ?>]"
                                       value="<?php if ($value != null) echo $value->surcharge; ?>">
                            </div>
                        </div>
                    <?php
                    }
                    ?>

                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Сохранить</button>
                        </div>
                    </div>

                    <?php echo CHtml::endForm(); ?>
                </div>
            </div>
        </div>
    </div>
</div>