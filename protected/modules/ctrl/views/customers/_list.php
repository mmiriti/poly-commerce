<table class="table table-hover">
    <thead>
    <tr>
        <th>Имя</th>
        <th>E-mail</th>
        <th>Дата регистрации</th>
        <th>Менеджеры</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($customers as $customer) {
        ?>
        <tr>
            <td><?php echo $customer->user->fullName; ?></td>
            <td><?php echo CHtml::link($customer->user->email, 'mailto:' . $customer->user->email); ?></td>
            <td><?php echo Yii::app()->dateFormatter->format("d MMM y, H:m", strtotime($customer->user->create_time)); ?></td>
            <td><?php
                if (count($customer->managers) == 0) {
                    echo CHtml::link('<span class="glyphicon glyphicon-asterisk"></span> Назначить', array('attach', 'id' => $customer->id), array('class' => 'btn btn-xs btn-warning'));
                } else {
                    foreach ($customer->managers as $manager) {
                        echo $manager->fullName . ' ';
                    }
                    echo CHtml::link('<span class="glyphicon glyphicon-asterisk"></span>', array('attach', 'id' => $customer->id), array('class' => 'btn btn-xs btn-warning', 'title' => 'Изменить назначенных менеджеров'));
                }
                ?></td>
            <td>
                <div class="btn-group">
                    <?php
                    echo CHtml::link('<span class="glyphicon glyphicon-eye-open"></span>', array('view', 'id' => $customer->id), array('class' => 'btn btn-default btn-xs', 'title' => 'Просмотр данных пользователя'));
                    echo CHtml::link('<span class="glyphicon glyphicon-list"></span>', array('surcharge', 'id' => $customer->id), array('class' => 'btn btn-xs btn-info', 'title' => 'Индивидуальные наценки пользователя'));
                    ?>
                </div>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>