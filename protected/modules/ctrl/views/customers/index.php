<?php
/* @var $allCustomers Customer[] */
/* @var $this CustomersController */
/* @var $dataFields CustomerDataField[] */

$nomCount = Customer::unattachedCount();
?>
<h1>Клиенты</h1>
<hr>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="#all" role="tab" data-toggle="tab">Все</a></li>
    <?php
    if ($nomCount > 0) {
        ?>
        <li><a href="#nomanagers" role="tab" data-toggle="tab"><strong>Менеджеры не назначены
                    <span class="badge"><?php echo Customer::unattachedCount(); ?></span></strong></a></li>
    <?php
    }
    ?>
</ul>
<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="all">
                <div class="table-responsive">
                    <?php
                    $this->renderPartial('_list', array('customers' => $allCustomers));
                    ?>
                </div>
            </div>

            <?php
            if ($nomCount > 0) {
                ?>
                <div class="tab-pane" id="nomanagers">
                    <div class="table-responsive">
                        <?php
                        $nomanager = array();
                        foreach ($allCustomers as $customer) {
                            if (count($customer->managers) == 0) {
                                $nomanager[] = $customer;
                            }
                        }

                        $this->renderPartial('_list', array('customers' => $nomanager));
                        ?>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>