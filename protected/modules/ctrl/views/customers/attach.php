<?php
/* @var $this CustomersController */
/* @var $managers User[] */
/* @var $customer Customer */
/* @var $attachedManagersIDS[] integer */
?>
    <h1>Назначить менеджеров</h1>
    <div class="well">
        <?php echo CHtml::link('Назад', array('index'), array('class' => 'btn btn-danger')); ?>
    </div>
    <h2>Менеджеры для <strong><?php echo $customer->user->fullName; ?></strong></h2>
<?php
echo CHtml::beginForm();
foreach ($managers as $manager) {
    ?>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="AttachManager[]" value="<?php echo $manager->id; ?>"<?php if(in_array($manager->id, $attachedManagersIDS)) { ?> checked="checked"<?php } ?>> <?php echo $manager->fullName; ?>
        </label>
    </div>
<?php
}
?>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
<?php
echo CHtml::endForm();