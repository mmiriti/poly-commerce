<style>
    .media-object {
        max-width: 150px;
    }
</style>
<h1><?php echo CHtml::link('Заказы', array('index')); ?> - Заказ #<?php echo $order->id; ?></h1>
<hr>
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Информация о заказе</div>
                    <table class="table table-hover">
                        <?php
                        foreach (OrderDataField::model()->findAll() as $field) {
                            ?>
                            <tr>
                                <td><strong><?php echo $field->name; ?></strong></td>
                                <td><?php echo $order->getField($field->slug); ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Информация о клиенте</div>
                    <table class="table table-hover">
                        <?php
                        if ($order->customer->user != null) {
                            ?>
                            <tr>
                                <td><strong>E-mail</strong></td>
                                <td><?php echo CHtml::link($order->customer->user->email, 'mailto:' . $order->customer->user->email); ?></td>
                            </tr>
                        <?php
                        }

                        foreach (CustomerDataField::model()->findAll() as $field) {
                            ?>
                            <tr>
                                <td><strong><?php echo $field->name; ?></strong></td>
                                <td><?php echo $order->customer->getField($field->slug); ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<h2>Состав заказа</h2>
<hr>
<div class="row">
    <div class="col-md-12">
        <?php foreach ($order->orderItems as $item) { ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <a class="pull-left"
                           href="<?php echo $this->createUrl('catalog/edititem', array('id' => $item->item->id, 'parent_id' => (count($item->item->catalogCategories) > 0) ? $item->item->catalogCategories[0]->id : null)); ?>">
                            <?php
                            if (count($item->item->galleryItems) > 0) {
                                ?>
                                <img class="media-object"
                                     src="<?php echo $item->item->galleryItems[0]->file->thumb_src; ?>"
                                     alt="<?php echo $item->item->name; ?>">
                            <?php
                            }
                            ?>
                        </a>

                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $item->item->name; ?></h4>

                            <p><b>Количество: </b><?php echo $item->quantity; ?></p>

                            <p>
                                <b>Цена: </b><?php echo number_format($item->price->value) . ' ' . $item->price->currency->sign; ?>
                                .
                                <em>(на <?php echo Yii::app()->dateFormatter->format("d MMM H:mm", strtotime($item->price->create_time)); ?>
                                    )</em></p>

                            <p><b>Итого: </b><?php echo number_format($item->price->value * $item->quantity); ?>
                                <?php echo $item->price->currency->sign; ?>
                            </p>
                            <?php
                            foreach ($item->variants as $variant) {
                                ?>
                                <p><strong><?php echo $variant->option->name; ?>
                                        : </strong><?php echo $variant->string; ?></p>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>