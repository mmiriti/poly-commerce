<?php
/* @var $orders Order[] */
$this->pageTitle = 'Заказы';
?>
<script>
    $(function () {
        $('.change-status').bind('click', function (e) {
            var status = $(this).text();
            var title = $(this).parent().parent().parent().find('.title');

            console.log($(this).attr('href'));

            $.ajax({
                url: $(this).attr('href'),
                success: function () {
                    $(title).html(status);
                }
            });
            e.preventDefault();
        });
    });
</script>
<h1>Заказы</h1>
<hr>
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li <?php if ($status_id == null) echo ' class="active"'; ?>><?php echo CHtml::link('Все <span class="badge">' . count(Order::model()->findAll()) . '</span>', array('index')); ?></li>
            <?php
            $all_statuses = OrderStatus::model()->findAll();
            foreach ($all_statuses as $status) {
                ?>
                <li<?php if ($status->id == $status_id) echo ' class="active"' ?>><?php echo CHtml::link($status->name . ' <span class="badge">' . count($status->orders) . '</span>', array('index', 'status_id' => $status->id)); ?></li>
            <?php
            }
            ?>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th width="1">#</th>
                <th>Поступил</th>
                <th>Итого</th>
                <th>Статус</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $order) { ?>
                <tr>
                    <td><?php echo CHtml::link($order->id, array('view', 'id' => $order->id)); ?></td>
                    <td><?php echo Yii::app()->dateFormatter->format("d MMM H:mm", strtotime($order->create_time)); ?></td>
                    <td><?php
                        $cnt = count($order->orderItems);
                        echo $cnt . ' ' . Common::getNumEnding($cnt, array('позиция', 'позиции', 'позиций')); ?> на
                        <strong><?php echo number_format($order->calcTotal('EUR')); ?></strong> <?php echo Currency::model()->findByAlpha3('EUR')->sign; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="title"><?php echo $order->status->name; ?></span> <span
                                    class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <?php
                                foreach ($all_statuses as $status) {
                                    ?>
                                    <li><?php echo CHtml::link($status->name, array('status', 'order_id' => $order->id, 'id' => $status->id), array('class' => 'change-status')); ?></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </td>
                    <td width="1">
                        <div class="btn-group">
                            <?php echo CHtml::link('<span class="glyphicon glyphicon-info-sign"></span> инфо', array('view', 'id' => $order->id), array('class' => 'btn btn-info btn-xs', 'title' => 'Просмотр данных заказа')); ?>
                            <?php //echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('delete', 'id' => $order->id), array('class' => 'btn btn-danger btn-xs', 'title' => 'Удалить')); ?>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>