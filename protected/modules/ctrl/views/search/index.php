<script>
    $(function () {
        $('.show-options').bind('click', function (e) {
            e.preventDefault();
            var queryId = $(this).attr('data-query-id');
            $('#options-' + queryId).toggle(200);
        });
    });
</script>
<h1>Поиск</h1>
<hr>
<h2>Поисковые запросы</h2>
<div class="row">
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Запрос</th>
                <th>Параметры запроса</th>
                <th>Дата первого запроса</th>
                <th>Дата последнего индексирования</th>
                <th>Количество запросов</th>
                <th>Количество результатов</th>
                <th>Отображать в подсказках</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($querys as $q) {
                ?>
                <tr>
                    <td><?php echo CHtml::link($q->query, array('/search/index', 'q' => $q->query)); ?></td>
                    <td><?php
                        if ($q->options != null) {
                            ?>
                            <a href="#" data-query-id="<?php echo $q->id; ?>" class="show-options">показать/скрыть</a>
                            <pre id="options-<?php echo $q->id; ?>"
                                 style="display: none;"><?php print_r(unserialize($q->options)); ?></pre>
                        <?php
                        }
                        ?></td>
                    <td><?php echo $q->create_time; ?></td>
                    <td><?php echo $q->index_time; ?></td>
                    <td><?php echo $q->count; ?></td>
                    <td><?php echo count($q->searchQueryResults); ?></td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                <?php echo $q->public == 1 ? 'да' : 'нет'; ?> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li <?php if ($q->public == 1) { ?> class="active"<?php } ?>><?php echo CHtml::link('да', array('public', 'query_id' => $q->id, 'public' => 1)); ?></li>
                                <li <?php if ($q->public == 0) { ?> class="active"<?php } ?>><?php echo CHtml::link('нет', array('public', 'query_id' => $q->id, 'public' => 0)); ?></li>
                            </ul>
                        </div>
                    </td>
                    <td></td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>