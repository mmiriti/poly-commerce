<?php

if(count($item->galleryItems))
{
	?>
    <h3>Фотографий: <?php echo count($item->galleryItems); ?></h3>
	<div class="row">
		<?php
		$c = 0;
		foreach ($item->galleryItems as $galleryItem) 
		{
			?>
			<div class="col-md-3">
				<div class="thumbnail">
					<?php echo CHtml::link(CHtml::image($galleryItem->file->url, $item->name), $galleryItem->file->url, array('target' => '_blank')); ?>
					<p><div class="btn-group">
						<button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
						Действия <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#" onclick="return deleteImage(<?php echo $item->id; ?>, <?php echo $galleryItem->id; ?>);">Удалить</a></li>
						</ul>
					</div></p>
				</div>
			</div>
			<?php

			$c++;

			if($c == 4)
			{
				$c = 0;
				?>
			</div>
			<div class="row">
				<?php
			}
		}
		?>
	</div>
	<?php
}else{
	?>
	<div class="alert alert-info">Пока не загружено ни одной фотографии</div>
	<?php
}