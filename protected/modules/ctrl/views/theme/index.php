<?php
/* @var $this ThemeController */
$this->pageTitle = 'Тема';
?>
    <h1><?php echo $this->pageTitle; ?></h1>
    <hr/>
<?php echo CHtml::beginForm(); ?>
    <div class="form-group">
        <label for="">Тема сайта</label>
        <?php
        $listData = array();

        foreach ($themes as $theme_id => $t) {
            $listData[$theme_id] = $t['name'];
        }

        echo CHtml::dropDownList('theme', Options::get('site_theme', 'bootstrap', 'Тема сайта'), $listData, array('class' => 'form-control'));
        ?>
    </div>
<div class="form-group">
    <button type="submit" class="btn btn-primary">Сохранить</button>
</div>
<?php echo CHtml::endForm(); ?>