<?php
/* @var $this ClassifierController */
/* @var $classifierGroups ClassifierGroup[] */
?>
<h1>Классификаторы</h1>
<hr>
<div class="well">
    <?php echo CHtml::link('В Каталог', array('catalog/index'), array('class' => 'btn btn-danger')); ?>
    <?php echo CHtml::link('Создать классификатор', array('create'), array('class' => 'btn btn-primary')); ?>
</div>
<table class="table table-hover table-stripped">
    <thead>
    <tr>
        <th>Название</th>
        <th>Техническое имя</th>
        <th>Описание</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($classifierGroups as $group) {
        ?>
        <tr>
            <td><?php echo $group->name; ?></td>
            <td><code><?php echo $group->slug; ?></code></td>
            <td><?php echo $group->description; ?></td>
            <td>
                <div class="btn btn-group pull-right">
                    <?php echo CHtml::link('<span class="glyphicon glyphicon-edit"></span>', array('update', 'id' => $group->id), array('class' => 'btn btn-xs btn-default')); ?>
                    <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('delete', 'id' => $group->id), array('class' => 'btn btn-xs btn-danger')); ?>
                </div>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>