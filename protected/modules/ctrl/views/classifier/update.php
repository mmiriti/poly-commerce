<?php
/* @var $this ClassifierController */
/* @var $classifierGroup ClassifierGroup */
/* @var $classifier Classifier */
?>
    <style>
        ul#values li {
            font-weight: bold;
        }

        ul#values, ul#values ul {
            list-style: none;
            padding-left: 15px;
        }
    </style>
    <h1>Изменить классификатор</h1>
    <hr>
    <div class="well">
        <?php echo CHtml::link('Отмена', array('index'), array('class' => 'btn btn-danger')); ?>
    </div>
<?php
$this->renderPartial('_form', array('classifierGroup' => $classifierGroup));
?>
    <h2>Значения</h2>
    <hr>
    <ul id="values">
        <?php
        /**
         * @param $item Classifier
         * @param $_this ClassifierController
         */
        function _renderList($item, $_this)
        {
            ?>
            <li title="<?php echo $item->info; ?>"><?php echo $item->title; ?>
                <div class="btn-group"><?php
                    echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deleteClassifier', 'id' => $item->id), array('class' => 'btn btn-danger btn-xs'));
                    ?></div>
                <?php
                if (count($item->classifiers) > 0) {
                    ?>
                    <ul>
                        <?php
                        foreach ($item->classifiers as $cl) {
                            _renderList($cl, $_this);
                        }
                        ?>
                    </ul>
                <?php
                }
                ?></li>
        <?php
        }

        foreach ($classifierGroup->classifiers as $cl) {
            _renderList($cl, $this);
        }
        ?>
    </ul>
    <h3>Добавить</h3>
    <hr>
<?php
echo CHtml::beginForm();
$this->widget('FormGroup', array('model' => $classifier, 'fieldName' => 'title'));
$this->widget('FormGroup', array('model' => $classifier, 'rootModels' => Classifier::model()->findAllByAttributes(array('group_id' => $classifierGroup->id, 'parent_id' => null)), 'fieldName' => 'parent_id', 'type' => 'listTree', 'value_field' => 'id', 'text_field' => 'title', 'parent_field' => 'parent_id', 'add_root' => true));
$this->widget('FormGroup', array('model' => $classifier, 'fieldName' => 'info'));
$this->widget('FormButton');
echo CHtml::endForm();