<?php
/* @var $this ClassifierController */
/* @var $classifierGroup ClassifierGroup */
echo CHtml::beginForm();
$this->widget('FormGroup', array('model' => $classifierGroup, 'fieldName' => 'name'));
$this->widget('FormGroup', array('model' => $classifierGroup, 'fieldName' => 'slug'));
$this->widget('FormGroup', array('model' => $classifierGroup, 'fieldName' => 'description', 'type' => 'textarea'));
$this->widget('FormButton');
echo CHtml::endForm();