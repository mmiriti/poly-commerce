<?php
/* @var $this ClassifierController */
/* @var $classifierGroup ClassifierGroup */
?>
    <h1>Создать классификатор</h1>
    <hr>
    <div class="well">
        <?php echo CHtml::link('Отмена', array('index'), array('class' => 'btn btn-danger')); ?>
    </div>
<?php
$this->renderPartial('_form', array('classifierGroup' => $classifierGroup));