<?php
/* @var $items CatalogItem[] */
/* @var $current CatalogCategory */
foreach ($items as $item) {
    ?>
    <div class="col-md-2 catalog-item">
        <div class="thumbnail">
            <a href="<?php echo $this->createUrl('edititem', array('id' => $item->id, 'parent_id' => ($current == null ? null : $current->id))); ?>">
                <?php
                if (count($item->galleryItems)) {
                    echo CHtml::image($item->galleryItems[0]->file->thumb_url, $item->name);
                } else {
                    ?>
                    <img src="http://placehold.it/350x250&text=N/a" alt="">
                <?php
                }
                ?>
            </a>

            <div class="caption">
                <p><strong><?php echo $item->name; ?></strong></p>

                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                            data-toggle="dropdown">
                        Операции <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><?php echo CHtml::link('<span class="glyphicon glyphicon-edit"></span> Изменить', array('edititem', 'id' => $item->id, 'parent_id' => ($current == null ? null : $current->id))); ?></li>
                        <li><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span> Удалить', array('deleteitem', 'id' => $item->id, 'parent_id' => ($current == null ? null : $current->id))); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php
}
?>