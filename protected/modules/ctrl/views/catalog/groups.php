<h1>Группы товаров</h1>
<div class="well">
    <div class="btn-group">
        <?php
        echo CHtml::link('<span class="glyphicon glyphicon-arrow-left"></span> Назад в каталог', array('index'), array('class' => 'btn btn-default'));
        echo CHtml::link('Создать группу товаров', array('creategroup'), array('class' => 'btn btn-default'));
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-stripped table-hover">
            <thead>
            <tr>
                <th>Название</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($groups as $g) {
                ?>
                <tr>
                    <td><?php echo $g->name; ?></td>
                    <td>
                        <div class="btn-group">
                            <?php
                            echo CHtml::link('<span class="glyphicon glyphicon-edit"></span>', array('editgroup', 'id' => $g->id), array('class' => 'btn btn-primary btn-xs'));
                            echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deletegroup', 'id' => $g->id), array('class' => 'btn btn-danger btn-xs'));
                            ?>
                        </div>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>