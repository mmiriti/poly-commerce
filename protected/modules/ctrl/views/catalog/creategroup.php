<h1>Свойства группу товаров</h1>
<div class="well">
    <?php echo CHtml::link('<span class="glyphicon glyphicon-arrow-left"></span> Назад к списку групп', array('groups'), array('class' => 'btn btn-default')); ?>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php
        echo CHtml::beginForm();
        $this->widget('FormGroup', array('model' => $group, 'fieldName' => 'name'));
        $this->widget('FormButton', array('title' => 'Далее'));
        echo CHtml::endForm();
        ?>
    </div>
</div>