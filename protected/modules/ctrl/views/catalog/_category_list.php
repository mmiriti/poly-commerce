<?php
/* @var $this CatalogController */

foreach ($categories as $cat) {

    $icon = 'folder-close';
    ?>
    <li<?php
    if ((isset($_GET['parent_id'])) && ($_GET['parent_id'] == $cat->id)) {
        $icon = 'folder-open';
        ?> class="active"<?php
    }
    ?>><?php echo CHtml::link('<span class="glyphicon glyphicon-' . $icon . '"></span> ' . $cat->name, array('index', 'parent_id' => $cat->id));
        if (count($cat->catalogCategories)) {
            ?>
            <ul class="nav nav-pills nav-stacked idented">
                <?php $this->renderPartial('_category_list', array('categories' => $cat->catalogCategories)); ?>
            </ul>
        <?php
        }
        ?></li>
<?php
}