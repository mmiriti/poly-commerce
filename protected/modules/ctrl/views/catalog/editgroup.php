<?php
/* @var $this CatalogController */
?>
<style>
    #formGroup_yw4 {
        display: none;
    }
</style>
<script>
    $(function () {
        $('#Parameter_type').on('change', function () {
            if ($(this).val() == 'classifier') {
                $('#formGroup_yw4').show();
            } else {
                $('#formGroup_yw4').hide();
            }
        });
    });
</script>
<h1>Параметры группы товаров</h1>
<div class="well">
    <?php echo CHtml::link('<span class="glyphicon glyphicon-arrow-left"></span> Назад к списку групп', array('groups'), array('class' => 'btn btn-default')); ?>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php
        echo CHtml::beginForm();
        $this->widget('FormGroup', array('model' => $group, 'fieldName' => 'name'));
        $this->widget('FormButton', array('title' => 'Сохранить'));
        echo CHtml::endForm();
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Параметры товаров</div>
            <div class="panel-body">
                <ul class="list-group">
                    <?php
                    foreach ($group->parameters as $param) {
                        ?>
                        <li class="list-group-item"><?php echo $param->name; ?> <code><?php echo $param->slug; ?></code>
                            (<?php echo Parameter::$types[$param->type]; ?>) <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('editgroup', 'id' => $group->id, 'delete_parameter' => $param->id), array('class' => 'btn btn-xs btn-danger pull-right')); ?></li>
                    <?php
                    }
                    ?>
                </ul>

                <?php
                $parameterList = Parameter::model()->findAll();

                if (count($parameterList) > 0) {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Добавить параметер из существующих</h3>
                            <?php
                            echo CHtml::beginForm();
                            ?>
                            <div class="form-group"><?php
                                echo CHtml::dropDownList('AttachParam', '', CHtml::listData($parameterList, 'id', 'name'), array('class' => 'form-control'));
                                ?></div><?php
                            $this->widget('FormButton', array('title' => 'Добавить параметер'));
                            echo CHtml::endForm();
                            ?>
                        </div>
                    </div>
                <?php
                }
                ?>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Создать новый параметер</h3>
                        <?php
                        echo CHtml::beginForm();
                        $this->widget('FormGroup', array('model' => $new_parameter, 'fieldName' => 'name'));
                        $this->widget('FormGroup', array('model' => $new_parameter, 'fieldName' => 'slug'));
                        $this->widget('FormGroup', array('model' => $new_parameter, 'fieldName' => 'type', 'type' => 'dropdown', 'listData' => Parameter::$types));
                        $this->widget('FormGroup', array('model' => $new_parameter, 'fieldName' => 'classifier_id', 'type' => 'dropdown', 'listData' => CHtml::listData(ClassifierGroup::model()->findAll(), 'id', 'name')));
                        $this->widget('FormButton', array('title' => 'Создать параметер'));
                        echo CHtml::endForm();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">Опции товаров</div>
            <div class="panel-body">

                <ul class="list-group">
                    <?php
                    foreach ($group->options as $option) {
                        ?>
                        <li class="list-group-item">
                            <?php echo $option->name; ?>
                            <span class="pull-right">
                                <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('editgroup', 'id' => $group->id, 'delete_option' => $option->id), array('class' => 'btn btn-danger btn-xs', 'title' => 'Удалить опцию')); ?>
                                <?php echo CHtml::link('<span class="glyphicon glyphicon-list"></span>', array('itemOptions/edit', 'id' => $option->id), array('class' => 'btn btn-info btn-xs', 'title' => 'Редактировать варианты')); ?>
                            </span>
                        </li>
                    <?php
                    }
                    ?>
                </ul>

                <?php
                $optionList = ItemOption::model()->findAll();

                if (count($optionList) > 0) {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Добавить из существующих</h3>
                            <?php
                            echo CHtml::beginForm();
                            ?>
                            <div class="form-group">
                                <?php echo CHtml::dropDownList('AttachOption', '', CHtml::listData($optionList, 'id', 'name'), array('class' => 'form-control')); ?>
                            </div>
                            <?php
                            echo CHtml::endForm();
                            ?>
                        </div>
                    </div>
                <?php
                }
                ?>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Создать новую опцию</h3>
                        <?php
                        echo CHtml::beginForm();
                        $this->widget('FormGroup', array('model' => $newOption, 'fieldName' => 'name'));
                        $this->widget('FormButton', array('title' => 'Создать опцию'));
                        echo CHtml::endForm();
                        ?>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <a class="btn btn-warning btn-block" href="?set_all_options=1">Разрешить все опции для всех
                            товаров в этой группе</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>