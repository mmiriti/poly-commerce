<?php
/* @var $items CActiveDataProvider */
/* @var $current CatalogCategory */
/* @var $group CatalogItemGroup */
?>
<div class="col-xs-12">
    <table class="table table-stripped table-hover">
        <thead>
        <tr>
            <th>Фото</th>
            <th>Наименование</th>
            <th>Описание</th>
            <?php
            foreach (CatalogItem::model()->editableFields() as $field => $type) {
                ?>
                <th><?php echo CatalogItem::model()->getAttributeLabel($field); ?></th>
            <?php
            }
            if ($group == null) {
                ?>
                <th>Группа товаров</th>
            <?php
            } else {
                foreach ($group->parameters as $param) {
                    ?>
                    <th><?php echo $param->name; ?></th>
                <?php
                }
            }
            ?>
            <th>Текущая цена</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($items->data as $item) {
            /* @var $item CatalogItem */
            ?>
            <tr>
                <td><?php
                    if (count($item->galleryItems)) {
                        $image = $item->galleryItems[0]->file;
                        echo CHtml::link(CHtml::image($image->thumb_url, $item->name, array('height' => 30)), $image->url, array('target' => '_blank'));
                    } else {
                        ?>
                        <span class="label label-default">нет</span>
                    <?php
                    }
                    ?></td>
                <td><?php echo $item->name; ?></td>
                <td><?php
                    if ($item->document != null) {
                        echo substr(strip_tags($item->document->html), 0, 100) . '...';
                    }
                    ?></td>
                <?php

                foreach (CatalogItem::model()->editableFields() as $field => $type) {
                    ?>
                    <td><?php echo $item->getAttribute($field); ?></td>
                <?php
                }

                if ($group == null) {
                    ?>
                    <td><?php echo $item->group->name; ?></td>
                <?php
                } else {
                    foreach ($group->parameters as $param) {
                        ?>
                        <td><?php echo $item->parameterValue($param->id); ?></td>
                    <?php
                    }
                }
                ?>
                <td><?php
                    if ($item->price != null) {
                        echo $item->readable_price;
                    } else {
                        ?>
                        <span class="label label-danger">не указана</span>
                    <?php
                    }
                    ?></td>
                <td>
                    <div class="btn-group pull-right">
                        <?php echo CHtml::link('<span class="glyphicon glyphicon-edit"></span>', array('edititem', 'id' => $item->id, 'parent_id' => ($current == null ? null : $current->id), 'group_id' => ($group == null ? null : $group->id)), array('class' => 'btn btn-xs btn-default', 'title' => 'Изменить ' . $item->name)); ?>
                        <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deleteItem', 'id' => $item->id, 'parent_id' => ($current == null ? null : $current->id), 'group_id' => ($group == null ? null : $group->id)), array('class' => 'btn btn-xs btn-danger', 'title' => 'Удалить ' . $item->name)); ?>
                    </div>
                </td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</div>