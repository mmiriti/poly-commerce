<?php
/* @var $this CatalogController */
/* @var $item CatalogItem */

Yii::app()->clientScript->registerScriptFile('/js/item_edit.js', CClientScript::POS_END);
?>
    <div class="well">
        <div class="btn-group">
            <?php
            echo CHtml::link('<span class="glyphicon glyphicon-arrow-left"></span> Назад', array('index', 'parent_id' => (isset($_GET['parent_id']) ? $_GET['parent_id'] : null)), array('class' => 'btn btn-default'));
            ?>
        </div>
    </div>
<?php

echo CHtml::beginForm();

?>
    <div class="row">
        <div class="col-md-3">
            <h2>Категории</h2>
            <?php
            function renderCategoryList($cat, $checkedIDs, $current_category_id)
            {
                ?>
                <ul class="category-select">
                    <?php
                    foreach ($cat as $sub) {
                        ?>
                        <li>
                            <?php
                            echo CHtml::checkBox('CatalogCategory[' . $sub->id . ']', (in_array($sub->id, $checkedIDs) || ($current_category_id == $sub->id)));
                            echo '&nbsp;' . $sub->name;
                            if (count($sub->catalogCategories)) {
                                renderCategoryList($sub->catalogCategories, $checkedIDs, $current_category_id);
                            }
                            ?>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            <?php
            }

            $rootCategories = CatalogCategory::model()->findAllByAttributes(array('parent_id' => null));
            if ($rootCategories != null) {
                $checkedIDs = array();

                foreach ($item->catalogCategories as $c) {
                    $checkedIDs[] = $c->id;
                }

                renderCategoryList($rootCategories, $checkedIDs, $current_category_id);
            }
            ?>
        </div>
        <div class="col-md-9">
            <h2>Свойства товара</h2>
            <?php
            $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'name'));
            $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'group_id', 'type' => 'dropdown', 'listData' => CHtml::listData($groups, 'id', 'name')));
            foreach ($item->editableFields() as $field => $type) {
                $this->widget('FormGroup', array('model' => $item, 'fieldName' => $field, 'type' => $type));
            }
            ?>
            <?php
            if ($item->id) {

                if (count($item->group->parameters)) {
                    ?>
                    <h2 class="folding-header" data-folding-selector="#itemParameters"><a href="#">Параметры товара
                            <span class="caret"></span></a></h2>
                    <div id="itemParameters">
                        <?php

                        if ($item->group) {
                            foreach ($item->group->parameters as $param) {
                                $this->widget('EditParam', array('param' => $param, 'item' => $item));
                            }
                        }

                        ?>
                    </div>
                <?php
                }
                if (count($item->group->options)) {
                    ?>
                    <h2 class="folding-header" data-folding-selector="#itemOptions"><a href="#">Опции товара <span
                                class="caret"></span></a></h2>
                    <div id="itemOptions">
                        <div class="row">
                            <?php
                            foreach ($item->group->options as $option) {
                                ?>
                                <div class="col-md-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <?php echo $option->name; ?>
                                        </div>
                                        <div class="panel-body">
                                            <?php
                                            foreach ($option->itemOptionVariants as $var) {
                                                ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <?php echo CHtml::checkBox('ItemOptionVariant[' . $var->id . ']', $item->isVariantAvailable($var->id)) . ' ' . $var->string; ?>
                                                    </label>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>

                <?php
                $this->widget('EditItemGallery', array('item' => $item));

                ?>
                <h2 class="folding-header" data-folding-selector="#priceDynamics"><a href="#">Динамика цен <span
                            class="caret"></span></a></a></h2>
                <div id="priceDynamics">
                    <?php
                    if (count($item->priceDynamics)) {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Текущая цена</h3>
                            </div>
                            <div class="panel-body" style="font-size: x-large;">
                                <?php echo $item->price->value . ' ' . $item->price->currency->sign; ?>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">История цен</div>
                            <table class="table table-stripped table-hover">
                                <thead>
                                <tr>
                                    <th width="200">Дата</th>
                                    <th>Цена</th>
                                    <th>Комментарий</th>
                                </tr>
                                </thead>
                                <?php
                                foreach ($item->priceDynamics as $price) {
                                    ?>
                                    <tr>
                                        <td><?php echo $price->create_time; ?></td>
                                        <td><?php echo $price->value . ' ' . $price->currency->sign; ?></td>
                                        <td><?php echo $price->comment; ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div>
                    <?php
                    } else {
                        ?>
                        <div class="alert alert-danger">Цена не указана</div>
                    <?php
                    }
                    ?>

                    <div class="panel panel-default">
                        <div class="panel-heading">Новая цена</div>
                        <div class="panel-body">
                            <?php
                            echo CHtml::activeTextField($new_price, 'value', array('class' => 'form-control form-control-inline', 'placeholder' => '0.00'));
                            echo CHtml::activeDropDownList($new_price, 'currency_id', CHtml::listData(Currency::model()->findAll(), 'id', 'sign'), array('class' => 'form-control form-control-inline'));
                            $this->widget('FormGroup', array('model' => $new_price, 'fieldName' => 'comment'));
                            ?>
                        </div>
                    </div>
                </div>
                <h2 class="folding-header" data-folding-selector="#itemDescription"><a href="#">Описание <span
                            class="caret"></span></a></h2>
                <div id="itemDescription">
                    <?php
                    $this->widget('DocumentEditor', array('document' => $item->document));
                    ?>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
<?php

$this->widget('FormButton', array('title' => 'Сохранить'));
echo CHtml::endForm();