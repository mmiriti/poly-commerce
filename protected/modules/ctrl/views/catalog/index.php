<?php
/* @var $this CatalogController */
/* @var $current CatalogCategory */
/* @var $categories CatalogCategory[] */
/* @var $items CActiveDataProvider */
/* @var $group CatalogItemGroup */
$this->fluid = true;
$this->pageTitle = 'Каталог';
?>
<h1>
    <?php
    echo CHtml::link('Каталог', array('index'));

    if ($current != null) {
        ?>
        - <?php echo $current->name; ?>
    <?php
    }
    ?>
</h1>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="well">
            <div class="btn-group">
                <?php
                echo CHtml::link('<span class="glyphicon glyphicon-plus"></span><span class="glyphicon glyphicon-gift"></span> Создать товар', array('createitem', 'current_category_id' => $current == null ? null : $current->id), array('class' => 'btn btn-success'));
                echo CHtml::link('<span class="glyphicon glyphicon-plus"></span><span class="glyphicon glyphicon-folder-open"></span> Создать категорию', array('createcategory'), array('class' => 'btn btn-default'));
                if ($current != null) {
                    ?>
                    <?php echo CHtml::link('<span class="glyphicon glyphicon-edit"></span> Изменить категорию', array('editcategory', 'category_id' => $current->id), array('class' => 'btn btn-info')); ?>
                    <?php
                    if ((count($current->items) == 0) && (count($current->catalogCategories) == 0)) {
                        echo CHtml::link('<span class="glyphicon glyphicon-remove"></span> Удалить категорию', array('deletecategory', 'category_id' => $current->id), array('class' => 'btn btn-danger'));
                    }
                }
                ?>
            </div>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-th-large"></span> Группы товаров', array('groups'), array('class' => 'btn btn-default')); ?>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-link"></span> Классификаторы', array('classifier/index'), array('class' => 'btn btn-default')); ?>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-wrench"></span> Настройки', array('options'), array('class' => 'btn btn-default')); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked">
            <?php
            $this->renderPartial('_category_list', array('categories' => $categories));
            ?>
        </ul>
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    $groups = CatalogItemGroup::model()->findAll();

                    if (count($groups) > 1) {
                        ?>
                        <li<?php if ($group == null) { ?> class="active"<?php } ?>><?php echo CHtml::link('Все группы', array('index', 'parent_id' => $current == null ? null : $current->id, 'group_id' => null)); ?></li>
                    <?php
                    }

                    foreach ($groups as $g) {
                        ?>
                        <li<?php if (($group != null) && ($group->id == $g->id)) { ?> class="active"<?php } ?>><?php echo CHtml::link($g->name, array('index', 'parent_id' => $current == null ? null : $current->id, 'group_id' => $g->id)); ?></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <?php $this->renderPartial('_tableView', array('items' => $items, 'current' => $current, 'group' => $group)); ?>
        </div>
        <div class="row">
            <?php
            if ($items->pagination->pageCount > 1) {
                ?>
                <ul class="pagination pagination-sm">
                    <?php
                    $pagenation_len = 25;

                    $start_page = $items->pagination->currentPage - floor($pagenation_len / 2);

                    if ($start_page < 0) {
                        $start_page = 0;
                    }

                    if ($start_page > 0) {
                        echo CHtml::tag('li', array(), CHtml::link("1", array('index', 'parent_id' => $current == null ? null : $current->id, 'group_id' => $group == null ? null : $group->id, 'page' => 0)));
                        echo '<li class="disabled"><a href="#">...</a></li>';
                    }

                    for ($i = $start_page; $i < $start_page + $pagenation_len; $i++) {
                        if ($i >= $items->pagination->pageCount) {
                            break;
                        }
                        echo CHtml::tag('li', array('class' => ($items->pagination->currentPage == $i) ? 'active' : ''), CHtml::link($i + 1, array('index', 'parent_id' => $current == null ? null : $current->id, 'group_id' => $group == null ? null : $group->id, 'page' => $i)));
                    }

                    if ($i < $items->pagination->pageCount - 1) {
                        echo '<li class="disabled"><a href="#">...</a></li>';
                        echo CHtml::tag('li', array(), CHtml::link($items->pagination->pageCount, array('index', 'parent_id' => $current == null ? null : $current->id, 'group_id' => $group == null ? null : $group->id, 'page' => $items->pagination->pageCount - 1)));
                    }
                    ?>
                </ul>
            <?php
            }
            ?>
        </div>
    </div>
</div>