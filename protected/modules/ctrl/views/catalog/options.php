<?php
/* @var $this CatalogCategory */
/* @var $options Options[] */
?>
<div class="container">
    <h1>Настройки каталога</h1>

    <div class="well">
        <?php echo CHtml::link('Назад в каталог', array('index'), array('class' => 'btn btn-danger')); ?>
    </div>
    <?php
    echo CHtml::beginForm('', 'post', array('class' => 'form-horizontal', 'role' => 'form'));
    foreach ($options as $opt) {
        ?>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label"><?php echo $opt->option_name; ?></label>

            <div class="col-sm-10">
                <?php
                switch ($opt->option_type) {
                    case 'value':
                        break;

                    default:
                        echo CHtml::textField('Option[' . $opt->id . ']', $opt->option_value, array('class' => 'form-control'));
                        break;
                }
                ?>
            </div>
        </div>
    <?php
    }

    $this->widget('FormButton', array('title' => 'Сохранить'));
    echo CHtml::endForm();
    ?>
</div>