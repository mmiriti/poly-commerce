<?php
/* @var $this CatalogController */
/* @var $category CatalogCategory */
?>
    <div class="well">
        <?php echo CHtml::link('<span class="glyphicon glyphicon-arrow-left"></span> Назад', array('index'), array('class' => 'btn btn-default')); ?>
    </div>
<?php
echo CHtml::beginForm('', 'post', array('enctype' => 'multipart/form-data'));
$this->widget('FormGroup', array('model' => $category, 'fieldName' => 'name'));
$this->widget('FormGroup', array('model' => $category, 'fieldName' => 'slug'));
?>
    <div class="form-group">
        <label for=""><?php echo $category->attributeLabels()['parent_id']; ?></label>
        <?php
        $this->widget('CategoryDropDown', array('fieldName' => 'CatalogCategory[parent_id]', 'selected' => $category->parent_id));
        ?>
    </div>
<?php
$this->widget('FormGroup', array('model' => $category, 'fieldName' => 'surcharge', 'hint' => 'Дробное значение. Например 1.0 - цена без изменений, 1.05 - +5% к стоимости товара. Также можно оставить пустое значение если наценка не будет использоваться.'));
if ($category->file != null) {
    ?>
    <div class="form-group">
        <?php echo CHtml::image($category->file->url, $category->name, array('class' => 'thumbnail')); ?>
    </div>
<?php
}
?>
    <div class="form-group">
        <label for="file">Загрузить новое изображение</label>
        <input class="form-control" type="file" name="image_file" id="file">
    </div>
    <h2>Описание</h2>
<?php
$this->widget('DocumentEditor', array('document' => $category->document));
$this->widget('FormButton', array('title' => $category->id ? 'Сохранить' : 'Создать'));
echo CHtml::endForm();
?>