<h1>Создать товар</h1>
<?php

if(count($groups))
{
	$this->renderPartial('_item', array(
		'item' => $item, 
		'groups' => $groups, 
		'current_category_id' => $current_category_id)
	);
}else{
	?>
	<div class="alert alert-danger">
		Прежде чем создавать товары необходимо <?php echo CHtml::link('создать хотя-бы одну группу товаров', array('creategroup')); ?>
	</div>
	<?php
}