<?php
$this->pageTitle = 'Создать форму';
?>
<h1>Создать форму</h1>
<hr/>
<div class="row">
    <div class="col-md-12">
        <div class="well">
            <?php echo CHtml::link('К списку форм', array('index'), array('class' => 'btn btn-default')); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php $this->renderPartial('_form', array('form' => $newForm)); ?>
    </div>
</div>
