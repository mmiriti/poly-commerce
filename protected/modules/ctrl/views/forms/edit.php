<?php
?>
<h1>Форма "<?php echo $form->name; ?>"</h1>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="well">
            <?php echo CHtml::link('К списку форм', array('index'), array('class' => 'btn btn-default')); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?php $this->renderPartial('_form', array('form' => $form)); ?>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Поля формы</div>
                    <div class="panel-body">
                        <ul class="list-group">
                            <?php foreach ($form->formFields as $field) { ?>
                                <li class="list-group-item">
                                    <?php echo $field->name; ?> (<code><?php echo $field->slug; ?></code>)
                                    <?php if ($field->required) { ?>
                                        <span class="required">*</span>
                                    <?php } ?>
                                    <span class="pull-right">
                                        <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deleteField', 'id' => $field->id), array('class' => 'btn btn-danger btn-xs')); ?>
                                    </span>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Добавить поле</div>
                    <div class="panel-body">
                        <?php echo CHtml::beginForm(); ?>
                        <?php $this->widget('FormGroup', array('model' => $newField, 'fieldName' => 'name')); ?>
                        <?php $this->widget('FormGroup', array('model' => $newField, 'fieldName' => 'slug')); ?>
                        <?php $this->widget('FormGroup', array('model' => $newField, 'fieldName' => 'required', 'type' => 'dropdown', 'listData' => array(0 => 'нет', 1 => 'да'))); ?>
                        <?php $this->widget('FormButton'); ?>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>