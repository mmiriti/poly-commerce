<?php
$this->pageTitle = 'Формы';
?>
<h1>Формы</h1>
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="well">
            <?php echo CHtml::link('Создать форму', array('create'), array('class' => 'btn btn-primary')); ?>
        </div>
    </div>
</div>
<div class="row">

    <?php
    foreach ($forms as $form) {
        ?>
        <div class="col-md-4">
            <div class="panel<?php if(count($form->formFillsNew) > 0){?> panel-primary<?php } else { ?> panel-default<?php } ?>">
                <div class="panel-heading"><?php echo $form->name; ?></div>
                <table class="table table-hover">
                    <tr>
                        <td>Новых ответов</td>
                        <td><span class="badge"><?php echo count($form->formFillsNew); ?></span></td>
                    </tr>
                    <tr>
                        <td>Всего ответов</td>
                        <td><span class="badge"><?php echo count($form->formFills); ?></span></td>
                    </tr>
                </table>
                <div class="panel-footer">
                    <?php echo CHtml::link('Просмотр заполненных', array('view', 'id' => $form->id), array('class' => 'btn btn-default')); ?>
                    <?php echo CHtml::link('Редактировать', array('edit', 'id' => $form->id), array('class' => 'btn btn-default')); ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>
