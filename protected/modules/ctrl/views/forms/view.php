<h1>Заполненные формы "<?php echo $form->name; ?>"</h1>
<hr/>
<div class="well">
    <?php echo CHtml::link('К списку форм', array('index'), array('class' => 'btn btn-default')); ?>
</div>
<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#new" data-toggle="tab">Новые</a></li>
    <li><a href="#archive" data-toggle="tab">Все</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="new">
        <?php $this->renderPartial('_list', array('form' => $form, 'fills' => $form->formFillsNew)); ?>
    </div>
    <div class="tab-pane" id="archive">
        <?php $this->renderPartial('_list', array('form' => $form, 'fills' => $form->formFills)); ?>
    </div>
</div>