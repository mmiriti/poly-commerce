<table class="table table-hover">
    <thead>
    <tr>
        <th></th>
        <?php
        foreach ($form->formFields as $field) {
            ?>
            <th><?php echo $field->name; ?></th>
        <?php
        }
        ?>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($fills as $fill) {
        ?>
        <tr<?php if($fill->is_new) { ?> class="success"<?php } ?>>
            <td><?php echo Yii::app()->dateFormatter->format("d MMM, H:m", strtotime($fill->fill_time)); ?></td>
            <?php
            foreach ($form->formFields as $field_inner) {
                ?>
                <td><?php echo $fill->getFieldValue($field_inner->slug); ?></td>
            <?php
            }
            ?>
            <td>
                <div class="btn-group pull-right">
                    <?php
                    if ($fill->is_new) {
                        echo CHtml::link('<span class="glyphicon glyphicon glyphicon-ok"></span>', array('archiveFill', 'id' => $fill->id, 'form_id' => $form->id), array('class' => 'btn btn-default btn-xs'));
                    }
                    ?>
                    <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deleteFill', 'id' => $fill->id, 'form_id' => $form->id), array('class' => 'btn btn-danger btn-xs')); ?>
                </div>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>