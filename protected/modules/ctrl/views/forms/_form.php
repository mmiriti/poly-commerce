<div class="panel panel-default">
    <div class="panel-heading">Основные параметры</div>
    <div class="panel-body">
        <?php echo CHtml::beginForm(); ?>
        <?php $this->widget('FormGroup', array('model' => $form, 'fieldName' => 'name')); ?>
        <?php $this->widget('FormGroup', array('model' => $form, 'fieldName' => 'slug')); ?>
        <?php $this->widget('FormGroup', array('model' => $form, 'fieldName' => 'message', 'type' => 'textarea', 'htmlOptions' => array('rows' => 10, 'class' => 'wysiwyg'))); ?>
        <?php $this->widget('FormButton'); ?>
        <?php echo CHtml::link('Отмена', array('index'), array('class' => 'btn btn-danger')); ?>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>