<?php
/* @var $this MenusController */
/* @var $menu Menu */

?>
<h1><?php echo $this->pageTitle; ?></h1>
<hr>
<div class="well">
    <?php echo CHtml::link('Назад', array('index'), array('class' => 'btn btn-default')); ?>
</div>
<div class="panel panel-default">
    <div class="panel-heading">Создать меню</div>
    <div class="panel-body">
        <?php $this->renderPartial('_menu_form', array('menu' => $menu)); ?>
    </div>
</div>
