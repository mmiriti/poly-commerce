<?php
/* @var $this MenusController */
/* @var $menus [] Menu */
$this->pageTitle = 'Меню';
?>
    <h1><?php echo $this->pageTitle; ?></h1>
    <hr>
    <div class="well">
        <?php echo CHtml::link('Создать меню', array('create'), array('class' => 'btn btn-default')); ?>
    </div>
<?php

if (count($menus) > 0) {
    ?>
    <ul class="list-group">
        <?php
        foreach ($menus as $menu) {
            ?>
            <li class="list-group-item"><?php echo $menu->name; ?> (<code><?php echo $menu->slug; ?></code>)
                <span class="pull-right">
                    <?php echo CHtml::link('<span class="glyphicon glyphicon-list"></span>', array('items', 'id' => $menu->id), array('class' => 'btn btn-xs btn-primary', 'title' => 'Пункты меню')); ?>
                </span>
            </li>
        <?php
        }
        ?>
    </ul>
<?php
} else {
    ?>
    <div class="alert alert-warning">
        Не создано ни одного меню
    </div>
<?php
}