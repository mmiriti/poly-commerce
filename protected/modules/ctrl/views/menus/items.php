<?php
/* @var $this MenusController */
/* @var $menu Menu */
/* @var $newItem MenuItem */

$this->pageTitle = 'Пункты меню "' . $menu->name . '"';
?>
<h1><?php echo $this->pageTitle; ?></h1>
<hr>
<div class="well">
    <?php echo CHtml::link('Назад', array('index'), array('class' => 'btn btn-default')); ?>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <?php echo CHtml::beginForm(); ?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Позиция</th>
                <th>Заголовок</th>
                <th>Действие</th>
                <th>Get параметры</th>
                <th>Ссылка</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($menu->menuItems as $item) {
                ?>
                <tr>
                    <td><?php echo CHtml::textField('MenuItems[' . $item->id . '][position]', $item->position, array('size' => 2, 'class' => 'form-control input-xs')); ?></td>
                    <td><?php echo CHtml::textField('MenuItems[' . $item->id . '][title]', $item->title, array('size' => 2, 'class' => 'form-control input-xs')); ?></td>
                    <td><?php echo CHtml::textField('MenuItems[' . $item->id . '][action]', $item->action, array('size' => 2, 'class' => 'form-control input-xs')); ?></td>
                    <td><?php echo CHtml::textArea('MenuItems[' . $item->id . '][get_data]', $item->get_data, array('class' => 'form-control')); ?></td>
                    <td><?php echo CHtml::textField('MenuItems[' . $item->id . '][href]', $item->href, array('size' => 2, 'class' => 'form-control input-xs')); ?></td>
                    <td><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deleteItem', 'id' => $item->id), array('class' => 'btn btn-sm btn-danger', 'title' => 'Удалить')); ?></td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
        <button type="submit" class="btn btn-primary">Сохранить</button>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Добавить пункт</div>
    <div class="panel-body">
        <?php echo CHtml::beginForm(); ?>
        <?php $this->widget('FormGroup', array('model' => $newItem, 'fieldName' => 'position')); ?>
        <?php $this->widget('FormGroup', array('model' => $newItem, 'fieldName' => 'title')); ?>
        <?php $this->widget('FormGroup', array('model' => $newItem, 'fieldName' => 'action')); ?>
        <?php $this->widget('FormGroup', array('model' => $newItem, 'fieldName' => 'get_data', 'type' => 'textarea')); ?>
        <?php $this->widget('FormGroup', array('model' => $newItem, 'fieldName' => 'href')); ?>
        <?php $this->widget('FormButton'); ?>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>