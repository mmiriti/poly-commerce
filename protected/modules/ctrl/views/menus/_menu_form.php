<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 7/5/14
 * Time: 2:02 PM
 */
echo CHtml::beginForm();
$this->widget('FormGroup', array('model' => $menu, 'fieldName' => 'name'));
$this->widget('FormGroup', array('model' => $menu, 'fieldName' => 'slug'));
$this->widget('FormButton');
echo CHtml::endForm();