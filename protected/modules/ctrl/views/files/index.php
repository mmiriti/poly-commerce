<?php
/* @var $this FilesController */
/* @var $files File[] */
?>
<style>
    .thumbnail {
        text-align: center;
    }

    .thumbnail .file-icon {
        font-size: 80pt;
        display: block;
    }

    .file-item {
        min-height: 200px;
    }
</style>
<h1>Файлы</h1>
<div class="well">
    <form action="" method="post" enctype="multipart/form-data" style="display: inline">
        <input type="file" name="upload_file" style="display: inline"> <input class="btn btn-primary" type="submit" value="Загрузить">
    </form>
</div>
<div class="row">
    <?php
    foreach ($files as $file) {
        ?>
        <div class="col-xs-2 file-item">
            <div class="thumbnail">
                <a href="<?php echo $this->createUrl('view', array('id' => $file->id)); ?>">
                    <?php
                    if ($file->isImage()) {
                        echo CHtml::image($file->thumb_url, '');
                    } else {
                        ?>
                        <span class="glyphicon glyphicon-file file-icon"></span>
                    <?php
                    } ?>
                    <p><?php echo $file->original_name; ?></p>
                </a>
            </div>
        </div>
    <?php
    }
    ?>
</div>