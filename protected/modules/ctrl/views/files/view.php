<?php
/* @var $this FilesController */
/* @var $file File */
?>
<h1><?php echo $file->original_name; ?></h1>
<div class="well">
    <?php echo CHtml::link('Назад', array('index', 'folder_id' => $file->folder_id), array('class' => 'btn btn-danger')); ?>
</div>
<div class="row">
    <div class="col-xs-4">
        <div class="thumbnail">
            <?php
            if ($file->isImage()) {
                echo CHtml::link(CHtml::image($file->url, $file->original_name), $file->url, array('target' => '_blank'));
            }
            ?>
            <form>
                <input type="text" value="<?php echo $file->url; ?>" class="form-control">
            </form>
        </div>
    </div>
    <div class="col-xs-8">
        <table class="table table-hover table-stripped">
            <tr>
                <td>Дата и время загрузки</td>
                <td><?php echo Yii::app()->dateFormatter->format("d MMM y, H:m", strtotime($file->create_time)); ?></td>
            </tr>
            <tr>
                <td>Имя файла призагрузке</td>
                <td><?php echo $file->original_name; ?></td>
            </tr>
            <tr>
                <td>Размер файла</td>
                <td><?php echo $file->size; ?> байт</td>
            </tr>
            <tr>
                <td>MIME-type</td>
                <td><?php echo $file->mime; ?></td>
            </tr>
            <tr>
                <td>Абсолютный URL файла</td>
                <td><?php
                    $url = 'http://' . $_SERVER['HTTP_HOST'] . $file->url;
                    echo CHtml::link($url, $url);
                    ?></td>
            </tr>
        </table>
    </div>
</div>