<?php
/* @var $this CurrencyController */
/* @var $currencies Currency[] */
/* @var $exchanges CurrencyExchange[] */
/* @var $new_exchange CurrencyExchange */
?>
<h1>Валюты</h1>
<hr>
<div class="well">
    <?php echo CHtml::link('Создать', array('create'), array('class' => 'btn btn-primary')); ?>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th><?php echo Currency::model()->getAttributeLabel('full_name'); ?></th>
        <th><?php echo Currency::model()->getAttributeLabel('sign'); ?></th>
        <th><?php echo Currency::model()->getAttributeLabel('alpha_3'); ?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($currencies as $currency) {
        ?>
        <tr>
            <td><?php echo $currency->full_name; ?></td>
            <td><?php echo $currency->sign; ?></td>
            <td><?php echo $currency->alpha_3; ?></td>
            <td>
                <div class="btn-group pull-right"><?php

                    ?></div>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>
<h2>Курсы валют</h2>
<table class="table table-hover">
    <thead>
    <tr>
        <th><?php echo CurrencyExchange::model()->getAttributeLabel('currency_from_id'); ?></th>
        <th><?php echo CurrencyExchange::model()->getAttributeLabel('currency_to_id'); ?></th>
        <th><?php echo CurrencyExchange::model()->getAttributeLabel('ratio'); ?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    echo CHtml::beginForm();
    foreach ($exchanges as $exchange) {
        ?>
        <tr>
            <td><?php echo $exchange->currencyFrom->full_name; ?></td>
            <td><?php echo $exchange->currencyTo->full_name; ?></td>
            <td><?php echo CHtml::textField('UpdateRatio[' . $exchange->id . ']', $exchange->ratio, array('class' => 'form-control')); ?></td>
            <td><?php
                echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('index', 'deleteExchange' => $exchange->id), array('class' => 'btn btn-sm btn-danger', 'title' => 'Удалить'));
                ?></td>
        </tr>
    <?php
    }
    ?>
    <tr>
        <td colspan="4">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </td>
    </tr>
    <?php
    echo CHtml::endForm();
    ?>
    </tbody>
    <?php echo CHtml::beginForm(); ?>
    <thead>
    <tr>
        <th colspan="3">Добавить конвертацию</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if ($new_exchange->hasErrors()) {
        ?>
        <tr>
            <td colspan="4">
                <div class="alert alert-danger">
                    <?php echo CHtml::errorSummary($new_exchange); ?>
                </div>
            </td>
        </tr>
    <?php
    }
    ?>
    <tr>
        <td><?php echo CHtml::activeDropDownList($new_exchange, 'currency_from_id', CHtml::listData(Currency::model()->findAll(), 'id', 'full_name'), array('class' => 'form-control')); ?></td>
        <td><?php echo CHtml::activeDropDownList($new_exchange, 'currency_to_id', CHtml::listData(Currency::model()->findAll(), 'id', 'full_name'), array('class' => 'form-control')); ?></td>
        <td><?php echo CHtml::activeTextField($new_exchange, 'ratio', array('class' => 'form-control')); ?></td>
        <td>
            <button type="submit" class="btn btn-primary">Добавить</button>
        </td>
    </tr>
    </tbody>
    <?php echo CHtml::endForm(); ?>
</table>