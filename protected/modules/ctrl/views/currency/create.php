<?php
/* @var $this CurrencyController */
/* @var $currency Currency */
?>
<h1>Создать валюту</h1>
<hr>
<div class="well">
    <?php echo CHtml::link('Отмена', array('index'), array('class' => 'btn btn-danger')); ?>
</div>
<?php
$this->renderPartial('_form', array('currency' => $currency));
?>
