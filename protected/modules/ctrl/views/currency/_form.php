<?php
/* @var $currency Currency */
?>
<div class="panel panel-default">
    <div class="panel-body">
        <?php
        echo CHtml::beginForm();
        $this->widget('FormGroup', array('model' => $currency, 'fieldName' => 'full_name'));
        $this->widget('FormGroup', array('model' => $currency, 'fieldName' => 'sign'));
        $this->widget('FormGroup', array('model' => $currency, 'fieldName' => 'alpha_3'));
        $this->widget('FormButton');
        echo CHtml::endForm();
        ?>
    </div>
</div>