<h1>Редактирование опции</h1>
<hr/>
<div class="panel panel-default">
    <div class="panel-heading">Название опции</div>
    <div class="panel-body">
        <?php
        echo CHtml::beginForm();
        $this->widget('FormGroup', array('model' => $itemOption, 'fieldName' => 'name'));
        $this->widget('FormButton');
        echo CHtml::endForm();
        ?>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Варианты</div>
    <?php echo CHtml::beginForm(); ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th><?php echo ItemOptionVariant::model()->attributeLabels()['string']; ?></th>
            <th><?php echo ItemOptionVariant::model()->attributeLabels()['value']; ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($itemOption->itemOptionVariants as $variant) {
            ?>
            <tr>
                <td><?php echo $variant->string; ?></td>
                <td><?php echo $variant->value; ?></td>
                <td>
                    <div class="btn-group">
                        <?php
                        echo CHtml::link('<span class="glyphicon glyphicon-ok-circle"></span>', array('allowVariant', 'id' => $variant->id), array('class' => 'btn btn-success btn-xs', 'title' => 'Показать этот вариант для всех товаров, которые используют данную опцию'));
                        echo CHtml::link('<span class="glyphicon glyphicon-ban-circle"></span>', array('banVariant', 'id' => $variant->id), array('class' => 'btn btn-warning btn-xs', 'title' => 'Скрыть этот вариант для всех товаров, которые используют данную опцию'));
                        if (count($variant->orderItemOptionVariants) == 0) {
                            echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deleteVariant', 'id' => $variant->id), array('class' => 'btn btn-danger btn-xs', 'title' => 'Удалить вариант'));
                        }
                        ?>
                    </div>
                </td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    <?php echo CHtml::endForm(); ?>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Добавить вариант</div>
    <div class="panel-body">
        <?php
        echo CHtml::beginForm();
        $this->widget('FormGroup', array('model' => $newVariant, 'fieldName' => 'string'));
        $this->widget('FormGroup', array('model' => $newVariant, 'fieldName' => 'value'));
        $this->widget('FormButton');
        echo CHtml::endForm();
        ?>
    </div>
</div>