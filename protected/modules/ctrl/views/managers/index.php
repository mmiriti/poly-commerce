<?php
/* @var $this ManagersController */
/* @var $managers User[] */
?>
    <h1>Менеджеры</h1>
<div class="well">
    <?php
    echo CHtml::link('<span class="glyphicon glyphicon-plus"></span> Добавить менеджера', array('create'), array('class' => 'btn btn-primary'));
    ?>
</div>
<?php
foreach ($managers as $manager) {
    echo CHtml::link('<span class="glyphicon glyphicon-user"></span> ' . $manager->fullName, array('update', 'id' => $manager->id));
}