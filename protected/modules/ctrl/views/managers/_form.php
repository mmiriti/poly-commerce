<?php
/* @var $this ManagersController */
/* @var $manager User */
?>
<div class="well">
    <?php echo CHtml::link('Отмена', array('index'), array('class' => 'btn btn-danger')); ?>
</div>
<?php
echo CHtml::beginForm();
$this->widget('FormGroup', array('model' => $manager, 'fieldName' => 'email', 'type' => 'email'));
$this->widget('FormGroup', array('model' => $manager, 'fieldName' => 'name'));
$this->widget('FormGroup', array('model' => $manager, 'fieldName' => 'password', 'type' => 'password'));
$this->widget('FormGroup', array('model' => $manager, 'fieldName' => 'password_again', 'type' => 'password'));
$this->widget('FormButton');
echo CHtml::endForm();