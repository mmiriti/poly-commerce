<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo Options::get('site_name', Yii::app()->name, 'Название сайта') . ' - Панель управления - ' . $this->pageTitle; ?></title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/ctrl.css">
    <link rel="stylesheet" href="/js/jquery-ui/jquery-ui.min.css">
    <script src="/js/jquery-2.1.1.min.js"></script>
    <script src="/js/jquery-ui/jquery-ui.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script src="/js/ckeditor/ckeditor.js"></script>
    <script src="/js/ckeditor/adapters/jquery.js"></script>
    <script src="/js/ctrl.js"></script>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php echo CHtml::link(Options::get('site_name', Yii::app()->name, 'Название сайта'), array('/ctrl'), array('class' => 'navbar-brand')); ?>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php
            $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->adminMenuItems,
                    'htmlOptions' => array('class' => 'nav navbar-nav'),
                    'encodeLabel' => false
                )
            );
            ?>
            <?php
            $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array(
                            'label' => '<span class="glyphicon glyphicon-cog"></span> <b class="caret"></b>',
                            'url' => '#',
                            'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                            'submenuOptions' => array('class' => 'dropdown-menu'),
                            'items' => array(
                                array('label' => '<span class="glyphicon glyphicon-user"></span> Пользователи', 'url' => array('/ctrl/users/index')),
                                array('label' => '<span class="glyphicon glyphicon-wrench"></span> Прием заказов', 'url' => array('/ctrl/options/orders')),
                                array('label' => '<span class="glyphicon glyphicon-usd"></span> Валюты', 'url' => array('/ctrl/currency/index')),
                                array('label' => '<span class="glyphicon glyphicon-list"></span> Меню', 'url' => array('/ctrl/menus/index')),
                                array('label' => '<span class="glyphicon glyphicon-picture"></span> Тема', 'url' => array('/ctrl/theme/index')),
                                array('label' => '<span class="glyphicon glyphicon-wrench"></span> Опции', 'url' => array('/ctrl/options/index')),
                                array('label' => '<span class="glyphicon glyphicon-file"></span> Шаблоны писем', 'url' => array('/ctrl/letterTemplates/index')),
                                array('label' => '', 'itemOptions' => array('class' => 'divider')),
                                array('label' => '<span class="glyphicon glyphicon-question-sign"></span> О Poly Commerce', 'url' => '/ctrl/about/index')
                            )
                        ),
                        array('label' => '<span class="glyphicon glyphicon-globe"></span>', 'url' => '/'),
                        array(
                            'label' => '<span class="glyphicon glyphicon-user"></span> <b class="caret"></b>',
                            'url' => '#',
                            'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                            'submenuOptions' => array('class' => 'dropdown-menu'),
                            'items' => array(
                                array('label' => User::current()->email, 'url' => array('/user/home/index')),
                                array('label' => 'Выход', 'url' => array('/user/auth/logout'))
                            )
                        )
                    ),
                    'htmlOptions' => array('class' => 'nav navbar-nav navbar-right'),
                    'encodeLabel' => false
                )
            );
            ?>
        </div>
    </div>
</nav>
<?php
if ($this->fluid) {
    ?>
    <div class="container-fluid">
        <?php
        echo $content;
        ?>
    </div>
<?php
} else {
    ?>
    <div class="container">
        <?php echo $content; ?>
    </div>
<?php
}
?>
<hr/>
<div class="container">
    <footer>
    </footer>
</div>
</body>
</html>