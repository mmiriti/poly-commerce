<?php
$this->pageTitle = 'Редактировать раздел - ' . $item->name;
?>
<h1><?php echo $this->pageTitle; ?></h1>
<hr/>
<div class="well">
    <?php echo CHtml::link('Назад', array('index'), array('class' => 'btn btn-default')); ?>
</div>
<?php $this->renderPartial('_form', array('item' => $item)); ?>
<h2>Содержание</h2>
<hr/>
<?php
foreach ($item->structureBlocks as $block) {
    $this->renderPartial('_editblock', array('block' => $block));
}
?>
<div class="panel panel-default">
    <div class="panel-heading">Добавить блок</div>
    <div class="panel-body">
        <?php echo CHtml::beginForm(); ?>
        <?php $this->widget('FormGroup', array('model' => $newBlock, 'fieldName' => 'title')); ?>
        <?php $this->widget('FormGroup', array('model' => $newBlock, 'fieldName' => 'type', 'type' => 'dropdown', 'listData' => array(
            'text' => 'Текстовый блок',
            'gallery' => 'Галерея',
            'news' => 'Новости',
        ))); ?>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>