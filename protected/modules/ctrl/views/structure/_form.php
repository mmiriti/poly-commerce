<div class="panel panel-default">
    <div class="panel-body">
        <?php echo CHtml::beginForm(); ?>
        <?php $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'name')); ?>
        <?php $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'slug')); ?>
        <?php $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'position')); ?>
        <?php $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'layout', 'type' => 'dropdown', 'listData' => array(null => '- по умолчанию -') + Theme::current()->info['layouts'])); ?>
        <?php $this->widget('FormButton'); ?>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>