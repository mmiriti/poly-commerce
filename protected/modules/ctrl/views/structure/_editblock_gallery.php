<div class="row">
    <?php
    if (count($block->galleryItems)) {
        foreach ($block->galleryItems as $item) {
            ?>
            <div class="col-md-3">
                <div class="thumbnail">
                    <img class="img-responsive" src="<?php echo $item->file->url; ?>" alt=""/>
                </div>
            </div>
        <?php
        }
    } else {
        ?>
        <div class="col-md-10 col-md-offset-1">
            <div class="alert alert-warning">
                Нет фотографий
            </div>
        </div>
    <?php
    }
    ?>
</div>