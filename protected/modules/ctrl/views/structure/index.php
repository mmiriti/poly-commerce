<?php
$this->pageTitle = 'Структура сайта';

function renderItem($item)
{
    ?>
    <li><strong><?php echo $item->name; ?></strong> (<code><?php echo $item->slug; ?></code>)
        <span>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-edit"></span>', array('edit', 'id' => $item->id), array('class' => 'btn btn-primary btn-xs', 'title' => 'Редактировать содержание "' . $item->name . '"')); ?>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-plus"></span>', array('create', 'parent_id' => $item->id), array('class' => 'btn btn-default btn-xs', 'title' => 'Добавить дочерний элемент под "' . $item->name . '"')); ?>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('delete', 'id' => $item->id), array('class' => 'btn btn-danger btn-xs', 'title' => 'Удалить "' . $item->name . '"')); ?>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-globe"></span> Просмотр', array('/site/structure', 'slug' => $item->slug), array('target' => '_blank')); ?>
        </span>
        <?php
        if (count($item->structureItems) > 0) {
            ?>
            <ul class="nav nav-pills nav-stacked idented">
                <?php
                foreach ($item->structureItems as $child) {
                    renderItem($child);
                }
                ?>
            </ul>
        <?php
        }
        ?>
    </li>
<?php
}

?>
    <h1>Структура сайта</h1>
    <hr/>
    <div class="well">
        <?php echo CHtml::link('Создать раздел', array('create'), array('class' => 'btn btn-primary')); ?>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">
                <?php
                foreach ($items as $item) {
                    renderItem($item);
                }
                ?>
            </ul>
        </div>
    </div>
    <hr>
    <h2>Общие блоки</h2>
    <div class="well">
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                Добавить блок <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><?php echo CHtml::link('Текстовый блок', array('createBlock', 'type' => 'text')); ?></li>
                <li><?php echo CHtml::link('Галерея', array('createBlock', 'type' => 'gallery')); ?></li>
                <li><?php echo CHtml::link('Новости', array('createBlock', 'type' => 'news')); ?></li>
            </ul>
        </div>
    </div>
<?php
foreach ($commonBlocks as $block) {
    $this->renderPartial('_editblock', array('block' => $block));
}
