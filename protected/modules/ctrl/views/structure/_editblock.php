<div class="panel panel-default">
    <div class="panel-heading" id="block<?php echo $block->id; ?>"><?php echo $block->title; ?>
        (<code><?php echo $block->slug; ?></code>)
        <span class="pull-right">
                <?php echo CHtml::link('<span class="glyphicon glyphicon-edit"></span>', array('/ctrl/blockEditor/edit/' . $block->type, 'id' => $block->id), array('class' => 'btn btn-info btn-xs', 'title' => 'Редактировать блок')); ?>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('deleteBlock', 'id' => $block->id), array('class' => 'btn btn-danger btn-xs', 'title' => 'Удалить блок')); ?>
            <span class="btn-group">
                <?php echo CHtml::link('<span class="glyphicon glyphicon-arrow-up"></span>', array('upBlock', 'id' => $block->id), array('class' => 'btn btn-default btn-xs', 'title' => 'Выше')); ?>
                <?php echo CHtml::link('<span class="glyphicon glyphicon-arrow-down"></span>', array('downBlock', 'id' => $block->id), array('class' => 'btn btn-default btn-xs', 'title' => 'Ниже')); ?>
                </span>
            </span>
    </div>
    <div class="panel-body">
        <?php
        try {
            $this->renderPartial('_editblock_' . $block->type, array('block' => $block));
        } catch (Exception $e) {
            ?>
            - нет данных для отображения -
        <?php
        }
        ?>
    </div>
</div>