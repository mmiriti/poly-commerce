<?php
$this->pageTitle='Создать раздел';
?>
<h1>Создать раздел</h1>
<hr/>
<div class="well">
    <?php echo CHtml::link('Назад', array('index'), array('class' => 'btn btn-default')); ?>
</div>
<?php
$this->renderPartial('_form', array('item' => $item));
?>