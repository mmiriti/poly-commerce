<?php
/* @var $this NewsController */
/* @var $model NewsItem */
/* @var $document Document */
?>

<h1>Добавить новость</h1>
<hr>

<?php $this->renderPartial('_form', array('model' => $model, 'document' => $document)); ?>