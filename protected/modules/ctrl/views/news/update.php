<?php
/* @var $this NewsController */
/* @var $model NewsItem */
?>

<h1>Редактировать <?php echo $model->title; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'document' => $model->document)); ?>