<?php
/* @var $this NewsController */
/* @var $model NewsItem */
/* @var $form CActiveForm */
/* @var $document Document */
?>
<div class="well">
    <?php echo CHtml::link('Все новости', array('index'), array('class' => 'btn btn-default')); ?>
</div>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'news-item-form',
    'enableAjaxValidation' => false,
));

echo $form->errorSummary($model);

$this->widget('FormGroup', array('model' => $model, 'fieldName' => 'publish_date_time', 'type' => 'date'));
$this->widget('FormGroup', array('model' => $model, 'fieldName' => 'title'));
$this->widget('DocumentEditor', array('document' => $document));
$this->widget('FormButton');
?>


<?php $this->endWidget(); ?>