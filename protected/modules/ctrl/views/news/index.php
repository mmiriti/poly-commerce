<?php
/* @var $this NewsController */
/* @var $news NewsItem[] */
?>
<script type="text/javascript">
    $(function() {
        $('.load-body').bind("click", function(e) {
            e.preventDefault();
            $(this).parent().html("Загрузка...");
            var newId = $(this).attr('data-news-id');
            $('#body-container-' + newId).load('<?php echo $this->createUrl('view'); ?>?id=' + newId);
        });
    });
</script>
<h1>Новости</h1>
<hr>
<div class="well">
    <?php echo CHtml::link('Создать новость', array('create'), array('class' => 'btn btn-default')); ?>
</div>
<div class="row">
    <div class="col-md-12">
        <?php
        foreach ($news as $item) {
            ?>
            <div class="panel panel-default">
                <div class="panel-heading"><em><?php echo Yii::app()->dateFormatter->format("d MMMM y", strtotime($item->publish_date_time)); ?></em>: <strong><?php echo $item->title; ?></strong></div>
                <div class="panel-body" id="body-container-<?php echo $item->id; ?>"><a href="#" class="load-body" data-news-id="<?php echo $item->id; ?>"><span class="glyphicon glyphicon-cloud-upload"></span> Загрузить содержимое</a></div>
                <div class="panel-footer">
                    <?php echo CHtml::link('Редактировать', array('update', 'id' => $item->id), array('class' => 'btn btn-primary')); ?>
                    <?php echo CHtml::link('Удалить', array('delete', 'id' => $item->id), array('class' => 'btn btn-danger')); ?>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>
