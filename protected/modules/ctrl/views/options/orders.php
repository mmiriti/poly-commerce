<?php
/* @var $this OptionsController */
/* @var $orderDataField OrderDataField */
/* @var $customerDataFields CustomerDataField[] */
/* @var $customerDataField CustomerDataField */
?>
<h1>Прием заказов</h1>
<hr>
<div class="row">
    <div class="col-md-6">
        <h2 id="order">Свойства заказа</h2>

        <div class="row">
            <div class="col-md-12">
                <ul class="list-group">
                    <?php foreach ($orderDataFields as $field) { ?>
                        <li class="list-group-item"><?php echo $field->name; ?><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('orders', 'delete-order-field' => $field->id), array('class' => 'pull-right')); ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Новое свойство</div>
                    <div class="panel-body">
                        <?php echo CHtml::beginForm(); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderDataField, 'fieldName' => 'name')); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderDataField, 'fieldName' => 'hint')); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderDataField, 'fieldName' => 'mandatory')); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderDataField, 'fieldName' => 'slug')); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderDataField, 'fieldName' => 'type', 'type' => 'dropdown', 'listData' => array(
                                'string' => 'Строка',
                                'number' => 'Число',
                                'boolean' => 'Логическое значение'
                            )
                            )
                        ); ?>
                        <?php $this->widget('FormButton', array('title' => 'Добавить')); ?>
                        <?php echo Chtml::endForm(); ?>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <h2 id="customer">Свойства клиента</h2>

        <div class="row">
            <div class="col-md-12">

                <?php

                foreach ($customerDataFields as $field) {
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading"><a href="#" onclick="$('#customerField_<?php echo $field->id; ?>').slideToggle(250); return false;"><?php echo $field->position . '. ' . $field->name; ?></a>
                            <span
                                class="pull-right"><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', '?delete-customer-field=' . $field->id, array('title' => 'Удалить поле')); ?></span>
                        </div>
                        <div class="panel-body" style="display: none;" id="customerField_<?php echo $field->id; ?>">
                            <?php
                            echo CHtml::beginForm('?save-data-field=' . $field->id);
                            $this->renderPartial('_customerField_form', array('field' => $field));

                            $this->widget('FormButton');
                            echo CHtml::endForm();
                            ?>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Новое свойство</div>
                    <div class="panel-body">
                        <?php echo CHtml::beginForm(); ?>
                        <?php
                        $this->renderPartial('_customerField_form', array('field' => $customerDataField));
                        ?>
                        <?php $this->widget('FormButton', array('title' => 'Добавить')); ?>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <h2>Статусы заказов</h2>

        <div class="row">
            <div class="col-md-12">
                <ul class="list-group">
                    <?php foreach ($orderStatuses as $status) { ?>
                        <li class="list-group-item"><?php if ($status->default) { ?><strong><?php
                                }
                                echo $status->name; ?><?php if ($status->default) {
                                ?></strong><?php } ?><span
                                class="pull-right"><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('orders', 'delete-order-status' => $status->id), array('title' => 'Удалить'));
                                echo CHtml::link('<span class="glyphicon glyphicon-map-marker"></span>', array('orders', 'order-status-default' => $status->id), array('title' => 'По умолчанию')) ?></span>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Новый статус</div>
                    <div class="panel-body">
                        <?php echo CHtml::beginForm(); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderStatus, 'fieldName' => 'name')); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderStatus, 'fieldName' => 'slug')); ?>
                        <?php $this->widget('FormGroup', array('model' => $orderStatus, 'fieldName' => 'default', 'type' => 'checkbox')); ?>
                        <?php $this->widget('FormButton', array('title' => 'Добавить')); ?>
                        <?php echo CHtml::endForm(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>