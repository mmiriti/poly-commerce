<h1>Настройки</h1>
<?php 
echo CHtml::beginForm('', 'post', array('class' => 'form-horizontal', 'role' => 'form'));
foreach ($options as $opt) {
	?>
	<div class="form-group">
		<label for="inputEmail3" class="col-sm-2 control-label"><?php echo $opt->option_name; ?></label>
		<div class="col-sm-10">
			<?php
			switch ($opt->option_type) {
				case 'value':
				break;
				
				default:
				echo CHtml::textField('Option[' . $opt->id . ']', $opt->option_value, array('class' => 'form-control'));
				break;
			}
			?>
		</div>
	</div>
	<?php
}

$this->widget('FormButton', array('title' => 'Сохранить'));
echo CHtml::endForm(); 
?>