<?php
/* @var $field CustomerDataField */
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'name'));
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'hint'));
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'slug'));
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'mandatory', 'type' => 'yesno'));
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'fillable', 'type' => 'yesno'));
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'type', 'type' => 'dropdown', 'listData' => array(
        'string' => 'Строка',
        'number' => 'Число',
        'boolean' => 'Логическое значение',
        'select' => 'Выбор из списка',
    )
    )
);
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'position'));
$this->widget('FormGroup', array('model' => $field, 'fieldName' => 'options', 'type' => 'textarea', 'hint' => 'По одному на строку'));