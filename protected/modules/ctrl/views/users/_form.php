<?php
/* @var $this CtrlController */
/* @var $user User */
?>
    <div class="well">
        <?php echo CHtml::link('Отмена', array('index'), array('class' => 'btn btn-danger')); ?>
    </div>
<?php
echo CHtml::beginForm();
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'email'));
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'password', 'type' => 'password'));
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'password_again', 'type' => 'password'));
$this->widget('FormGroup', array('model' => $user, 'fieldName' => 'user_group_id', 'type' => 'dropdown', 'listData' => CHtml::listData(UserGroup::model()->findAll(), 'id', 'name')));
$this->widget('FormButton');
echo CHtml::endForm();