<?php
/* @var $this CtrlController */
/* @var $user User */
/* @var $dataProvider CActiveDataProvider */
?>
    <h1>Пользователи</h1>
    <div class="well">
        <?php echo CHtml::link('Создать пользователя', array('create'), array('class' => 'btn btn-primary')); ?>
    </div>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items' => array(
        array('label' => 'Все пользователи', 'url' => array('/ctrl/users/index')),
        array('label' => 'Администраторы', 'url' => array('/ctrl/users/admins')),
    ),
    'htmlOptions' => array('class' => 'nav nav-tabs', 'role' => 'tablist')
));

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'columns' => array(
        'id',
        'email',
        'userGroup.name',
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'label' => '<span class="glyphicon glyphicon-eye-open"></span>',
                    'url' => 'Yii::app()->createUrl("/ctrl/users/view", array("id" => $data->id))',
                    'imageUrl' => null,
                    'options' => array('title' => 'Просмотр'),
                ),
            )
        )
    )
));
?>