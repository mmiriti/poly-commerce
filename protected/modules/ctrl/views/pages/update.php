<h1><?php echo CHtml::link('Страницы', array('index')); ?> - <?php echo $page->title; ?></h1>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="btn-group">
			<?php echo CHtml::link('<span class="glyphicon glyphicon-chevron-left"></span> Назад', array('index'), array('class' => 'btn btn-default')); ?>
		</div>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php $this->renderPartial('_form', array('page' => $page)); ?>
			</div>
		</div>
	</div>
</div>