<?php echo CHtml::beginForm(); ?>
<?php $this->widget('FormGroup', array('model' => $page, 'fieldName' => 'title')); ?>
<?php $this->widget('FormGroup', array('model' => $page, 'fieldName' => 'slug')); ?>
<?php $this->widget('FormGroup', array('model' => $page, 'fieldName' => 'page_id', 'type' => 'dropdown', 'listData' => array(null => '- корень -') + CHtml::listData(Page::model()->findAll(), 'id', 'title'))); ?>
<?php $this->widget('DocumentEditor', array('document' => $page->document)); ?>
<?php $this->widget('FormButton'); ?>
<?php echo CHtml::endForm(); ?>