<?php
$this->pageTitle = 'Страницы';
?>
<h1>Страницы</h1>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="btn-group">
			<?php echo CHtml::link('<span class="glyphicon glyphicon-plus"></span> Создать стрницу', array('create'), array('class' => 'btn btn-primary')); ?>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<?php

		function page_list($ctrl, $list)
		{
			foreach ($list as $page) 
			{
				?>
				<div class="media">
					<a class="pull-left" href="<?php echo $ctrl->createUrl('update', array('id' => $page->id)); ?>">
						<span class="glyphicon glyphicon-file"></span>
					</a>
					<div class="media-body">
						<h4 class="media-heading"><?php echo CHtml::link($page->title, array('update', 'id' => $page->id)); ?><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('delete', 'id' => $page->id), array('class' => 'pull-right btn btn-danger btn-xs', 'title' => 'Удалить страницу "' . $page->title . '"')); ?></h4>
						<?php echo substr(strip_tags($page->document->html), 0, 100); ?>
						<?php
						page_list($ctrl, $page->pages);
						?>
					</div>					
				</div>
				<?php
			}
		}

		page_list($this, $pages);

		?>
	</div>
</div>