<?php
/* @var $this LetterTemplatesController */
/* @var $model LetterTemplate */
/* @var $form CActiveForm */
?>
<div class="well">
    <?php echo CHtml::link('Отмена', array('index'), array('class' => 'btn btn-danger')); ?>
</div>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'letter-template-form',
        'enableAjaxValidation' => false,
    )); ?>

    <?php
    if ($model->hasErrors()) {
        ?>
        <div class="alert alert-danger">
            <?php
            echo $form->errorSummary($model);
            ?>
        </div>
    <?php
    }
    ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'slug'); ?>
        <?php echo $form->textField($model, 'slug', array('size' => 60, 'maxlength' => 64, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'slug'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'subject'); ?>
        <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
        <?php echo $form->error($model, 'subject'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'content'); ?>
        <?php echo $form->textArea($model, 'content', array('rows' => 6, 'cols' => 50, 'class' => 'wysiwyg form-control')); ?>
        <?php echo $form->error($model, 'content'); ?>
    </div>

    <div class="form-group">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-primary')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>