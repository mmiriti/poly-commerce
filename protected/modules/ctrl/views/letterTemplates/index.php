<?php
/* @var $this LetterTemplatesController */
/* @var $templates LetterTemplate[] */
?>
<h1>Шаблоны писем</h1>
<div class="well">
    <?php echo CHtml::link('Создать', array('create'), array('class' => 'btn btn-primary')); ?>
</div>

<div class="row">
    <?php
    foreach ($templates as $template) {
        ?>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $template->name; ?> (<code><?php echo $template->slug; ?></code>)</div>
                <div class="panel-body">
                    <?php
                    echo $template->content;
                    ?>
                </div>
                <div class="panel-footer">
                    <?php echo CHtml::link('Редактировать', array('update', 'id' => $template->id), array('class' => 'btn btn-default')); ?>
                    <?php echo CHtml::link('Удалить', array('delete', 'id' => $template->id), array('class' => 'btn btn-danger')); ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>