<?php
/* @var $this LetterTemplatesController */
/* @var $model LetterTemplate */

$this->breadcrumbs=array(
	'Letter Templates'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List LetterTemplate', 'url'=>array('index')),
	array('label'=>'Create LetterTemplate', 'url'=>array('create')),
	array('label'=>'Update LetterTemplate', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LetterTemplate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LetterTemplate', 'url'=>array('admin')),
);
?>

<h1>View LetterTemplate #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_time',
		'slug',
		'name',
		'subject',
		'content',
	),
)); ?>
