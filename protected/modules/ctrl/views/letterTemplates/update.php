<?php
/* @var $this LetterTemplatesController */
/* @var $model LetterTemplate */
?>

<h1>Изменить шаблон &laquo;<?php echo $model->name; ?>&raquo;</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>