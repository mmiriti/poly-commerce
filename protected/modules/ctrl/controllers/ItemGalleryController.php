<?php

class ItemGalleryController extends CtrlController
{
	public function actionDelete($gallery_item_id)
	{
		$galleryItem = ItemGalleryItem::model()->findByPk($gallery_item_id);
		$galleryItem->delete();
	}

	public function actionIndex($item_id)
	{
		$item = CatalogItem::model()->findByPk($item_id);
		$this->renderPartial('index', array('item' => $item));
	}

	public function actionUpload($item_id)
	{
		$file = File::upload('image');

		if($file != null)
		{
			$galleryItem = new ItemGalleryItem('create');
			$galleryItem->item_id = $item_id;
			$galleryItem->file_id = $file->id;
			$galleryItem->save();
		} else {
			throw new CHttpException(500);
		}
	}
}