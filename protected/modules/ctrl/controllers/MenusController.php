<?php

class MenusController extends CtrlController
{
    public function actionCreate()
    {
        $newMenu = new Menu();

        if (isset($_POST['Menu'])) {
            $newMenu->attributes = $_POST['Menu'];
            if ($newMenu->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'menu' => $newMenu
        ));
    }

    public function actionCreateItem()
    {
        $this->render('createItem');
    }

    public function actionDelete()
    {
        $this->render('delete');
    }

    public function actionDeleteItem($id)
    {
        $menuItem = MenuItem::model()->findByPk($id);
        $menuItem->delete();
        $this->redirect(array('items', 'id' => $menuItem->menu_id));
    }

    public function actionIndex()
    {
        $menus = Menu::model()->findAll();
        $this->render('index', array(
            'menus' => $menus
        ));
    }

    public function actionItems($id)
    {
        $menu = Menu::model()->findByPk($id);
        $newItem = new MenuItem();
        $newItem->menu_id = $id;

        if (isset($_POST['MenuItem'])) {
            $newItem->attributes = $_POST['MenuItem'];
            if ($newItem->save()) {
                $this->redirect(array('items', 'id' => $id));
            }
        }

        if (isset($_POST['MenuItems'])) {
            foreach ($_POST['MenuItems'] as $item_id => $itemData) {
                $menuItem = MenuItem::model()->findByPk($item_id);
                $menuItem->attributes = $itemData;
                $menuItem->save();
            }
            $this->redirect(array('items', 'id' => $id));
        }

        $this->render('items', array(
            'menu' => $menu,
            'newItem' => $newItem
        ));
    }
}