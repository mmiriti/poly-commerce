<?php

class OptionsController extends CtrlController
{
    /**
     * Опции
     *
     */
    public function actionIndex()
    {
        if (isset($_POST['Option'])) {
            foreach ($_POST['Option'] as $id => $value) {
                $opt = Options::model()->findByPk($id);
                $opt->option_value = $value;
                $opt->save();
            }

            $this->redirect(array('index'));
        }

        $options = Options::model()->findAllByAttributes(array('editable' => 1));

        $this->render('index', array(
            'options' => $options
        ));
    }

    /**
     * Свойства заказов
     *
     */
    public function actionOrders()
    {
        $newOrderDataField = new OrderDataField;

        if (isset($_POST['OrderDataField'])) {
            $newOrderDataField->attributes = $_POST['OrderDataField'];
            if ($newOrderDataField->save()) {
                $this->redirect(array('orders'));
            }
        }

        if (isset($_GET['delete-order-field'])) {
            $orderDataField = OrderDataField::model()->findByPk(intval($_GET['delete-order-field']));
            $orderDataField->delete();
            $this->redirect(array('orders'));
        }

        $orderDataFields = OrderDataField::model()->findAll();

        if (isset($_GET['save-data-field']) && ($_POST['CustomerDataField'])) {
            $field = CustomerDataField::model()->findByPk($_GET['save-data-field']);

            if ($field != null) {
                $field->attributes = $_POST['CustomerDataField'];
                if ($field->save()) {
                    $this->redirect(array('orders', '#' => 'customer'));
                }
            }
        }

        $newCustomerDataField = new CustomerDataField;

        if (isset($_POST['CustomerDataField']) && (!isset($_GET['save-data-field']))) {
            $newCustomerDataField->attributes = $_POST['CustomerDataField'];
            if ($newCustomerDataField->save()) {
                $this->redirect(array('orders', '#' => 'customer'));
            }
        }

        if (isset($_GET['delete-customer-field'])) {
            $customerDataField = CustomerDataField::model()->findByPk(intval($_GET['delete-customer-field']));
            $customerDataField->delete();
            $this->redirect(array('orders', '#' => 'customer'));
        }

        $customerDataFields = CustomerDataField::model()->findAll();

        $newStatus = new OrderStatus;

        if (isset($_POST['OrderStatus'])) {
            $newStatus->attributes = $_POST['OrderStatus'];
            if ($newStatus->save()) {
                if ($newStatus->default == 1) {
                    Yii::app()->db->createCommand()->update('order_status', array('default' => 0), 'id <> :id', array('id' => $newStatus->id));
                }
                $this->redirect(array('orders'));
            }
        }

        if (isset($_GET['delete-order-status'])) {
            $status = OrderStatus::model()->findByPk(intval($_GET['delete-order-status']));
            if ($status != null) {
                $status->delete();
            }
            $this->redirect(array('orders'));
        }

        if (isset($_GET['order-status-default'])) {
            Yii::app()->db->createCommand()->update('order_status', array('default' => 0));

            $status = OrderStatus::model()->findByPk(intval($_GET['order-status-default']));
            $status->default = 1;
            $status->save();
            $this->redirect(array('orders'));
        }

        $orderStatuses = OrderStatus::model()->findAll();

        $this->render('orders', array(
            'orderDataFields' => $orderDataFields,
            'orderDataField' => $newOrderDataField,
            'customerDataField' => $newCustomerDataField,
            'customerDataFields' => $customerDataFields,
            'orderStatus' => $newStatus,
            'orderStatuses' => $orderStatuses
        ));
    }
}
