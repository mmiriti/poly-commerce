<?php

class ItemOptionsController extends CtrlController
{
    public function actionEdit($id)
    {
        $itemOption = ItemOption::model()->findByPk($id);
        if ($itemOption != null) {

            if (isset($_POST['ItemOption'])) {
                $itemOption->attributes = $_POST['ItemOption'];
                if ($itemOption->save()) {
                    $this->redirect(array('edit', 'id' => $id));
                }
            }

            $newVariant = new ItemOptionVariant();
            $newVariant->option_id = $id;

            if (isset($_POST['ItemOptionVariant'])) {
                $newVariant->attributes = $_POST['ItemOptionVariant'];
                if ($newVariant->save()) {
                    $this->redirect(array('edit', 'id' => $id));
                }
            }

            $this->render('edit', array(
                'itemOption' => $itemOption,
                'newVariant' => $newVariant,
            ));
        } else {
            throw new CHttpException(404, 'Опция не найдена');
        }
    }

    public function actionDeleteVariant($id)
    {
        $variant = ItemOptionVariant::model()->findByPk($id);
        $variant->delete();
        $this->redirect(array('edit', 'id' => $variant->option_id));
    }

    public function actionAllowVariant($id)
    {
        $this->redirect(array('edit', 'id' => $id));
    }

    public function actionBanVariant($id)
    {
        $this->redirect(array('edit', 'id' => $id));
    }

    public function actionIndex()
    {
        $this->render('index');
    }
}