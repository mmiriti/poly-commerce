<?php

class ThemeController extends CtrlController
{
    public function actionIndex()
    {
        $themeDir = opendir('themes');

        $themes = array();

        while (($file = readdir($themeDir)) !== FALSE) {
            if (($file[0] !== '.') && (file_exists('themes/' . $file . '/info.php'))) {
                $themes[$file] = include 'themes/' . $file . '/info.php';
            }
        }

        if (isset($_POST['theme'])) {
            Options::set('site_theme', $_POST['theme']);
            $this->redirect(array('index'));
        }

        $this->render('index', array('themes' => $themes));
    }
}