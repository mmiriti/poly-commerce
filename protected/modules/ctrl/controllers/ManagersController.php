<?php

class ManagersController extends CtrlController
{
    /**
     * @return UserGroup
     */
    private function getManagersGroup()
    {
        return UserGroup::model()->findByAttributes(array('role' => 'manager'));
    }

    public function actionIndex()
    {
        $managersGroup = $this->getManagersGroup();
        $managers = $managersGroup->users;
        $this->render('index', array(
            'managers' => $managers
        ));
    }

    public function actionCreate()
    {
        $managersGroup = $this->getManagersGroup();

        $manager = new User('reg');
        $manager->user_group_id = $managersGroup->id;

        if (isset($_POST['User'])) {
            $manager->attributes = $_POST['User'];

            if ($manager->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'manager' => $manager
        ));
    }
} 