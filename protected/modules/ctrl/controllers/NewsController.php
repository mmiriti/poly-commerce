<?php

class NewsController extends CtrlController
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->renderPartial('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    private function loadNewsItem($model, $document)
    {
        if (isset($_POST['NewsItem'])) {
            $model->attributes = $_POST['NewsItem'];
            $model->validate();

            if (isset($_POST['Document'])) {
                $document->attributes = $_POST['Document'];
                if ($document->save()) {
                    $model->document_id = $document->id;
                    if ($model->save()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new NewsItem;
        $document = new Document;

        if ($this->loadNewsItem($model, $document)) {
            $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
            'document' => $document
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if ($this->loadNewsItem($model, $model->document)) {
            $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex($skip = 0)
    {
        $newsCriteria = new CDbCriteria();
        $newsCriteria->order = 'publish_date_time desc';
        $newsCriteria->limit = 10;
        $newsCriteria->offset = $skip;

        $news = NewsItem::model()->findAll($newsCriteria);

        $this->render('index', array(
            'news' => $news,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new NewsItem('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['NewsItem']))
            $model->attributes = $_GET['NewsItem'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return NewsItem the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = NewsItem::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param NewsItem $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'news-item-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
