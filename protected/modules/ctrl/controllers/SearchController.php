<?php

class SearchController extends CtrlController
{
	public function actionPublic($query_id, $public)
	{
		$query = SearchQuery::model()->findByPk($query_id);
		$query->public = $public;
		
		if($query->save())
		{
			$this->redirect(array('index'));
		}
	}

	public function actionIndex()
	{
		$querys = SearchQuery::model()->findAll();

		$this->render('index', array(
			'querys' => $querys
			)
		);
	}
}