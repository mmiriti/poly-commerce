<?php

class LetterTemplatesController extends CtrlController
{
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new LetterTemplate;

        if (isset($_POST['LetterTemplate'])) {
            $model->attributes = $_POST['LetterTemplate'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['LetterTemplate'])) {
            $model->attributes = $_POST['LetterTemplate'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        $this->redirect(array('index'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $templates = LetterTemplate::model()->findAll();
        $this->render('index', array(
            'templates' => $templates
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new LetterTemplate('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['LetterTemplate']))
            $model->attributes = $_GET['LetterTemplate'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return LetterTemplate the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = LetterTemplate::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param LetterTemplate $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'letter-template-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
