<?php

class ClassifierController extends CtrlController
{
    /**
     * Список
     */
    public function actionIndex()
    {
        $classifierGroups = ClassifierGroup::model()->findAll();
        $this->render('index', array(
            'classifierGroups' => $classifierGroups
        ));
    }

    /**
     * Создание
     */
    public function actionCreate()
    {
        $classifierGroup = new ClassifierGroup();

        if (isset($_POST['ClassifierGroup'])) {
            $classifierGroup->attributes = $_POST['ClassifierGroup'];
            if ($classifierGroup->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'classifierGroup' => $classifierGroup
        ));
    }

    /**
     * Изменение
     *
     * @param $id
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        $classifierGroup = ClassifierGroup::model()->findByPk($id);

        if ($classifierGroup != null) {
            if (isset($_POST['ClassifierGroup'])) {
                $classifierGroup->attributes = $_POST['ClassifierGroup'];
                if ($classifierGroup->save()) {
                    $this->redirect(array('index'));
                }
            }

            $classifier = new Classifier();
            $classifier->group_id = $id;

            if (isset($_POST['Classifier'])) {
                $classifier->attributes = $_POST['Classifier'];
                if ($classifier->save()) {
                    $this->redirect(array('update', 'id' => $id));
                }
            }

            $this->render('update', array(
                'classifierGroup' => $classifierGroup,
                'classifier' => $classifier,
            ));
        } else {
            throw new CHttpException(404, 'Not found');
        }
    }

    /**
     * Удаление
     *
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $classifierGroup = ClassifierGroup::model()->findByPk($id);
        if ($classifierGroup != null) {
            $classifierGroup->delete();
            $this->redirect(array('index'));
        } else {
            throw new CHttpException(404, 'Не найдено');
        }
    }

    /**
     * Удалить значение
     *
     * @param $id
     */
    public function actionDeleteClassifier($id)
    {
        $classifier = Classifier::model()->findByPk($id);

        if ($classifier != null) {
            $classifier->delete();
            $this->redirect(array('update', 'id' => $classifier->group_id, '#' => 'values'));
        }
    }
} 