<?php

class FilesController extends CtrlController
{
    public function actionIndex($folder_id = null)
    {
        if (isset($_FILES['upload_file']) && ($_FILES['upload_file']['error'] == 0)) {
            $file = File::upload('upload_file');
            if ($file != null) {
                $file->folder_id = $folder_id;
                $file->save();
            }

            $this->redirect(array('index', 'folder_id' => $folder_id));
        }

        $criteria = new CDbCriteria(array('order' => 'create_time desc'));
        $criteria->compare('folder_id', $folder_id);

        $files = File::model()->findAll($criteria);
        $this->render('index', array(
            'files' => $files
        ));
    }

    public function actionView($id)
    {
        $file = File::model()->findByPk($id);
        if ($file != null) {
            $this->render('view', array('file' => $file));
        } else {
            throw new CHttpException(404, 'File not found');
        }
    }
} 