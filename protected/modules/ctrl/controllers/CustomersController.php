<?php

class CustomersController extends CtrlController
{
    public function actionIndex()
    {
        $this->fluid = true;
        $allCustomers = Customer::model()->findAll();
        $dataFields = CustomerDataField::model()->findAll();

        $this->render('index', array(
                'allCustomers' => $allCustomers,
                'dataFields' => $dataFields
            )
        );
    }

    /**
     * Просмотр всех данных о пользователе
     *
     * @param $id
     * @throws CHttpException
     */
    public function actionView($id)
    {
        $customer = Customer::model()->findByPk($id);
        if ($customer != null) {
            $this->render('view', array(
                'customer' => $customer,
            ));
        } else {
            throw new CHttpException(404, 'Не найдено');
        }
    }

    public function actionSurcharge($id)
    {
        $customer = Customer::model()->findByPk($id);

        if (isset($_POST['Customer'])) {
            $customer->attributes = $_POST['Customer'];
            $customer->save();

            if (isset($_POST['CategorySurcharge'])) {
                foreach ($_POST['CategorySurcharge'] as $categoryID => $value) {
                    $attributes = array(
                        'customer_id' => $customer->id,
                        'category_id' => $categoryID,
                    );

                    $customerCategorySurcharge = CustomerCategorySurcharge::model()->findByAttributes($attributes);

                    if ($customerCategorySurcharge == null) {
                        $customerCategorySurcharge = new CustomerCategorySurcharge();
                    }

                    if ($value != "") {
                        $attributes['surcharge'] = floatval($value);
                        $customerCategorySurcharge->attributes = $attributes;
                        $customerCategorySurcharge->save();
                    } else {
                        if (!$customerCategorySurcharge->isNewRecord)
                            $customerCategorySurcharge->delete();
                    }
                }
            }

            $this->redirect(array('surcharge', 'id' => $id));
        }

        $this->render('surcharge', array(
            'customer' => $customer,
        ));
    }

    /**
     * Прикрепить менеджеров для клиента
     *
     * @param $id
     * @throws CHttpException
     */
    public function actionAttach($id)
    {
        $customer = Customer::model()->findByPk($id);

        if ($customer != null) {

            if (isset($_POST['AttachManager'])) {
                ManagerCustomer::model()->deleteAllByAttributes(array('customer_id' => $customer->id));
                foreach ($_POST['AttachManager'] as $manager_id) {
                    $managerLink = new ManagerCustomer();
                    $managerLink->customer_id = $customer->id;
                    $managerLink->manager_id = $manager_id;
                    $managerLink->save();
                }

                $this->redirect(array('index'));
            }

            $managers = array();

            $managersGroup = UserGroup::model()->findByAttributes(array('role' => 'manager'));

            if ($managersGroup != null) {
                $managers = $managersGroup->users;
            }

            $attachedManagers = ManagerCustomer::model()->findAllByAttributes(array('customer_id' => $customer->id));
            $attachedManagersIDS = array();

            foreach ($attachedManagers as $manager) {
                $attachedManagersIDS[$manager->manager_id] = true;
            }

            $this->render('attach', array(
                'managers' => $managers,
                'customer' => $customer,
                'attachedManagersIDS' => $attachedManagersIDS,
            ));
        } else {
            throw new CHttpException(404, 'Клиент не найден');
        }
    }
}