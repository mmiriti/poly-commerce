<?php

class DefaultController extends CtrlController
{
	public $layout = '/layouts/ctrl';
	
	public function actionIndex()
	{
		$this->render('index');
	}
}