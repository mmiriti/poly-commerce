<?php

class CatalogController extends CtrlController
{
    /**
     * List
     *
     * @param null $parent_id
     * @param null $group_id
     * @param int $page
     */
    public function actionIndex($parent_id = null, $group_id = null, $page = 0)
    {
        $criteria = new CDbCriteria();

        if ($group_id == null) {
            if (count(CatalogItemGroup::model()->findAll()) == 1) {
                $this->redirect(array('index', 'parent_id' => $parent_id, 'group_id' => CatalogItemGroup::model()->find()->id));
            }
        }

        $emptyCategory = new CatalogCategory();
        $emptyCategory->name = 'Все категории';

        $categories = array_merge(array($emptyCategory), CatalogCategory::model()->findAllByAttributes(array('parent_id' => null)));

        if ($parent_id != null) {
            $currentCategory = CatalogCategory::model()->findByPk($parent_id);
            /* @var $currentCategory CatalogCategory */
        } else {
            $currentCategory = null;
        }

        if ($group_id != null) {
            $group = CatalogItemGroup::model()->findByPk($group_id);
            if ($group != null) {
                $criteria->compare('group_id', $group->id);
            }
        } else {
            $group = null;
        }

        if ($currentCategory != null) {
            $c = new CDbCriteria();
            $c->compare('catalog_category_id', $currentCategory->id);
            $c->group = 'catalog_item_id';
            $c->distinct = true;

            $allLinks = CatalogItemCategory::model()->findAll($c);

            $ids = array();

            foreach ($allLinks as $l) {
                $ids[] = $l->catalog_item_id;
            }

            $criteria->addInCondition('id', $ids);
        }

        $items = new CActiveDataProvider(CatalogItem::model(), array(
            'criteria' => $criteria,
        ));

        $items->pagination->pageSize = 30;
        $items->pagination->currentPage = $page;

        $this->render('index', array(
                'current' => $currentCategory,
                'categories' => $categories,
                'items' => $items,
                'group' => $group,
            )
        );
    }

    /**
     * Создать категорию
     *
     */
    public function actionCreateCategory()
    {
        $category = new CatalogCategory('create');

        $category->document = new Document('create');

        if (isset($_POST['CatalogCategory'])) {
            $category->attributes = $_POST['CatalogCategory'];

            if (isset($_POST['Document'])) {
                $category->document->attributes = $_POST['Document'];
                if ($category->document->save()) {
                    $category->document_id = $category->document->id;
                }
            }

            if ($category->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('createcategory', array('category' => $category));
    }

    /**
     * Создать группу
     *
     */
    public function actionCreategroup()
    {
        $group = new CatalogItemGroup('create');

        if (isset($_POST['CatalogItemGroup'])) {
            $group->attributes = $_POST['CatalogItemGroup'];
            if ($group->save()) {
                $this->redirect(array('editgroup', 'id' => $group->id));
            }
        }

        $this->render('creategroup', array(
                'group' => $group
            )
        );
    }

    /**
     * Редактировать группу
     *
     * @param $id
     * @param null $delete_parameter
     * @param null $delete_option
     * @param null $set_all_options
     * @throws CHttpException
     */
    public function actionEditgroup($id, $delete_parameter = null, $delete_option = null, $set_all_options = null)
    {
        $group = CatalogItemGroup::model()->findByPk($id);
        if ($group == null) {
            throw new CHttpException(404, 'Группа товаров не найдена');
        } else {
            if (isset($_POST['CatalogItemGroup'])) {
                $group->attributes = $_POST['CatalogItemGroup'];
                if ($group->save()) {
                    $this->redirect(array('editgroup', 'id' => $id));
                }
            }

            $newParameter = new Parameter('create');

            if (isset($_POST['Parameter'])) {
                $newParameter->attributes = $_POST['Parameter'];

                if ($newParameter->save()) {
                    $newGroupParameter = new GroupParameter('create');
                    $newGroupParameter->group_id = $group->id;
                    $newGroupParameter->parameter_id = $newParameter->id;
                    if ($newGroupParameter->save()) {
                        $this->redirect(array('editgroup', 'id' => $id));
                    } else {
                        $newParameter->delete();
                    }
                }
            }

            $newOption = new ItemOption();

            if (isset($_POST['ItemOption'])) {
                $newOption->attributes = $_POST['ItemOption'];
                if ($newOption->save()) {
                    $newGroupOption = new GroupOption();
                    $newGroupOption->group_id = $group->id;
                    $newGroupOption->option_id = $newOption->id;
                    if ($newGroupOption->save()) {
                        $this->redirect(array('editgroup', 'id' => $id));
                    } else {
                        $newOption->delete();
                    }
                }
            }

            if (isset($_POST['AttachParam'])) {
                $paramToAttach = Parameter::model()->findByPk(intval($_POST['AttachParam']));
                if ($paramToAttach != null) {
                    if (GroupParameter::model()->findByAttributes(array('group_id' => $group->id, 'parameter_id' => $paramToAttach->id)) == null) {
                        $newGroupParameter = new GroupParameter('create');
                        $newGroupParameter->group_id = $group->id;
                        $newGroupParameter->parameter_id = $paramToAttach->id;
                        if ($newGroupParameter->save()) {
                            $this->redirect(array('editgroup', 'id' => $id));
                        }
                    }
                }
            }

            if ($delete_parameter != null) {
                $groupParameter = GroupParameter::model()->findByAttributes(array('group_id' => $id, 'parameter_id' => $delete_parameter));
                $groupParameter->delete();

                $this->redirect(array('editgroup', 'id' => $id));
            }

            if ($delete_option != null) {
                $groupOption = GroupOption::model()->findByAttributes(array('group_id' => $id, 'option_id' => $delete_option));
                $groupOption->delete();

                $this->redirect(array('editgroup', 'id' => $id));
            }

            if ($set_all_options != null) {
                foreach ($group->items as $item) {
                    foreach ($item->availableOptionVariants as $availOpt) {
                        $availOpt->delete();
                    }

                    foreach ($group->options as $option) {
                        foreach ($option->itemOptionVariants as $variant) {
                            $newAvailOption = new CatalogItemOptionVariantAvailable();
                            $newAvailOption->option_variant_id = $variant->id;
                            $newAvailOption->catalog_item_id = $item->id;
                            $newAvailOption->save();
                        }
                    }
                }
                $this->redirect(array('editgroup', 'id' => $id));
            }

            $this->render('editgroup', array(
                    'group' => $group,
                    'new_parameter' => $newParameter,
                    'newOption' => $newOption,
                )
            );
        }
    }

    /**
     * Удалить группу
     *
     * @param $id
     * @throws CHttpException
     */
    public function actionDeletegroup($id)
    {
        $group = CatalogItemGroup::model()->findByPk($id);
        if ($group == null) {
            throw new CHttpException(404, 'Группа товаров не найдена');
        } else {
            if (count($group->items) == 0) {
                $group->delete();
                $this->redirect(array('groups'));
            } else {
                $this->render('deletegroup');
            }
        }
    }

    /**
     * Группы
     *
     */
    public function actionGroups()
    {
        $groups = CatalogItemGroup::model()->findAll();
        $this->render('groups', array(
                'groups' => $groups
            )
        );
    }

    /**
     * Создать ссылки на категории
     *
     * @param $item
     * @param null $current_category_id
     */
    private function _item_create_links($item, $current_category_id = null)
    {
        if (isset($_POST['CatalogCategory'])) {
            foreach ($_POST['CatalogCategory'] as $id => $on) {
                if ($id !== $current_category_id) {
                    $link = new CatalogItemCategory('create');
                    $link->catalog_category_id = $id;
                    $link->catalog_item_id = $item->id;
                    if (!$link->save()) {
                    }
                }
            }
        }
    }

    /**
     * Создать элемент каталога
     *
     * @param null $current_category_id
     */
    public function actionCreateitem($current_category_id = null)
    {
        $groups = CatalogItemGroup::model()->findAll();
        $item = new CatalogItem('create');

        if (isset($_POST['CatalogItem'])) {
            $item->attributes = $_POST['CatalogItem'];

            if ($item->save()) {
                $this->_item_create_links($item, $current_category_id);
                $this->redirect(array('edititem', 'id' => $item->id));
            } else {
            }
        }

        $this->render('createitem', array(
                'groups' => $groups,
                'item' => $item,
                'current_category_id' => $current_category_id
            )
        );
    }

    /**
     * Удалить элемент каталога
     *
     * @param $id
     * @param null $parent_id
     * @param null $group_id
     * @throws CHttpException
     */
    public function actionDeleteItem($id, $parent_id = null, $group_id = null)
    {
        $item = CatalogItem::model()->findByPk($id);
        if ($item != null) {
            $item->delete();
            $this->redirect(array('index', 'parent_id' => $parent_id, 'group_id' => $group_id));
        } else {
            throw new CHttpException(404, 'Элемент каталога не найден');
        }
    }

    /**
     * Редактирование элемента каталога
     *
     * @param $id
     * @throws CHttpException
     */
    public function actionEdititem($id)
    {
        $groups = CatalogItemGroup::model()->findAll();
        $item = CatalogItem::model()->findByPk($id);

        if ($item != null) {

            $newPrice = new ItemPrice('create');
            $newPrice->item_id = $item->id;

            if ($item->document == null) {
                $item->document = new Document('create');
            }

            if (isset($_POST['Document'])) {
                $item->document->attributes = $_POST['Document'];
                if ($item->document->save()) {
                    $item->document_id = $item->document->id;
                    $item->save();
                }
            }

            if (isset($_POST['ItemPrice'])) {
                $newPrice->attributes = $_POST['ItemPrice'];
                $newPrice->save();
            }

            if (isset($_POST['ParameterValue'])) {
                foreach ($_POST['ParameterValue'] as $value_id => $value) {
                    $paramValue = ItemParameterValue::model()->findByPk($value_id);
                    $paramValue->value = $value;
                    $paramValue->save();
                }
            }

            if (!empty($_POST)) {
                foreach ($item->availableOptionVariants as $variant) {
                    if (!isset($_POST['ItemOptionVariant'][$variant->option_variant_id])) {
                        $variant->delete();
                    }
                }
            }

            if (isset($_POST['ItemOptionVariant'])) {
                foreach ($_POST['ItemOptionVariant'] as $variantID => $availVariant) {
                    if (CatalogItemOptionVariantAvailable::model()->findByAttributes(array('catalog_item_id' => $item->id, 'option_variant_id' => $variantID)) == null) {
                        $newAvailVariant = new CatalogItemOptionVariantAvailable();
                        $newAvailVariant->catalog_item_id = $item->id;
                        $newAvailVariant->option_variant_id = $variantID;
                        $newAvailVariant->save();
                    }
                }
            }

            if (isset($_POST['CatalogItem'])) {
                $item->attributes = $_POST['CatalogItem'];
                if ($item->save()) {
                    CatalogItemCategory::model()->deleteAllByAttributes(array('catalog_item_id' => $item->id));
                    $this->_item_create_links($item);
                    $this->redirect(array('edititem', 'id' => $item->id));
                }
            }

            $this->render('edititem', array(
                    'groups' => $groups,
                    'item' => $item,
                    'new_price' => $newPrice
                )
            );
        } else {
            throw new CHttpException(404);
        }
    }

    /**
     * Редактировать категорию
     *
     * @param $category_id
     * @throws CHttpException
     */
    public function actionEditcategory($category_id)
    {
        $category = CatalogCategory::model()->findByPk($category_id);
        if ($category != null) {
            if ($category->document == null) {
                $category->document = new Document('create');
            }

            if (isset($_POST['Document'])) {
                $category->document->attributes = $_POST['Document'];
                if ($category->document->save()) {
                    $category->document_id = $category->document->id;
                    $category->save();
                }
            }

            if (isset($_POST['CatalogCategory'])) {
                $category->attributes = $_POST['CatalogCategory'];

                $file = File::upload('image_file');

                if ($file != null) {
                    $category->file_id = $file->id;
                }

                if ($category->save()) {
                    $this->redirect(array('editcategory', 'category_id' => $category_id));
                } else {
                    if ($file != null) {
                        $file->delete();
                    }
                }
            }
            $this->render('editcategory', array('category' => $category));
        } else {
            throw new CHttpException(404, "Категория не найдена");
        }
    }

    /**
     * Удалить категорию
     *
     * @param $category_id
     * @throws CHttpException
     */
    public function actionDeletecategory($category_id)
    {
        $category = CatalogCategory::model()->findByPk($category_id);
        if ($category !== null) {
            if ((count($category->items) == 0) && (count($category->catalogCategories) == 0)) {
                $category->delete();
                $this->redirect(array('index'));
            } else {
                throw new CHttpException(500, "Можно удалить только пустую категорию");

            }
        } else {
            throw new CHttpException(404, "Категория не найдена");
        }
    }

    public function actionOptions()
    {
        if (isset($_POST['Option'])) {
            foreach ($_POST['Option'] as $id => $value) {
                $opt = Options::model()->findByPk($id);
                $opt->option_value = $value;
                $opt->save();
            }

            $this->redirect(array('options'));
        }

        $options = Options::model()->findAllByAttributes(array('category' => 'catalog'));

        $this->render('options', array(
            'options' => $options
        ));
    }
}