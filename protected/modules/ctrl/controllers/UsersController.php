<?php

class UsersController extends CtrlController
{
    public function actionIndex()
    {
        $user = new User('search');
        $user->unsetAttributes();

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
        }

        $this->render('index', array(
                'user' => $user,
                'dataProvider' => $user->search()
            )
        );
    }

    public function actionAdmins()
    {
        $adminGroups = UserGroup::model()->findByAttributes(array('role' => 'admin'));

        $user = new User('search');
        $user->unsetAttributes();

        $user->user_group_id = $adminGroups->id;

        $this->render('index', array(
            'user' => $user,
            'dataProvider' => $user->search(),
        ));
    }

    public function actionCreate()
    {
        $user = new User('reg');

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            if ($user->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'user' => $user
        ));
    }

    public function actionUpdate($id)
    {
        $this->render('update');
    }
}