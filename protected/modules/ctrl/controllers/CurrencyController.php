<?php

class CurrencyController extends CtrlController
{
    public function actionIndex($deleteExchange = null)
    {
        $currencies = Currency::model()->findAll();

        $exchanges = CurrencyExchange::model()->findAll();
        $new_exchange = new CurrencyExchange();

        if (isset($_POST['CurrencyExchange'])) {
            $new_exchange->attributes = $_POST['CurrencyExchange'];
            if ($new_exchange->save()) {
                $this->redirect(array('index'));
            }
        }

        if (isset($_POST['UpdateRatio'])) {
            foreach ($_POST['UpdateRatio'] as $exchange_id => $value) {
                $exchange = CurrencyExchange::model()->findByPk($exchange_id);
                $exchange->ratio = $value;
                $exchange->save();
            }

            $this->redirect(array('index'));
        }

        if ($deleteExchange != null) {
            $exchange = CurrencyExchange::model()->findByPk($deleteExchange);
            if ($exchange != null) {
                $exchange->delete();
                $this->redirect(array('index'));
            }
        }

        $this->render('index', array(
            'currencies' => $currencies,
            'exchanges' => $exchanges,
            'new_exchange' => $new_exchange,
        ));
    }

    public function actionCreate()
    {
        $currency = new Currency();

        if (isset($_POST['Currency'])) {
            $currency->attributes = $_POST['Currency'];
            if ($currency->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array(
            'currency' => $currency,
        ));
    }
} 