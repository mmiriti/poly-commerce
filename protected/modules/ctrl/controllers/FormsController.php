<?php

class FormsController extends CtrlController
{
    /**
     * Создать форму
     */
    public function actionCreate()
    {
        $newForm = new Form();

        if (isset($_POST['Form'])) {
            $newForm->attributes = $_POST['Form'];
            if ($newForm->save()) {
                $this->redirect(array('edit', 'id' => $newForm->id));
            }
        }

        $this->render('create', array('newForm' => $newForm));
    }

    /**
     * Удалить форму
     */
    public function actionDelete($id)
    {
        $this->render('delete');
    }

    /**
     * Удалить поле формы
     *
     * @param $id
     */
    public function actionDeleteField($id)
    {
        $formField = FormField::model()->findByPk($id);

        $formID = $formField->form_id;

        if ($formField != null) {
            if (count($formField->formFillValues)) {
                $formField->form_id = null;
                $formField->save();
            } else {
                $formField->delete();
            }
        }

        $this->redirect(array('edit', 'id' => $formID));
    }

    public function actionDeleteFill($id, $form_id)
    {
        $fill = FormFill::model()->findByPk($id);
        $fill->delete();
        $this->redirect(array('view', 'id' => $form_id));
    }

    public function actionArchiveFill($id, $form_id)
    {
        $fill = FormFill::model()->findByPk($id);
        $fill->is_new = 0;
        $fill->save();
        $this->redirect(array('view', 'id' => $form_id));
    }

    /**
     * Редактировать форму
     * @param $id
     */
    public function actionEdit($id)
    {
        $form = Form::model()->findByPk($id);

        if (isset($_POST['Form'])) {
            $form->attributes = $_POST['Form'];
            if ($form->save()) {
                $this->redirect(array('edit', 'id' => $id));
            }
        }

        $newField = new FormField();
        $newField->form_id = $form->id;

        if (isset($_POST['FormField'])) {
            $newField->attributes = $_POST['FormField'];
            if ($newField->save()) {
                $this->redirect(array('edit', 'id' => $id));
            }
        }

        $this->render('edit', array('form' => $form, 'newField' => $newField));
    }

    /**
     * Список форм
     */
    public function actionIndex()
    {
        $forms = Form::model()->findAll();
        $this->render('index', array('forms' => $forms));
    }

    /**
     * Просмотр ответов
     */
    public function actionView($id, $is_new = 1)
    {
        $form = Form::model()->findByPk($id);
        if ($form != null) {
            $this->render('view', array('form' => $form));
        }
    }
}