<?php

class OrdersController extends CtrlController
{
    public function actionIndex($status_id = null, $start = 0)
    {
        $ordersCriteria = new CDbCriteria;
        if ($status_id != null) {
            $ordersCriteria->compare('status_id', $status_id);
        }
        $ordersCriteria->limit = 25;
        $ordersCriteria->offset = $start;
        $ordersCriteria->order = 'create_time desc';
        $orders = Order::model()->findAll($ordersCriteria);

        $this->render('index', array(
                'status_id' => $status_id,
                'orders' => $orders,
            )
        );
    }

    public function actionStatus($order_id, $id)
    {
        $order = Order::model()->findByPk($order_id);
        $order->status_id = $id;
        $order->save();
        echo CJSON::encode(array('status' => 'ok'));
    }

    public function actionUpdate()
    {
        $this->render('update');
    }

    public function actionView($id)
    {
        $order = Order::model()->findByPk($id);

        $this->render('view', array(
                'order' => $order
            )
        );
    }
}