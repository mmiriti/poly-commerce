<?php

class CtrlModule extends CWebModule
{
	public function init()
	{
		$this->setImport(array(
			'ctrl.models.*',
			'ctrl.components.*',
			'ctrl.widgets.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			return true;
		}
		else
			return false;
	}
}
