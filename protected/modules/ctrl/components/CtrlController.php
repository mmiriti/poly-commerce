<?php

class CtrlController extends CController
{
    public $layout = '/layouts/ctrl';

    public $allowActions = array('index');

    public $fluid = false;

    public $adminMenuItems = array();

    public function init()
    {
        $ordersUrl = array('/ctrl/orders/index');

        $defaultStatus = OrderStatus::model()->findByAttributes(array('default' => 1));
        if ($defaultStatus != null) {
            $ordersUrl['status_id'] = $defaultStatus->id;
        }

        $orderCount = CtrlController::newOrderCount();
        $fillCount = self::newFillsCount();

        $this->adminMenuItems = array(
            array('label' => '<span class="glyphicon glyphicon-sort-by-attributes-alt"></span> Структура сайта', 'url' => array('/ctrl/structure/index')),
            array('label' => '<span class="glyphicon glyphicon-list"></span> Каталог', 'url' => array('/ctrl/catalog/index')),
            array('label' => '<span class="glyphicon glyphicon-shopping-cart"></span> Заказы ' . (($orderCount > 0) ? ('<span class="badge">' . CtrlController::newOrderCount() . '</span>') : ''), 'url' => $ordersUrl),
            array('label' => '<span class="glyphicon glyphicon-user"></span> Клиенты', 'url' => array('/ctrl/customers/index')),
            array('label' => '<span class="glyphicon glyphicon-briefcase"></span> Менеджеры', 'url' => array('/ctrl/managers/index')),
            array('label' => '<span class="glyphicon glyphicon-bullhorn"></span> Новости', 'url' => array('/ctrl/news/index')),
            array('label' => '<span class="glyphicon glyphicon-plus"></span> <b class="caret"></b>', 'url' => '#', 'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), 'submenuOptions' => array('class' => 'dropdown-menu'), 'items' => array(
                array('label' => '<span class="glyphicon glyphicon-file"></span> Файлы', 'url' => array('/ctrl/files/index')),
                array('label' => '<span class="glyphicon glyphicon-search"></span> Поиск', 'url' => array('/ctrl/search/index')),
                array('label' => '<span class="glyphicon glyphicon-pencil"></span> Формы ' . ($fillCount == 0 ? '' : '<span class="badge">' . $fillCount . '</span>'), 'url' => array('/ctrl/forms/index')),
                //array('label' => '<span class="glyphicon glyphicon-stats"></span> Аналитика', 'url' => array('/ctrl/analytics/index')),
            )),
        );
    }

    public static function newFillsCount()
    {
        $newFills = FormFill::model()->findAllByAttributes(array('is_new' => 1));

        return count($newFills);
    }

    public static function newOrderCount()
    {
        $defaultStatus = OrderStatus::model()->findByAttributes(array('default' => 1));

        if ($defaultStatus !== null) {
            return count(Order::model()->findAllByAttributes(array('status_id' => $defaultStatus->id)));
        } else {
            return 0;
        }
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('admin'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function filters()
    {
        return array(
            'accessControl',
        );
    }
}