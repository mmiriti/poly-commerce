<?php

class EditController extends BlockEditorController
{
    private function loadBlock($block, $backRedirect = true)
    {
        if (isset($_POST['StructureBlock'])) {
            $block->attributes = $_POST['StructureBlock'];
            if ($block->save()) {
                if ($backRedirect) {
                    if ($block->item_id == null) {
                        $this->redirect(array('/ctrl/structure/index', '#' => 'block' . $block->id));
                    } else {
                        $this->redirect(array('/ctrl/structure/edit', 'id' => $block->item_id, '#' => 'block' . $block->id));
                    }
                } else {
                    $this->redirect(array_merge(array($this->id . '/' . $this->action->id), $_GET));
                }
            }
        }
    }

    public function actionText($id)
    {
        $this->block = $block = StructureBlock::model()->findByPk($id);

        if ($block != null) {
            $this->loadBlock($block);
            $this->render('text', array(
                'block' => $block
            ));
        } else {
            throw new CHttpException(404, 'Не найдено');
        }
    }

    public function actionNews($id)
    {
        $this->block = $block = StructureBlock::model()->findByPk($id);

        if ($block != null) {
            if (isset($_POST['Data'])) {
                $block->data = serialize($_POST['Data']);
                $block->save();
            }
            $this->loadBlock($block);

            $data = $block->data == null ? array() : unserialize($block->data);

            $this->render('news', array(
                'block' => $block,
                'data' => $data,
            ));
        } else {
            throw new CHttpException(404, 'Не найдено');
        }
    }

    public function actionGallery($id, $delete_item = null)
    {
        $this->block = $block = StructureBlock::model()->findByPk($id);

        if ($block != null) {
            $this->loadBlock($block, false);

            $item = new StructureBlockGalleryItem();
            $item->block_id = $block->id;

            if (isset($_POST['StructureBlockGalleryItem'])) {
                $item->attributes = $_POST['StructureBlockGalleryItem'];

                $file = File::upload('image_file');

                if ($file != null) {
                    $item->file_id = $file->id;
                }

                if ($item->save()) {
                    $this->redirect(array('gallery', 'id' => $id));
                } else {
                    if ($file != null)
                        $file->delete();
                }
            }

            if ($delete_item != null) {
                $item = StructureBlockGalleryItem::model()->findByPk($delete_item);
                $item->delete();
                $this->redirect(array('gallery', 'id' => $id));
            }

            $this->render('gallery', array('block' => $block, 'item' => $item));
        } else {
            throw new CHttpException(404, 'Не найдено');
        }
    }
} 