<?php
/** @var $this CtrlController */

$this->pageTitle = 'Текстовый блок "' . $this->block->title . '"';
echo CHtml::beginForm();
$this->renderPartial('_common_form', array('block' => $block));
$this->widget('FormGroup', array('model' => $block, 'fieldName' => 'data', 'type' => 'textarea', 'htmlOptions' => array('class' => 'wysiwyg', 'rows' => 20)));
$this->widget('FormButton');
echo CHtml::endForm();