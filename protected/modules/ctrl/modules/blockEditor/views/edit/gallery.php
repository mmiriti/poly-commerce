<?php
/** @var $this CtrlController */
$this->pageTitle = 'Редактировать галерею "' . $block->title . '"';
?>
<style>
    .thumbnail img {
        height: 100px;
    }
</style>
<div class="panel panel-default">
    <div class="panel-heading">Параметры блока</div>
    <div class="panel-body">
        <?php
        echo CHtml::beginForm();
        $this->renderPartial('_common_form', array('block' => $block));
        $this->widget('FormButton');
        echo CHtml::endForm();
        ?>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        Загрузить фотографию
    </div>
    <div class="panel-body">
        <?php echo CHtml::beginForm('', 'post', array('enctype' => 'multipart/form-data')); ?>
        <?php $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'title')); ?>
        <?php $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'description', 'type' => 'textarea')); ?>
        <?php $this->widget('FormGroup', array('model' => $item, 'fieldName' => 'position')); ?>
        <div class="form-group">
            <label for="">Файл</label>
            <input type="file" name="image_file">
        </div>
        <?php $this->widget('FormButton', array('title' => 'Загрузить')); ?>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>

<h2>Элементы</h2>
<hr/>
<div class="row">
    <?php
    foreach ($block->galleryItems as $item) {
        ?>
        <div class="col-md-3">
            <div class="thumbnail">
                <img class="img-responsive" src="<?php echo $item->file->url; ?>" alt="">

                <p class="caption"><span class="badge"><?php echo $item->position; ?></span> <?php echo $item->title; ?>
                </p>

                <p><?php echo CHtml::link('<span class="glyphicon glyphicon-remove"></span>', array('gallery', 'id' => $block->id, 'delete_item' => $item->id), array('class' => 'btn btn-danger btn-xs')); ?></p>
            </div>
        </div>
    <?php
    }
    ?>
</div>