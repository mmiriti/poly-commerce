<?php
/** @var $block StructureBlock */
/** @var $this BlockEditorController */

$this->pageTitle = 'Новостной блок';
echo CHtml::beginForm();
$this->renderPartial('_common_form', array('block' => $block));
?>
    <div class="form-group">
        <label for="">Количество новостей в блоке</label>
        <?php echo CHtml::textField('Data[news_count]', isset($data['news_count']) ? $data['news_count'] : '', array('class' => 'form-control')); ?>
    </div>
<?php
$this->widget('FormButton');
echo CHtml::endForm();
?>