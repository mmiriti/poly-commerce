<?php
$this->beginContent('application.modules.ctrl.views.layouts.ctrl');
?>
    <h1><?php echo $this->pageTitle; ?></h1>
    <hr/>
    <div class="well">
        <?php
        if ($this->block->item_id != null) {
            echo CHtml::link('<span class="glyphicon glyphicon-chevron-left"></span> Назад к "' . $this->block->item->name . '"', array('/ctrl/structure/edit', 'id' => $this->block->item_id, '#' => 'block' . $this->block->id), array('class' => 'btn btn-default'));
        } else {
            echo CHtml::link('<span class="glyphicon glyphicon-chevron-left"></span> Структура сайта', array('/ctrl/structure/index'), array('class' => 'btn btn-default'));
        }
        ?>
    </div>
<?php
echo $content;
$this->endContent();