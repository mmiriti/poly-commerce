<?php

/**
 * Class EditParam
 *
 * @property Parameter $param
 * @property CatalogItem $item
 */
class EditParam extends CWidget
{
	public $param;
	public $item;

	public function run()
	{
		$value = ItemParameterValue::model()->findByAttributes(array('catalog_item_id' => $this->item->id, 'parameter_id' => $this->param->id));
		if($value == null)
		{
			$value = new ItemParameterValue('create');
			$value->catalog_item_id = $this->item->id;
			$value->parameter_id = $this->param->id;
			$value->save();
		}

		$this->render('editParam', array('value' => $value));
	}
}