<?php

class CategoryDropDown extends CWidget
{
	public $fieldName;
	public $selected = null;

	public function run()
	{
		$this->render('categoryDropDown');
	}
}