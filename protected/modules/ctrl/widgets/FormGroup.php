<?php

class FormGroup extends CWidget
{
	public $listData = null;
	public $type = 'text';
	public $fieldName;
	public $model;
	public $hint = null;
	public $htmlOptions = array();
    public $value_field = null;
    public $text_field = null;
    public $parent_field = null;
    public $add_root = false;
    public $rootModels = array();

    public function run()
	{
		$this->render('formGroup');
	}
}