<?php
$this->widget('FormGroup', array('model' => $this->document, 'fieldName' => 'keywords', 'type' => 'textarea', 'htmlOptions' => array('rows' => 3)));
$this->widget('FormGroup', array('model' => $this->document, 'fieldName' => 'description', 'type' => 'textarea', 'htmlOptions' => array('rows' => 3)));
$this->widget('FormGroup', array('model' => $this->document, 'fieldName' => 'html', 'type' => 'textarea', 'htmlOptions' => array('rows' => 50, 'class' => 'form-control wysiwyg')));
?>