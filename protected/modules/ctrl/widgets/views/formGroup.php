<?php
/* @var $this FormGroup */

echo CHtml::openTag('div', array('class' => 'form-group' . ((isset($this->model->errors[$this->fieldName])) ? ' has-error' : ''), 'id' => 'formGroup_' . $this->id));

echo CHtml::activeLabelEx($this->model, $this->fieldName, array('class' => 'control-label')); ?>
<?php
switch ($this->type) {
    case 'checkbox':
        echo CHtml::activeCheckBox($this->model, $this->fieldName, $this->htmlOptions);
        break;

    case 'email':
        echo CHtml::activeEmailField($this->model, $this->fieldName, array_merge(array('class' => 'form-control'), $this->htmlOptions));
        break;

    case 'password':
        echo CHtml::activePasswordField($this->model, $this->fieldName, array_merge(array('class' => 'form-control'), $this->htmlOptions));
        break;

    case 'textarea':
        echo CHtml::activeTextArea($this->model, $this->fieldName, array_merge(array('class' => 'form-control'), $this->htmlOptions));
        break;

    case 'dropdown':
        echo CHtml::activeDropDownList($this->model, $this->fieldName, $this->listData, array_merge(array('class' => 'form-control'), $this->htmlOptions));
        break;

    case 'listTree':
        echo PHtml::activeDropDownTree($this->model, $this->rootModels, $this->fieldName, $this->value_field, $this->text_field, $this->parent_field, $this->add_root, array_merge(array('class' => 'form-control'), $this->htmlOptions));
        break;

    case 'date':
        echo CHtml::activeTextField($this->model, $this->fieldName, array_merge(array('class' => 'form-control date'), $this->htmlOptions));
        break;

    case 'file':
        echo CHtml::activeFileField($this->model, $this->fieldName);
        break;

    case 'yesno':
        echo CHtml::activeDropDownList($this->model, $this->fieldName, array(
            0 => 'нет',
            1 => 'да',
        ), array_merge(array('class' => 'form-control'), $this->htmlOptions));
        break;

    default:
        echo CHtml::activeTextField($this->model, $this->fieldName, array_merge(array('class' => 'form-control'), $this->htmlOptions));
        break;
}

if ($this->hint != null) {
    ?>
    <span class="help-block">
			<?php echo $this->hint; ?>
		</span>
<?php
}

if (isset($this->model->errors[$this->fieldName])) {
    ?>
    <span class="help-block">
			<?php
            foreach ($this->model->errors[$this->fieldName] as $key => $value) {
                echo '<p>' . $value . '</p>';
            }
            ?>
		</span>
<?php
}
?>
</div>