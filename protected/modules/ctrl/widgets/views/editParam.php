<?php
/* @var $this EditParam */
/* @var $value ItemParameterValue */
?>
<div class="form-group">
    <label for=""><?php echo $this->param->name; ?></label>
    <?php
    $fieldName = 'ParameterValue[' . $value->id . ']';
    switch ($this->param->type) {
        case 'bool':
            echo CHtml::dropDownList($fieldName, $value->value, array(0 => 'нет', 1 => 'да'), array('class' => 'form-control'));
            break;

        case 'classifier':
            $treeData = PHtml::treeData(Classifier::model()->findAllByAttributes(array('group_id' => $this->param->classifier_id, 'parent_id' => null)), 'id', 'title', 'parent_id');
            echo CHtml::dropDownList($fieldName, $value->value, $treeData, array('class' => 'form-control'));
            break;

        default:
            echo CHtml::textField($fieldName, $value->value, array('class' => 'form-control'));
            break;
    }
    ?>
</div>