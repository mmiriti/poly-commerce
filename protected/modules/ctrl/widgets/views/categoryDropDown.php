<?php

$root = CatalogCategory::model()->findAllByAttributes(array('parent_id' => null));

function renderCategoryList($category, $ident = '', $selected = null)
{
	foreach ($category as $c) {
		?>
		<option value="<?php echo $c->id; ?>"<?php if($selected == $c->id) echo " selected"; ?>><?php echo $ident . ' ' . $c->name; ?></option>
		<?php
		renderCategoryList($c->catalogCategories, $ident.'&nbsp;&nbsp;', $selected);
	}
}
?>

<select name="<?php echo $this->fieldName; ?>" id="<?php echo $this->id; ?>" class="form-control">
<option value="">[ корень ]</option>
<?php
renderCategoryList($root, '', $this->selected);
?>
</select>