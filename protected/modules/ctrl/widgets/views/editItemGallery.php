<script type="text/javascript">
	$(function () {
		updateGallery(<?php echo $this->item->id; ?>);
	});
</script>
<div class="row">
	<div class="col-md-12">
		<h2 class="folding-header" data-folding-selector="#itemGallery"><a href="#">Галерея товара <span class="caret"></span></a></h2>

		<div id="itemGallery">
			<div class="row">
				<div class="col-md-12" id="item-gallery-<?php echo $this->item->id; ?>"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="file-drop-area" data-item-id="<?php echo $this->item->id; ?>"
						 id="item-gallery-drop-<?php echo $this->item->id; ?>">
						Перетащите сюда файлы для загрузки
					</div>
					<div class="panel panel-default">
						<div class="panel-body">
							Или выбирите файлы на диске: <input type="file" id="galleryClassicFiles" multiple>
						</div>
						<div class="panel-footer">
							<a class="btn btn-default" href="#" id="galleryClassicUploadFiles" data-item-id="<?php echo $this->item->id; ?>">Загрузить</a>
                            <span class="pull-right" id="galleryClassicUploadStatus"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>