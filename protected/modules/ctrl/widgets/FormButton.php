<?php

class FormButton extends CWidget
{
	public $type = 'submit';
	public $title = 'Сохранить';
	
	public function run()
	{
		$this->render('formButton');
	}
}