<?php

class CartHelper
{
    public static function getCount()
    {
        if (isset(Yii::app()->session['cart']['items'])) {
            return count(Yii::app()->session['cart']['items']);
        } else {
            return 0;
        }
    }
} 