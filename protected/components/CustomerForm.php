<?php

/**
 * Class CustomerForm
 *
 * Форма клиента
 * Абстракция, которая позволяет обращаться с формой клиента как с обычной моделью Yii
 *
 * @property integer $customer_id
 */
class CustomerForm extends CModel
{
    public $customer_id = null;

    private static $_customerDataFields = null;
    private static $_customerDataFields_assoc = array();

    private $values = array();

    /**
     * @return CustomerDataField[]
     */
    public static function getCustomerDataFields()
    {
        if (self::$_customerDataFields == null) {
            $c = new CDbCriteria();
            $c->order = 'position';
            $c->compare('fillable', 1);
            self::$_customerDataFields = CustomerDataField::model()->findAll($c);

            foreach (self::$_customerDataFields as $dataField) {
                self::$_customerDataFields_assoc[$dataField->slug] = $dataField;
            }
        }
        return self::$_customerDataFields;
    }

    public function __get($name)
    {
        if (isset(CustomerForm::$_customerDataFields_assoc[$name])) {
            if (isset($this->values[$name])) {
                return $this->values[$name];
            } else {
                return '';
            }
            return 'hello';
        } else {
            return parent::__get($name);
        }
    }

    public function __set($name, $value)
    {
        if (isset(CustomerForm::$_customerDataFields_assoc[$name])) {
            $this->values[$name] = $value;
        } else
            parent::__set($name, $value);
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rules()
    {
        $fields = 'customer_id';
        $required_fields = array();

        $customerDataFields = CustomerForm::getCustomerDataFields();

        $result = array();

        foreach ($customerDataFields as $dataField) {
            $fields .= ', ' . $dataField->slug;
            if ($dataField->mandatory) {
                $required_fields[] = $dataField->slug;
            }

            if ($dataField->type == 'number') {
                $result[] = array($dataField->slug, 'numerical');
            }
        }

        $result[] = array('customer_id', 'numerical', 'integerOnly' => true);
        $result[] = array($fields, 'safe');

        if (count($required_fields) > 0) {
            $result[] = array(implode(', ', $required_fields), 'required');
        }

        return $result;
    }

    /**
     * Список аттрибутов клиента
     *
     * @return array
     */
    public function attributeNames()
    {
        $result = array(
            'customer_id',
        );

        $customerDataFields = CustomerForm::getCustomerDataFields();

        foreach ($customerDataFields as $dataField) {
            $result[] = $dataField->name;
        }

        return $result;
    }

    /**
     * Название аттрибутов
     *
     * @return array
     */
    public function attributeLabels()
    {
        $result = array(
            'customer_id' => 'Идентификатор клиента',
        );

        $customerDataFields = CustomerForm::getCustomerDataFields();

        foreach ($customerDataFields as $dataField) {
            $result[$dataField->slug] = $dataField->name;
        }

        return $result;
    }

    /**
     * Save data
     */
    public function save()
    {
        if ($this->customer_id != null) {
            $result = true;
            $customerDataFields = CustomerForm::getCustomerDataFields();

            foreach ($customerDataFields as $field) {
                $value = new CustomerDataValue();
                $value->customer_id = $this->customer_id;
                $value->field_id = $field->id;
                $value->value = $this->values[$field->slug];
                if ($value->save()) {
                    $result &= true;
                } else {
                    $this->addErrors($value->errors);
                    $result &= false;
                }
            }

            return $result;

        } else {
            $this->addError('customer_id', 'Для сохранения необходимо задать идентификатор клиента');
            return false;
        }
    }
} 