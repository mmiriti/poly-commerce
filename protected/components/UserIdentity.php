<?php

class UserIdentity extends CUserIdentity
{
	private $_id;
	private $_name;

	public function authenticate()
	{
		$user = User::model()->findByAttributes(array('email' => $this->username));

		if($user === null)
		{
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}else
		{
			if($user->pwd_hash === md5($this->password))
			{
				$this->_id = $user->id;
				$this->_name = $user->userGroup->role;
				$this->errorCode = self::ERROR_NONE;
			}else
			{
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			}
		}

		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}

	public function getName()
	{
		return $this->_name;
	}
}