<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 7/7/14
 * Time: 5:39 PM
 */
class Theme
{
    private static $currentTheme = null;
    public $info = array();

    public static function getList()
    {
        return array();
    }

    public static function current()
    {
        if (self::$currentTheme == null) {
            self::$currentTheme = new Theme(Options::get('site_theme', 'bootstrap'));
        }

        return self::$currentTheme;
    }

    public function __construct($name)
    {
        $this->info = include 'themes/' . $name . '/info.php';
    }
} 