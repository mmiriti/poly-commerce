<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 7/7/14
 * Time: 8:12 PM
 */
class UrlRules extends CBaseUrlRule
{

    public function createUrl($manager, $route, $params, $ampersand)
    {
        if ($route == 'site/structure') {
            if (isset($params['slug'])) {
                if ($params['slug'] == 'index') {
                    return '';
                }
                return $params['slug'];
            }
        }
        return false;
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        $structure = StructureItem::model()->findByAttributes(array('slug' => $pathInfo));
        if ($structure != null) {
            $_GET['slug'] = $pathInfo;
            return 'site/structure';
        }
        return false;
    }
}