<?php

/**
 * Class Controller
 *
 * @property $commonBlocks[] StructureBlock
 */
class Controller extends CController
{
    public $layout = '//layouts/index';
    public $commonBlocks = array();

    public $menu = array(
        array('label' => 'Главная', 'url' => array('site/index'))
    );

    public $breadcrumbs = array();

    public function init()
    {
        Yii::app()->theme = Options::get('site_theme', 'bootstrap');

        $this->layout = '//layouts/' . Theme::current()->info['defaultLayout'];

        $commonBlocks = StructureBlock::model()->findAllByAttributes(array('item_id' => null));

        foreach ($commonBlocks as $block) {
            $block_slug = ($block->slug == null) ? $block->id : $block->slug;
            $this->commonBlocks[$block_slug] = $block;
        }

        if (StructureItem::model()->findByAttributes(array('slug' => 'index')) != null) {
            $this->defaultAction = 'structure';
        } else {
            $this->defaultAction = 'index';
        }

        parent::init();
    }

}