<?php

class PHtml
{
    /**
     * Вывести лейбл для CustomerDataField
     *
     * @param CustomerDataField $customerDataField
     */
    public static function customerDataFieldLabel($customerDataField, $htmlOptions = array())
    {
        return CHtml::label($customerDataField->name . ($customerDataField->mandatory == 1 ? ' <span class="required">*</span>' : ''), get_class($customerDataField) . '_' . $customerDataField->slug, $htmlOptions);
    }

    /**
     * Вывести поле для ввода CustomerDataField
     *
     * @param CustomerDataField $customerDataField
     * @param CustomerDataValue $customerDataFieldValue
     * @param array $htmlOptions
     */
    public static function customerDataFieldInput($customerDataField, $customerDataFieldValue = null, $htmlOptions = array())
    {
        $fieldName = get_class($customerDataField) . '[' . $customerDataField->slug . ']';
        switch ($customerDataField->type) {
            case 'boolean':
                echo CHtml::dropDownList($fieldName, $customerDataFieldValue == null ? null : $customerDataFieldValue->value, array(
                    0 => 'нет',
                    1 => 'да',
                ), $htmlOptions);
            default:
                echo CHtml::textField($fieldName, $customerDataFieldValue == null ? '' : $customerDataFieldValue->value, $htmlOptions);
        }
    }

    /**
     * @param $models CActiveRecord[]
     * @param $value_field
     * @param $text_field
     * @param $parent_field
     * @param string $shift
     * @return array
     */
    public static function treeData($models, $value_field, $text_field, $parent_field, $shift = '')
    {
        $result = array();

        foreach ($models as $model) {
            $result[$model->{$value_field}] = $shift . ' ' . $model->{$text_field};
            $children = $model->findAllByAttributes(array($parent_field => $model->primaryKey));

            $result = $result + self::treeData($children, $value_field, $text_field, $parent_field, $shift . '-');
        }

        return $result;
    }

    /**
     * @param $model CActiveRecord
     * @param $field
     * @param $value_field
     * @param $text_field
     * @param $parent_field
     * @param bool $addRoot
     * @param array $htmlOptions
     * @return string
     */
    public static function activeDropDownTree($model, $rootModels, $field, $value_field, $text_field, $parent_field, $addRoot = false, $htmlOptions = array())
    {
        $listData = self::treeData($rootModels, $value_field, $text_field, $parent_field);

        if ($addRoot) {
            $listData = array(null => '- корень -') + $listData;
        }

        return CHtml::activeDropDownList($model, $field, $listData, $htmlOptions);
    }
} 