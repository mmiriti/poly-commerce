<?php

class m140414_105919_prices extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('currency', array(
			'id' => 'pk',
			'full_name' => 'varchar(64) not null',
			'sign' => 'varchar(5) not null'
			), 'engine=InnoDB');

		$this->createTable('item_price', array(
			'id' => 'pk',
			'item_id' => 'int',
			'currency_id' => 'int',
			'create_time' => 'TIMESTAMP default CURRENT_TIMESTAMP',
			'value' => 'float(10,2) not null',
			'comment' => 'string null'
			), 'engine=InnoDB');

		$this->addForeignKey('item_price_currency', 'item_price', 'currency_id', 'currency', 'id');
		$this->addForeignKey('item_price_item', 'item_price', 'item_id', 'catalog_item', 'id');
	}

	public function safeDown()
	{
		return false;
	}
}