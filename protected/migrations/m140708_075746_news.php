<?php

class m140708_075746_news extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('news_item', array(
            'id' => 'pk',
            'create_time' => 'timestamp default current_timestamp',
            'update_time' => 'timestamp null',
            'publish_date_time' => 'datetime',
            'author_id' => 'int',
            'document_id' => 'int',
            'title' => 'string',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_news_item_author', 'news_item', 'author_id', 'user', 'id');
        $this->addForeignKey('fk_news_item_document', 'news_item', 'document_id', 'document', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}