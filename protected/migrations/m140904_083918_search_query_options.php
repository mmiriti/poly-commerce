<?php

class m140904_083918_search_query_options extends CDbMigration
{
	public function safeUp()
	{
        $this->addColumn('search_query', 'options', 'text null');
	}

	public function safeDown()
	{
        return false;
	}
}