<?php

class m140406_134039_parameters extends CDbMigration
{
	public function safeUp()
	{
		try{
			$this->createTable('parameter', array(
				'id' => 'pk',
				'name' => 'string not null',
				'type' => "enum('string', 'number', 'bool') not null default 'string'"
				), 'engine=InnoDB');

			$this->createTable('group_parameter', array(
				'id' => 'pk',
				'group_id' => 'int',
				'parameter_id' => 'int'
				), 'engine=InnoDB');

			$this->addForeignKey('group_parameter_group', 'group_parameter', 'group_id', 'catalog_item_group', 'id');
			$this->addForeignKey('group_parameter_parameter', 'group_parameter', 'parameter_id', 'parameter', 'id');

			$this->createTable('item_parameter_value', array(
				'id' => 'pk',
				'catalog_item_id' => 'int',
				'parameter_id' => 'int',
				'value_string' => 'string null',
				'value_number' => 'float(12,3) null',
				'value_bool' => 'tinyint null',
				), 'engine=InnoDB');

			$this->addForeignKey('item_parameter_value_catalog_item', 'item_parameter_value', 'catalog_item_id', 'catalog_item', 'id');
			$this->addForeignKey('item_parameter_value_parameter', 'item_parameter_value', 'parameter_id', 'parameter', 'id');
		} catch (Exception $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
	}
}