<?php

class m140907_143902_default_order_status extends CDbMigration
{
    public function safeUp()
    {
        $this->insert('order_status', array(
            'slug' => 'new',
            'name' => 'Новый',
            'default' => 1
        ));
    }

    public function safeDown()
    {
        return false;
    }
}