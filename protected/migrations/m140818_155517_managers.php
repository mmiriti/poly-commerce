<?php

class m140818_155517_managers extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('manager', array(
            'id' => 'pk',
            'create_time' => 'timestamp default current_timestamp',
            'user_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_manager_user', 'manager', 'user_id', 'user', 'id');

        $this->createTable('manager_customer', array(
            'manager_id' => 'int',
            'customer_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_manager_customer_manager', 'manager_customer', 'manager_id', 'manager', 'id');
        $this->addForeignKey('fk_manager_customer_customer', 'manager_customer', 'customer_id', 'customer', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('manager');
    }
}