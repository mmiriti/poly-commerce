<?php

class m140904_070005_linked_items extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('catalog_item_link', array(
            'id' => 'pk',
            'position' => 'int',
            'catalog_item_1_id' => 'int',
            'catalog_item_2_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_link_item_1', 'catalog_item_link', 'catalog_item_1_id', 'catalog_item', 'id');
        $this->addForeignKey('fk_link_item_2', 'catalog_item_link', 'catalog_item_2_id', 'catalog_item', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}