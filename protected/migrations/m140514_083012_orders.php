<?php

class m140514_083012_orders extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->createTable('customer', array(
				'id' => 'pk',
				'user_id' => 'int null',
				'create_time' => 'TIMESTAMP default CURRENT_TIMESTAMP'
				), 'engine=InnoDB');

			$this->addForeignKey('fk_customer_user', 'customer', 'user_id', 'user', 'id');

			$this->createTable('customer_data_field', array(
				'id' => 'pk',
				'slug' => 'varchar(64) not null',
				'name' => 'varchar(64) not null',
				'type' => "enum('string', 'number', 'boolean') not null default 'string'",
				), 'engine=InnoDB');

			$this->createTable('customer_data_value', array(
				'id' => 'pk',
				'customer_id' => 'int',
				'field_id' => 'int',
				'value_string' => 'text null',
				'value_number' => 'float(10,3) null',
				'value_boolean' => 'tinyint(1) null'
				), 'engine=InnoDB');

			$this->addForeignKey('fk_customer_data_value_customer', 'customer_data_value', 'customer_id', 'customer', 'id');
			$this->addForeignKey('fk_customer_data_value_field', 'customer_data_value', 'field_id', 'customer_data_field', 'id');
		} catch (Exception $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}