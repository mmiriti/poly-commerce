<?php

class m140903_144616_currency_more_signs extends CDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('currency_exchange', 'ratio', 'float(8,4) not null');
    }

    public function safeDown()
    {
        return false;
    }
}