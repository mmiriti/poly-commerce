<?php

class m140630_071754_structure_slugs extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('structure_item', 'slug', 'varchar(64) null');
        $this->createIndex('structure_item_unique_slug', 'structure_item', 'slug', true);

        $this->addColumn('structure_block', 'slug', 'varchar(64) null');
        $this->createIndex('structure_block_unique_slug', 'structure_block', 'slug', true);
    }

    public function safeDown()
    {
        return false;
    }
}