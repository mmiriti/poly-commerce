<?php

class m140820_130000_managers_simplify extends CDbMigration
{
    public function safeUp()
    {
        $this->dropTable('manager_customer');
        $this->dropTable('manager');

        $this->createTable('manager_customer', array(
            'manager_id' => 'int',
            'customer_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_manager_customer_manager', 'manager_customer', 'manager_id', 'user', 'id');
        $this->addForeignKey('fk_manager_customer_customer', 'manager_customer', 'customer_id', 'customer', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}