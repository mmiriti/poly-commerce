<?php

class m140830_094629_classifier_data extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('classifier', 'data', 'text null');
    }

    public function safeDown()
    {
        return false;
    }
}