<?php

class m140704_070946_option_editable extends CDbMigration
{
    public function safeUp()
    {
        try {
            $this->addColumn('options', 'editable', 'tinyint default 1');
        } catch (Exception $e) {
            return false;
        }
    }

    public function safeDown()
    {
        $this->dropColumn('options', 'editable');
    }
}