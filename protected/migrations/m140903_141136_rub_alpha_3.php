<?php

class m140903_141136_rub_alpha_3 extends CDbMigration
{
    public function safeUp()
    {
        $this->update('currency', array('alpha_3' => 'RUR'), 'full_name = :full_name', array(':full_name' => 'Рубли'));
    }

    public function safeDown()
    {
        return false;
    }
}