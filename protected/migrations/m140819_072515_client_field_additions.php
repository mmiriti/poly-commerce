<?php

class m140819_072515_client_field_additions extends CDbMigration
{
	public function safeUp()
	{
        $this->addColumn('customer_data_field', 'fillable', 'tinyint(1) default 1');
	}

	public function safeDown()
	{
        return false;
	}
}