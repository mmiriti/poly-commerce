<?php

class m140912_162203_catalog_item_name_index extends CDbMigration
{
    public function safeUp()
    {
        $this->createIndex('idx_catalog_item_name', 'catalog_item', 'name', false);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_catalog_item_name', 'catalog_item');
    }
}