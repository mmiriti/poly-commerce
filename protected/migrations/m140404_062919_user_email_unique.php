<?php

class m140404_062919_user_email_unique extends CDbMigration
{
	public function safeUp()
	{
		$this->createIndex('user_email_unique', 'user', 'email', true);
	}

	public function safeDown()
	{
		$this->dropIndex('user_email_unique', 'user');
	}
}