<?php

class m140903_142348_currency_exchange extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('currency_exchange', array(
            'id' => 'pk',
            'currency_from_id' => 'int',
            'currency_to_id' => 'int',
            'ratio' => 'float(8,3) not null',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_currency_exchange_from', 'currency_exchange', 'currency_from_id', 'currency', 'id');
        $this->addForeignKey('fk_currency_exchange_to', 'currency_exchange', 'currency_to_id', 'currency', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}