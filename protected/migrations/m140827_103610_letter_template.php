<?php

class m140827_103610_letter_template extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('letter_template', array(
            'id' => 'pk',
            'create_time' => 'timestamp default current_timestamp',
            'slug' => 'varchar(64) not null',
            'name' => 'string',
            'subject' => 'string',
            'content' => 'text',
        ), 'engine=InnoDB');

        $this->createIndex('idx_letter_template_slug', 'letter_template', 'slug', true);
    }

    public function safeDown()
    {
        $this->dropTable('letter_template');
    }
}