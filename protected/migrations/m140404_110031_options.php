<?php

class m140404_110031_options extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('options', array(
			'id' => 'pk',
			'option' => 'varchar(32) not null',
			'option_name' => 'string',
			'option_value' => 'text not null',
			'option_type' => "enum('string', 'text', 'int', 'float') not null default 'string'"
			), 'engine=InnoDB'); 

		$this->createIndex('option_unique_option', 'options', 'option', true);
	}

	public function safeDown()
	{
		return false;
	}
}