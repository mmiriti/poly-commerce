<?php

class m140915_152818_catalog_category_additions extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('catalog_category', 'surcharge', 'float(8,4) null');
        $this->addColumn('catalog_category', 'file_id', 'int');

        $this->addForeignKey('fk_catalog_category_file_id', 'catalog_category', 'file_id', 'file', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}