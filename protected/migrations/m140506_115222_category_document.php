<?php

class m140506_115222_category_document extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->addColumn('catalog_category', 'document_id', 'int null');
			$this->addForeignKey('fk_catalog_category_document', 'catalog_category', 'document_id', 'document', 'id');
		} catch(Ecxeption $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}