<?php

class m140708_091204_news_block extends CDbMigration
{

    public function safeUp()
    {
        try {
            $this->alterColumn('structure_block', 'type', "enum('gallery', 'text', 'news') null");
        } catch (Exception $e) {
            return false;
        }
    }

    public function safeDown()
    {
        return false;
    }
}