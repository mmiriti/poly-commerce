<?php

class m140707_074157_gallery_item_file_fk extends CDbMigration
{
	public function safeUp()
	{
        $this->addForeignKey('fk_gallery_item_file', 'structure_block_gallery_item', 'file_id', 'file', 'id');
	}

	public function safeDown()
	{
        return false;
	}
}