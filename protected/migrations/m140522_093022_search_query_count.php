<?php

class m140522_093022_search_query_count extends CDbMigration
{
	public function safeUp()
	{
		try {
			$this->addColumn('search_query', 'count', 'int default 0');
		} catch (Exception $e) {
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}