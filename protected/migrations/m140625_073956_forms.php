<?php

class m140625_073956_forms extends CDbMigration
{
    public function safeUp()
    {
        try {
            $this->createTable('form', array(
                'id' => 'pk',
                'name' => 'varchar(64) not null',
                'slug' => 'varchar(32) not null',
            ), 'engine=InnoDB');

            $this->createIndex('ui_unique_slug', 'form', 'slug', true);

            $this->createTable('form_field', array(
                'id' => 'pk',
                'form_id' => 'int',
                'name' => 'varchar(64) not null',
                'required' => 'tinyint default 0',
            ), 'engine=InnoDB');

            $this->addForeignKey('fk_form_field_form', 'form_field', 'form_id', 'form', 'id');

            $this->createTable('form_fill', array(
                'id' => 'pk',
                'fill_time' => 'TIMESTAMP default CURRENT_TIMESTAMP',
                'form_id' => 'int',
                'user_id' => 'int null',
                'is_new' => 'tinyint default 0'
            ), 'engine=InnoDB');

            $this->addForeignKey('fk_form_fill_form', 'form_fill', 'form_id', 'form', 'id');

            $this->createTable('form_fill_value', array(
                'id' => 'pk',
                'fill_id' => 'int',
                'form_id' => 'int',
                'field_id' => 'int',
                'value' => 'text'
            ), 'engine=InnoDB');

            $this->addForeignKey('fk_form_fill_value_fill', 'form_fill_value', 'fill_id', 'form_fill', 'id');
            $this->addForeignKey('fk_form_fill_value_form', 'form_fill_value', 'form_id', 'form', 'id');
            $this->addForeignKey('fk_form_fill_value_field', 'form_fill_value', 'field_id', 'form_field', 'id');

        } catch (Exception $e) {
            return false;
        }
    }

    public function safeDown()
    {
        return false;
    }
}