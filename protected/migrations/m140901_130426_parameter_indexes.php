<?php

class m140901_130426_parameter_indexes extends CDbMigration
{
    public function safeUp()
    {
        $this->createIndex('idx_parameter_string_value', 'item_parameter_value', 'value_string');
        $this->createIndex('idx_parameter_bool_value', 'item_parameter_value', 'value_bool');
        $this->createIndex('idx_parameter_number_value', 'item_parameter_value', 'value_number');
    }

    public function safeDown()
    {
        return false;
    }
}