<?php

class m140626_123133_form_message extends CDbMigration
{
	public function safeUp()
	{
        $this->addColumn('form', 'message', 'text');
	}

	public function safeDown()
	{
        return false;
	}
}