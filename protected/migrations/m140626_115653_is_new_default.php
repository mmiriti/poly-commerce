<?php

class m140626_115653_is_new_default extends CDbMigration
{
    public function safeUp()
    {
        try {
            $this->alterColumn('form_fill', 'is_new', 'tinyint default 1');
        } catch (Exception $e) {
            return false;
        }
    }

    public function safeDown()
    {
        return false;
    }
}