<?php

class m140817_180641_customer_field_mandatory extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('customer_data_field', 'mandatory', 'tinyint(1) default 0');
    }

    public function safeDown()
    {
        $this->dropColumn('customer_data_field', 'mandatory');
    }
}