<?php

class m140414_123410_roubles extends CDbMigration
{
	public function safeUp()
	{
		$this->insert('currency', array(
			'full_name' => 'Рубли',
			'sign' => 'руб.'
			)
		);
	}

	public function safeDown()
	{
		return false;
	}
}