<?php

class m140915_210543_customer_surcharge_pk extends CDbMigration
{
    public function up()
    {
        $this->delete('customer_category_surcharge');
        $this->addColumn('customer_category_surcharge', 'id', 'pk');
    }

    public function down()
    {
        echo "m140915_210543_customer_surcharge_pk does not support migration down.\n";
        return false;
    }
}