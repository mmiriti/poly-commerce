<?php

class m140705_093701_menus extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('menu', array(
            'id' => 'pk',
            'name' => 'varchar(128) not null',
            'slug' => 'varchar(64) not null',
        ), 'engine=InnoDB');

        $this->createTable('menu_item', array(
            'id' => 'pk',
            'menu_id' => 'int',
            'position' => 'int',
            'title' => 'varchar(128) not null',
            'action' => 'varchar(64) null',
            'get_data' => 'text null',
            'href' => 'varchar(255) null',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_menu_item_menu', 'menu_item', 'menu_id', 'menu', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}