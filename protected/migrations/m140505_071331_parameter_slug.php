<?php

class m140505_071331_parameter_slug extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->addColumn('parameter', 'slug', 'varchar(16) null');
		} catch(Exception $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}