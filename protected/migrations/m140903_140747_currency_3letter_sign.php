<?php

class m140903_140747_currency_3letter_sign extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('currency', 'alpha_3', 'char(3) not null');
        $this->createIndex('idx_currency_alpha_3', 'currency', 'alpha_3', true);
    }

    public function safeDown()
    {
        return false;
    }
}