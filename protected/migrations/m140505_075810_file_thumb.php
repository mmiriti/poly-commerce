<?php

class m140505_075810_file_thumb extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->addColumn('file', 'thumb_src', 'varchar(128) null');
		} catch(Exception $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}