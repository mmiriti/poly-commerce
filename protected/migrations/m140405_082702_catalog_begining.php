<?php

class m140405_082702_catalog_begining extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('catalog_category', array(
			'id' => 'pk',
			'create_time' => 'TIMESTAMP not null default CURRENT_TIMESTAMP',
			'update_time' => 'TIMESTAMP null',
			'parent_id' => 'int null',
			'name' => 'string not null',
			'slug' => 'varchar(32) null'
			), 'engine=InnoDB');

		$this->addForeignKey('catalog_category_parent', 'catalog_category', 'parent_id', 'catalog_category', 'id');

		$this->createTable('catalog_item_group', array(
			'id' => 'pk',
			'name' => 'varchar(64) not null'
			), 'engine=InnoDB');

		$this->createTable('catalog_item', array(
			'id' => 'pk',
			'create_time' => 'TIMESTAMP not null default CURRENT_TIMESTAMP',
			'update_time' => 'TIMESTAMP null',		
			'group_id' => 'int not null',
			'name' => 'string not null'
			), 'engine=InnoDB');

		$this->addForeignKey('catalog_item_group', 'catalog_item', 'group_id', 'catalog_item_group', 'id');

		$this->createTable('catalog_item_category', array(
			'id' => 'pk',
			'catalog_category_id' => 'int not null',
			'catalog_item_id' => 'int not null'
			), 'engine=InnoDB');

		$this->addForeignKey('catalog_item_category_category', 'catalog_item_category', 'catalog_category_id', 'catalog_category', 'id');
		$this->addForeignKey('catalog_item_category_item', 'catalog_item_category', 'catalog_item_id', 'catalog_item', 'id');
	}

	public function safeDown()
	{
		return false;
	}
}