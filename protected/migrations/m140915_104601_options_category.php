<?php

class m140915_104601_options_category extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('options', 'category', 'varchar(40) null');
    }

    public function safeDown()
    {
        return false;
    }
}