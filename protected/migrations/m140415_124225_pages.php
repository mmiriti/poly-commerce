<?php

class m140415_124225_pages extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('page', array(
			'id' => 'pk',
			'create_time' => 'TIMESTAMP default CURRENT_TIMESTAMP',
			'slug' => 'varchar(64) null',
			'title' => 'string',
			'document_id' => 'int',
			), 'engine=InnoDB');

		$this->addForeignKey('page_document', 'page', 'document_id', 'document', 'id');
	}

	public function safeDown()
	{
		return false;
	}
}