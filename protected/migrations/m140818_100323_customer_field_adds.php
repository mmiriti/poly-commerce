<?php

class m140818_100323_customer_field_adds extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('customer_data_field', 'position', 'int');
        $this->addColumn('customer_data_field', 'options', 'text null');
    }

    public function safeDown()
    {
        $this->dropColumn('customer_data_field', 'position');
        $this->dropColumn('customer_data_field', 'options');
    }
}