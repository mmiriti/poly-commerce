<?php

class m140415_103952_documents extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable('document', array(
			'id' => 'pk',
			'create_time' => 'TIMESTAMP default CURRENT_TIMESTAMP',
			'keywords' => 'text',
			'description' => 'text',
			'html' => 'text not null'
			), 'engine=InnoDB');

		$this->addColumn('catalog_item', 'document_id', 'int null');

		$this->addForeignKey('catalog_item_document', 'catalog_item', 'document_id', 'document', 'id');
	}

	public function safeDown()
	{
		return false;
	}
}