<?php

class m140704_082556_text_block extends CDbMigration
{
    public function safeUp()
    {
        try {
            $this->alterColumn('structure_block', 'type', "enum('gallery', 'text') null");
        } catch (Exception $e) {
            return false;
        }
    }

    public function safeDown()
    {
        return false;
    }
}