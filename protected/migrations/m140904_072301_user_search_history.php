<?php

class m140904_072301_user_search_history extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('search_query_user', array(
            'id' => 'pk',
            'search_time' => 'timestamp default current_timestamp',
            'user_id' => 'int',
            'query_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_search_query_user_user', 'search_query_user', 'user_id', 'user', 'id');
        $this->addForeignKey('fk_search_query_user_query', 'search_query_user', 'query_id', 'search_query', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}