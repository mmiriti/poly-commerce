<?php

class m140522_073721_search_query_unique extends CDbMigration
{
	public function safeUp()
	{
		try {
			$this->createIndex('uk_search_query_unique_query', 'search_query', 'query', true);
		} catch (Exception $e) {
			return true;	
		}
	}

	public function safeDown()
	{
		return true;
	}
}