<?php

class m140821_112759_user_name extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('user', 'name', 'varchar(128) null');
    }

    public function safeDown()
    {
        return false;
    }
}