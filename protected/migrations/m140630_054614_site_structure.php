<?php

class m140630_054614_site_structure extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('structure_item', array(
            'id' => 'pk',
            'parent_id' => 'int null',
            'position' => 'int',
            'name' => 'string not null',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_structure_item_parent', 'structure_item', 'parent_id', 'structure_item', 'id');

        $this->createTable('structure_block', array(
            'id' => 'pk',
            'item_id' => 'int',
            'type' => "enum('gallery') null",
            'title' => 'string',
            'data' => 'text',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_structure_block_item', 'structure_block', 'item_id', 'structure_item', 'id');

        $this->createTable('structure_block_gallery', array(
            'id' => 'pk',
            'block_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_structure_block_gallery_block', 'structure_block_gallery', 'block_id', 'structure_block', 'id');

        $this->createTable('structure_block_gallery_item', array(
            'id' => 'pk',
            'gallery_block_id' => 'int',
            'position' => 'int',
            'title' => 'string',
            'description' => 'text null',
            'file_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_structure_block_gallery_item_gallery_block', 'structure_block_gallery_item', 'gallery_block_id', 'structure_block_gallery', 'id');
        $this->addForeignKey('fk_structure_block_gallery_item_file', 'structure_block_gallery_item', 'file_id', 'file', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}