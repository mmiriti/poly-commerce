<?php

class m140829_094646_classifier_parameter extends CDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('parameter', 'type', "enum('string', 'number', 'bool', 'classifier')");
        $this->addColumn('parameter', 'classifier_id', 'int null');

        $this->addForeignKey('fk_parameter_classifier', 'parameter', 'classifier_id', 'classifier_group', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}