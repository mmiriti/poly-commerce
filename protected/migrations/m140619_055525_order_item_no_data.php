<?php

class m140619_055525_order_item_no_data extends CDbMigration
{
    public function safeUp()
    {
        $this->dropColumn('order_item', 'data');
    }

    public function safeDown()
    {
        return false;
    }
}