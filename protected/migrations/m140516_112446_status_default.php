<?php

class m140516_112446_status_default extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->addColumn('order_status', 'default', 'tinyint(1)');
		} catch(Exception $e) {
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}