<?php

class m140403_092244_user_base extends CDbMigration
{	
	public function safeUp()
	{
		try 
		{
			$this->createTable('user_group', array(
				'id' => 'pk',
				'name' => 'string',
				'role' => "enum('customer', 'admin', 'manager') default 'customer'",
				), 'engine=InnoDB');

			$this->insert('user_group', array(
				'name' => 'Администраторы',
				'role' => 'admin')
			);

			$this->createTable('user', array(
				'id' => 'pk',
				'user_group_id' => 'int',
				'email' => 'string not null',
				'pwd_hash' => 'varchar(32) not null'
				), 'engine=InnoDB');

			$this->addForeignKey('user_user_group', 'user', 'user_group_id', 'user_group', 'id');

			$this->insert('user', array(
				'user_group_id' => '1',
				'email' => 'admin@domain.com',
				'pwd_hash' => md5('adminpassword')
				)
			);

		} catch(CDbException $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}