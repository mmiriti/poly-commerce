<?php

class m140818_101348_client_field_new_type extends CDbMigration
{
	public function safeUp()
	{
        $this->alterColumn('customer_data_field', 'type', "enum('string', 'number', 'boolean', 'select')");
	}

	public function safeDown()
	{
        $this->alterColumn('customer_data_field', 'type', "enum('string', 'number', 'boolean')");
	}
}