<?php

class m140409_063013_file extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->createTable('file_folder', array(
				'id' => 'pk',
				'folder_id' => 'int null',
				'name' => 'string not null'
				), 'engine=InnoDB');

			$this->addForeignKey('folder_folder', 'file_folder', 'folder_id', 'file_folder', 'id');

			$this->createTable('file', array(
				'id' => 'pk',
				'create_time' => "TIMESTAMP not null default CURRENT_TIMESTAMP",
				'folder_id' => 'int null',
				'mime' => "varchar(64) not null default 'application/octet-stream'",
				'size' => 'int not null',
				'original_name' => 'string not null',
				'url' => 'string',
				'file_path' => 'string',
				), 'engine=InnoDB');

			$this->addForeignKey('file_folder_id', 'file', 'folder_id', 'file_folder', 'id');

			$this->createTable('item_gallery_item', array(
				'id' => 'pk',
				'create_time' => 'TIMESTAMP default CURRENT_TIMESTAMP',
				'position' => 'int',
				'title' => 'string',
				'item_id' => 'int',
				'file_id' => 'int',
				), 'engine=InnoDB');

			$this->addForeignKey('item_gallery_item_item', 'item_gallery_item', 'item_id', 'catalog_item', 'id');
			$this->addForeignKey('item_gallery_item_file', 'item_gallery_item', 'file_id', 'file', 'id');

		} catch (Exception $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
} 