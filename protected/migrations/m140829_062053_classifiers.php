<?php

class m140829_062053_classifiers extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('classifier_group', array(
            'id' => 'pk',
            'name' => 'string',
            'description' => 'text null',
        ), 'engine=InnoDB');

        $this->createTable('classifier', array(
            'id' => 'pk',
            'group_id' => 'int',
            'parent_id' => 'int null',
            'title' => 'string',
            'info' => 'text null',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_classifier_group', 'classifier', 'group_id', 'classifier_group', 'id');
        $this->addForeignKey('fk_classifier_parent', 'classifier', 'parent_id', 'classifier', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}