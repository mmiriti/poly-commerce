<?php

class m140707_133430_structure_layout extends CDbMigration
{
    public function safeUp()
    {
        try {
            $this->addColumn('structure_item', 'layout', 'varchar(64) null');
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
            return false;
        }
    }

    public function safeDown()
    {
        return false;
    }
}