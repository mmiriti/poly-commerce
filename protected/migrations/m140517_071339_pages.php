<?php

class m140517_071339_pages extends CDbMigration
{
	public function safeUp()
	{
		try {
			$this->addColumn('page', 'page_id', 'int null');
			$this->addForeignKey('pk_page_parent_page', 'page', 'page_id', 'page', 'id');
		} catch (Exception $e) {
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}