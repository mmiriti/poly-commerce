<?php

class m140915_105949_catalog_surcharge extends CDbMigration
{
    public function safeUp()
    {
        $this->insert('options', array(
            'option' => 'global_surcharge',
            'option_name' => 'Глобальная наценка',
            'option_value' => 1.5,
            'option_type' => 'float',
            'editable' => 1,
            'category' => 'catalog',
        ));
    }

    public function safeDown()
    {
        return false;
    }
}