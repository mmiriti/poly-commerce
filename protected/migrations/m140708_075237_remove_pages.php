<?php

class m140708_075237_remove_pages extends CDbMigration
{
	public function safeUp()
	{
        $this->dropTable('page');
	}

	public function safeDown()
	{
        return false;
	}
}