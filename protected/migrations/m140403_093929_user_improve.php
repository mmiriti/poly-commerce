<?php

class m140403_093929_user_improve extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->addColumn('user', 'create_time', 'TIMESTAMP not null DEFAULT CURRENT_TIMESTAMP');

			$this->insert('user_group', array(
				'name' => 'Клиенты',
				'role' => 'customer',
				)
			);
		} catch(Exception $e)
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}