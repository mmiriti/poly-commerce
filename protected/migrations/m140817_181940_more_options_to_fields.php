<?php

class m140817_181940_more_options_to_fields extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('customer_data_field', 'hint', 'varchar(256) null');

        $this->addColumn('order_data_field', 'mandatory', 'tinyint(1) default 0');
        $this->addColumn('order_data_field', 'hint', 'varchar(256) null');
    }

    public function safeDown()
    {
        $this->dropColumn('customer_data_field', 'hint');

        $this->dropColumn('order_data_field', 'mandatory');
        $this->dropColumn('order_data_field', 'hint');
    }
}