<?php

class m140904_113458_query_not_unique extends CDbMigration
{
    public function safeUp()
    {
        $this->dropIndex('uk_search_query_unique_query', 'search_query');
    }

    public function safeDown()
    {
        return false;
    }
}