<?php

class m140708_090937_news_item_date extends CDbMigration
{
	public function safeUp()
	{
        $this->alterColumn('news_item', 'publish_date_time', 'date');
	}

	public function safeDown()
	{
        return false;
	}
}