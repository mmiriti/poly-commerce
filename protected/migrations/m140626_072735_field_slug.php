<?php

class m140626_072735_field_slug extends CDbMigration
{
    public function safeUp()
    {
        try {
            $this->addColumn('form_field', 'slug', 'varchar(64) not null');
        } catch (Exception $e) {
            return false;
        }
    }

    public function safeDown()
    {
        return false;
    }
}