<?php

class m140830_085429_classifier_slug extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('classifier_group', 'slug', 'varchar(64) null');
        $this->createIndex('idx_classifier_group_unique_slug', 'classifier_group', 'slug', true);
    }

    public function safeDown()
    {
        return false;
    }
}