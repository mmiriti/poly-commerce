<?php

class m140516_082218_orders extends CDbMigration
{
	public function safeUp()
	{
		try
		{
			$this->createTable('order_status', array(
				'id' => 'pk',
				'slug' => 'varchar(64) not null',
				'name' => 'varchar(64) not null',
				), 'engine=InnoDB');

			$this->createTable('order', array(
				'id' => 'pk',
				'create_time' => 'TIMESTAMP default CURRENT_TIMESTAMP',
				'customer_id' => 'int',
				'status_id' => 'int',
				), 'engine=InnoDB');

			$this->addForeignKey('fk_order_status', 'order', 'status_id', 'order_status', 'id');
			$this->addForeignKey('fk_order_customer', 'order', 'customer_id', 'customer', 'id');

			$this->createTable('order_data_field', array(
				'id' => 'pk',
				'slug' => 'varchar(64) not null',
				'name' => 'varchar(64) not null',
				'type' => "enum('string', 'number', 'boolean') not null default 'string'",
				), 'engine=InnoDB');

			$this->createTable('order_data_value', array(
				'id' => 'pk',				
				'order_id' => 'int',
				'field_id' => 'int',
				'value_string' => 'text null',
				'value_number' => 'float(10,3) null',
				'value_boolean' => 'tinyint(1) null'
				), 'engine=InnoDB');

			$this->addForeignKey('fk_order_data_value_order', 'order_data_value', 'order_id', 'order', 'id');
			$this->addForeignKey('fk_order_data_value_field', 'order_data_value', 'field_id', 'order_data_field', 'id');

			$this->createTable('order_item', array(
				'id' => 'pk',
				'order_id' => 'int',
				'item_id' => 'int',
				'price_id' => 'int',
				'data' => 'text null',
				'quantity' => 'int',
				), 'engine=InnoDB');

			$this->addForeignKey('fk_order_item_order', 'order_item', 'order_id', 'order', 'id');
			$this->addForeignKey('fk_order_item_catalog_item', 'order_item', 'item_id', 'catalog_item', 'id');
			$this->addForeignKey('fk_order_item_price', 'order_item', 'price_id', 'item_price', 'id');
		} catch(Exception $e) 
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}