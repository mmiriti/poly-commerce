<?php

class m140522_070026_search extends CDbMigration
{
	public function safeUp()
	{
		try {
			$this->createTable('search_query', array(
				'id' => 'pk',
				'create_time' => 'TIMESTAMP default CURRENT_TIMESTAMP',
				'index_time' => 'TIMESTAMP null',
				'query' => 'varchar(128) not null',
				'public' => 'tinyint(1) default 0',
				), 'engine=InnoDB');

			$this->createTable('search_query_result', array(
				'id' => 'pk',
				'query_id' => 'int not null',
				'catalog_item_id' => 'int not null',
				'relevance' => 'float(8,2)',
				), 'engine=InnoDB');

			$this->addForeignKey('fk_search_query_result_query', 'search_query_result', 'query_id', 'search_query', 'id');
			$this->addForeignKey('fk_serach_query_result_catalog_item', 'search_query_result', 'catalog_item_id', 'catalog_item', 'id');
		} catch (Exception $e) 
		{
			return false;
		}
	}

	public function safeDown()
	{
		return false;
	}
}