<?php

class m140818_170741_managers_groups extends CDbMigration
{
    public function safeUp()
    {
        $this->insert('user_group', array(
            'name' => 'Менеджеры',
            'role' => 'manager',
        ));
    }

    public function safeDown()
    {
        return false;
    }
}