<?php

class m140915_163538_surcharge_customer extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('customer', 'surcharge', 'float(8,4) null');

        $this->createTable('customer_category_surcharge', array(
            'customer_id' => 'int',
            'category_id' => 'int',
            'surcharge' => 'float(8,4)',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_customer_category_surcharge_customer', 'customer_category_surcharge', 'customer_id', 'customer', 'id');
        $this->addForeignKey('fk_customer_category_surcharge_catalog_category', 'customer_category_surcharge', 'category_id', 'catalog_category', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}