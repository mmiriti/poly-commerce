<?php

class m140707_065206_gallery_block_remove_entity extends CDbMigration
{
    public function safeUp()
    {
        echo "WARNING!! Все элементы галерей будут удалены.\n";

        $this->dropTable('structure_block_gallery_item');
        $this->dropTable('structure_block_gallery');

        $this->createTable('structure_block_gallery_item', array(
            'id' => 'pk',
            'block_id' => 'int',
            'position' => 'int',
            'title' => 'string',
            'description' => 'text null',
            'file_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_structure_gallery_item_block', 'structure_block_gallery_item', 'block_id', 'structure_block', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}