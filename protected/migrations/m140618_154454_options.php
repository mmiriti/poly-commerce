<?php

class m140618_154454_options extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable('item_option', array(
            'id' => 'pk',
            'name' => 'string',
        ), 'engine=InnoDB');

        $this->createTable('group_option', array(
            'id' => 'pk',
            'group_id' => 'int',
            'option_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_group_option_group', 'group_option', 'group_id', 'catalog_item_group', 'id');
        $this->addForeignKey('fk_group_option_option', 'group_option', 'option_id', 'item_option', 'id');

        $this->createTable('item_option_variant', array(
            'id' => 'pk',
            'option_id' => 'int',
            'string' => 'string',
            'value' => 'text',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_item_option_variant_option', 'item_option_variant', 'option_id', 'item_option', 'id');

        $this->createTable('catalog_item_option_variant_available', array(
            'id' => 'pk',
            'catalog_item_id' => 'int',
            'option_variant_id' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_catalog_item_variants', 'catalog_item_option_variant_available', 'catalog_item_id', 'catalog_item', 'id');
        $this->addForeignKey('fk_catalog_item_variant_variant', 'catalog_item_option_variant_available', 'option_variant_id', 'item_option_variant', 'id');

        $this->createTable('order_item_option_variant', array(
            'id' => 'pk',
            'order_item_id' => 'int',
            'option_variant' => 'int',
        ), 'engine=InnoDB');

        $this->addForeignKey('fk_order_item_option_variant_order_item_id', 'order_item_option_variant', 'order_item_id', 'order_item', 'id');
        $this->addForeignKey('fk_order_item_option_variant_variant', 'order_item_option_variant', 'option_variant', 'item_option_variant', 'id');
    }

    public function safeDown()
    {
        return false;
    }
}