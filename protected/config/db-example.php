<?php
return array(
	'connectionString' => 'mysql:host=localhost;dbname=dbname',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'password',
	'charset' => 'utf8',
);