<?php

class CartWidget extends CWidget
{
	public $items = array();
	public $count = 0;
	public $total = 0;

	public function run()
	{		
		if(isset(Yii::app()->session['cart']['items']))
		{
			$this->items = Yii::app()->session['cart']['items'];

			foreach ($this->items as $it) 
			{
				$item = CatalogItem::model()->findByPk($it['item_id']);
				if($item != null)
				{
					$this->total += ($item->calc_price * intval($it['qty']));
					$this->count += intval($it['qty']);
				}
			}
		}

		$this->render('cartView');
	}
}