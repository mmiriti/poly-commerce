<?php

class CurrencyCommand extends CConsoleCommand
{
    public function actionCbr($mult = 1)
    {
        $rubCurrency = Currency::model()->findByAttributes(array('alpha_3' => 'RUR'));

        if ($rubCurrency != null) {
            $xml_exchange_data = file_get_contents("http://www.cbr.ru/scripts/XML_daily.asp");
            $xml = new SimpleXMLElement($xml_exchange_data);

            foreach ($xml->Valute as $val) {
                $currency = Currency::model()->findByAttributes(array('alpha_3' => $val->CharCode));

                if ($currency != null) {
                    $currency->full_name = $val->Name;
                    $currency->save();

                    echo $currency->alpha_3 . " to " . $rubCurrency->alpha_3 . " = " . (floatval($val->Value) * floatval($mult)) . "\n";

                    $exchange = CurrencyExchange::model()->findByAttributes(array('currency_from_id' => $currency->id, 'currency_to_id' => $rubCurrency->id));

                    if ($exchange == null) {
                        $exchange = new CurrencyExchange();
                        $exchange->currency_from_id = $currency->id;
                        $exchange->currency_to_id = $rubCurrency->id;
                    }

                    $exchange->ratio = (floatval($val->Value) * floatval($mult));
                    $exchange->save();
                }
            }
        } else {
            echo "No RUR currency found\n";
        }
    }
} 