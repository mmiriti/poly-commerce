<?php

class EraseCommand extends CConsoleCommand
{
    public function run($args)
    {
        $items = CatalogItem::model()->findAll();

        $cnt = count($items);

        $i = 1;
        foreach ($items as $item) {
            try {
                if ($item->delete()) {
                    echo "Deleted $i / $cnt\n";
                } else {
                    print_r($item->errors);
                    return;
                }
            } catch (Exception $e) {
                echo "Error\n";
            }
            $i++;
        }
    }
} 