<?php

class SyncCommand extends CConsoleCommand
{
    /*
     * Convert seconds to human readable text.
     *
     */
    public function secs_to_h($secs)
    {
        $units = array(
            "week" => 7 * 24 * 3600,
            "day" => 24 * 3600,
            "hour" => 3600,
            "minute" => 60,
            "second" => 1,
        );

        // specifically handle zero
        if ($secs == 0) return "0 seconds";

        $s = "";

        foreach ($units as $name => $divisor) {
            if ($quot = intval($secs / $divisor)) {
                $s .= "$quot $name";
                $s .= (abs($quot) > 1 ? "s" : "") . ", ";
                $secs -= $quot * $divisor;
            }
        }

        return substr($s, 0, -2);
    }

    public function run($args)
    {
        $start_time = time();

        if (file_exists('sync') && is_dir('sync')) {
            echo "Sync data found\n";

            if (file_exists('sync/brands.csv')) {
                $f = fopen('sync/brands.csv', "r");
                while (($csv = fgetcsv($f, 2048, "|")) !== FALSE) {
                    $catID = intval($csv[0]);

                    $category = CatalogCategory::model()->findByPk($catID);

                    if ($category == null) {
                        $category = new CatalogCategory();
                        $category->id = $catID;
                    }

                    $category->name = iconv("WINDOWS-1251", "UTF-8", $csv[1]);
                    $category->slug = strtolower($category->name);

                    if ($category->save()) {
                        echo $csv[1] . " - saved\n";
                    } else {
                        print_r($category->errors);
                    }
                }
            }

            $update = !in_array('no-update', $args);

            if (file_exists('sync/inventory.csv')) {

                $linecount = 0;
                $handle = fopen("sync/inventory.csv", "r");
                while (!feof($handle)) {
                    fgets($handle);
                    $linecount++;
                }

                fclose($handle);

                $f = fopen('sync/inventory.csv', "r");

                $group = CatalogItemGroup::model()->findByAttributes(array('name' => 'Автозапчасти'));

                if ($group != null) {
                    $line = 1;
                    while (($csv = fgetcsv($f, 4096, '|')) !== false) {
                        $percent = floor(($line / $linecount) * 100);

                        if (count($csv) >= 7) {
                            $ID = intval($csv[0]);

                            if ($ID > 0) {

                                $item = CatalogItem::model()->findByPk($ID);

                                if ($item == null) {
                                    $item = new CatalogItem();
                                    $item->id = $ID;
                                    $item->group_id = $group->id;
                                    $item->name = 'NEW';
                                    if ($item->save()) {
                                        echo "[$line/$linecount, $percent%] New item: " . $ID . "\n";
                                    } else {
                                        echo "[$line/$linecount, $percent%] ERROR\n";
                                        print_r($item->errors);
                                    }
                                } else {
                                    if (!$update) {
                                        echo "[$line/$linecount, $percent%] Skipping " . $ID . "\n";
                                        $line++;
                                        continue;
                                    }
                                    echo "[$line/$linecount, $percent%] Updating item: " . $ID . "\n";
                                }

                                $brandID = intval($csv[2]);

                                $item->setAttribute('supplier_code', $csv[1]);
                                $item->setAttribute('oem_code', $csv[3]);
                                $item->name = iconv("WINDOWS-1251", "UTF-8", $csv[4]);
                                if ($item->name == '') {
                                    $item->name = '- не указано -';
                                }

                                if (trim($csv[5]) != '') {
                                    if ($item->document == null) {
                                        $document = new Document();
                                    } else {
                                        $document = $item->document;
                                    }

                                    $document->html = $csv[5];

                                    $document->save();

                                    $item->document_id = $document->id;
                                }

                                $item->price = $csv[6];
                                if ($item->save()) {
                                    $attrs = array(
                                        'catalog_category_id' => $brandID,
                                        'catalog_item_id' => $item->id,
                                    );
                                    $categoryLink = CatalogItemCategory::model()->findByAttributes($attrs);

                                    if ($categoryLink == null) {
                                        $categoryLink = new CatalogItemCategory();
                                        $categoryLink->attributes = $attrs;
                                        try {
                                            $categoryLink->save();
                                        } catch (Exception $e) {
                                            echo "[$line/$linecount, $percent] Error: " . $e->getMessage() . "\n";
                                        }
                                    }
                                } else {
                                    echo "[$line/$linecount, $percent] Can't save item:\n";
                                    print_r($item->errors);
                                }
                            } else {
                                echo "[$line/$linecount, $percent%] Error: Zero ID: " . implode('|', $csv) . "\n";
                            }
                        } else {
                            echo "[$line/$linecount, $percent%] Error: to short line: " . implode("|", $csv) . "\n";
                        }

                        $line++;
                    }
                } else {
                    echo "No group\n";
                }

                fclose($f);
            }

            if (file_exists('sync/assets.csv')) {
                $f = fopen('sync/assets.csv', "r");

                /*
                 * 4. Количество на складе
                 * 5. Количество в пути
                 * 6. Количество на сторонних складах
                 * 7. Количество в бекордере
                 * 8. Дата заказа (для бекордера)
                 * 9. Дата поставки (для товара в пути)
                 *
                 * Количество на складе count_warehouse (Число)
                 * Количество в пути count_on_road (Число)
                 * Количество на сторонних складах count_other (Число)
                 * Дата заказа order_date (Строка)
                 * Дата поставки shipping_date (Строка)
                 */
                while (($csv = fgetcsv($f, 2048, '|'))) {
                    $item = CatalogItem::model()->findByPk(intval($csv[0]));
                    /* @var $item CatalogItem */
                    if ($item != null) {
                        $item->parameterSetValue('count_warehouse', intval($csv[3]));
                        $item->parameterSetValue('count_on_road', intval($csv[4]));
                        $item->parameterSetValue('count_other', intval($csv[5]));
                        $item->parameterSetValue('count_backorder', intval($csv[6]));
                        $item->parameterSetValue('order_date', intval($csv[7]));
                        $item->parameterSetValue('shipping_date', intval($csv[8]));

                        echo "Update " . $item->id . "\n";
                    }
                }
            }
        } else {
            echo "No sync data found.";
        }

        echo "DONE " . $this->secs_to_h(time() - $start_time);
    }
} 