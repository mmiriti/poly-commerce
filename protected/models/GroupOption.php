<?php

/**
 * This is the model class for table "group_option".
 *
 * The followings are the available columns in table 'group_option':
 * @property integer $id
 * @property integer $group_id
 * @property integer $option_id
 *
 * The followings are the available model relations:
 * @property ItemOption $option
 * @property CatalogItemGroup $group
 */
class GroupOption extends CActiveRecord
{
	public function tableName()
	{
		return 'group_option';
	}

	public function rules()
	{
		return array(
			array('group_id, option_id', 'numerical', 'integerOnly'=>true),
			array('id, group_id, option_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'option' => array(self::BELONGS_TO, 'ItemOption', 'option_id'),
			'group' => array(self::BELONGS_TO, 'CatalogItemGroup', 'group_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'option_id' => 'Option',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('option_id',$this->option_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GroupOption the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
