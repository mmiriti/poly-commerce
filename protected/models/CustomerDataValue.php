<?php

/**
 * Class CustomerDataValue
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $field_id
 * @property mixed $value
 */
class CustomerDataValue extends CActiveRecord
{
    public function tableName()
    {
        return 'customer_data_value';
    }

    public function setValue($value)
    {
        switch ($this->field->type) {

            case 'number':
                $this->value_number = $value;
                break;

            case 'boolean':
                $this->value_boolean = $value;
                break;
            case 'string':
            case 'select':
            default:
                $this->value_string = $value;
                break;
        }
    }

    public function getValue()
    {
        switch ($this->field->type) {

            case 'number':
                return number_format($this->value_number);
                break;

            case 'boolean':
                return $this->value_boolean;
                break;

            case 'string':
            case 'select':
            default:
                return $this->value_string;
                break;
        }
    }

    public function rules()
    {
        return array(
            array('customer_id, field_id, value_boolean', 'numerical', 'integerOnly' => true),
            array('value_number', 'numerical'),
            array('value_string', 'safe'),
            array('id, customer_id, field_id, value_string, value_number, value_boolean', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'field' => array(self::BELONGS_TO, 'CustomerDataField', 'field_id'),
            'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'customer_id' => 'Customer',
            'field_id' => 'Field',
            'value_string' => 'Value String',
            'value_number' => 'Value Number',
            'value_boolean' => 'Value Boolean',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('customer_id', $this->customer_id);
        $criteria->compare('field_id', $this->field_id);
        $criteria->compare('value_string', $this->value_string, true);
        $criteria->compare('value_number', $this->value_number);
        $criteria->compare('value_boolean', $this->value_boolean);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
