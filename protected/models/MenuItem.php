<?php

/**
 * This is the model class for table "menu_item".
 *
 * The followings are the available columns in table 'menu_item':
 * @property integer $id
 * @property integer $menu_id
 * @property integer $position
 * @property string $title
 * @property string $action
 * @property string $get_data
 * @property string $href
 * @property string $url
 * @property mixed $route
 *
 * The followings are the available model relations:
 * @property Menu $menu
 */
class MenuItem extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'menu_item';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title', 'required'),
            array('menu_id, position', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 128),
            array('action', 'length', 'max' => 64),
            array('href', 'length', 'max' => 255),
            array('get_data', 'safe'),
            array('id, menu_id, position, title, action, get_data, href', 'safe', 'on' => 'search'),
        );
    }

    public function getGetArray()
    {
        if ($this->get_data != null) {
            $data = array();

            $lines = explode("\n", $this->get_data);

            foreach ($lines as $var) {
                $var_data = explode('=', $var);
                $data[$var_data[0]] = $var_data[1];
            }

            return $data;
        } else {
            return array();
        }
    }

    public function getRoute()
    {
        if ($this->href != null) {
            return $this->href;
        } else {
            return array($this->action) + $this->getGetArray();
        }
    }

    public function getUrl()
    {
        if ($this->href != null) {
            return $this->href;
        } else {
            $app = Yii::app();
            /** @var $app CWebApplication */
            return $app->createUrl($this->action, $this->getGetArray());
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'menu' => array(self::BELONGS_TO, 'Menu', 'menu_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'menu_id' => 'Menu',
            'position' => 'Позиция в меню',
            'title' => 'Заголовок',
            'action' => 'Действие (controller/action)',
            'get_data' => 'Параметры GET',
            'href' => 'Ссылка',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('menu_id', $this->menu_id);
        $criteria->compare('position', $this->position);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('get_data', $this->get_data, true);
        $criteria->compare('href', $this->href, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return MenuItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
