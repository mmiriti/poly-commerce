<?php

/**
 * This is the model class for table "catalog_item_category".
 *
 * The followings are the available columns in table 'catalog_item_category':
 * @property integer $id
 * @property integer $catalog_category_id
 * @property integer $catalog_item_id
 *
 * The followings are the available model relations:
 * @property CatalogItem $catalogItem
 * @property CatalogCategory $catalogCategory
 */
class CatalogItemCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catalog_item_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('catalog_category_id, catalog_item_id', 'required'),
			array('catalog_category_id, catalog_item_id', 'numerical', 'integerOnly'=>true),
			array('id, catalog_category_id, catalog_item_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'catalogItem' => array(self::BELONGS_TO, 'CatalogItem', 'catalog_item_id'),
			'catalogCategory' => array(self::BELONGS_TO, 'CatalogCategory', 'catalog_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'catalog_category_id' => 'Catalog Category',
			'catalog_item_id' => 'Catalog Item',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('catalog_category_id',$this->catalog_category_id);
		$criteria->compare('catalog_item_id',$this->catalog_item_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatalogItemCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
