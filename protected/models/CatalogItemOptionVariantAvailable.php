<?php

/**
 * This is the model class for table "catalog_item_option_variant_available".
 *
 * The followings are the available columns in table 'catalog_item_option_variant_available':
 * @property integer $id
 * @property integer $catalog_item_id
 * @property integer $option_variant_id
 *
 * The followings are the available model relations:
 * @property ItemOptionVariant $optionVariant
 * @property CatalogItem $catalogItem
 */
class CatalogItemOptionVariantAvailable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catalog_item_option_variant_available';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('catalog_item_id, option_variant_id', 'numerical', 'integerOnly'=>true),
			array('id, catalog_item_id, option_variant_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'optionVariant' => array(self::BELONGS_TO, 'ItemOptionVariant', 'option_variant_id'),
			'catalogItem' => array(self::BELONGS_TO, 'CatalogItem', 'catalog_item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'catalog_item_id' => 'Catalog Item',
			'option_variant_id' => 'Option Variant',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('catalog_item_id',$this->catalog_item_id);
		$criteria->compare('option_variant_id',$this->option_variant_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatalogItemOptionVariantAvailable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
