<?php

/**
 * This is the model class for table "customer_data_field".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $type
 * @property integer $mandatory
 * @property integer $fillable
 * @property integer $position
 * @property string $options
 * @property array $optionsList
 * @property string $inputType
 *
 * @property CustomerDataValue[] $customerDataValues
 */
class CustomerDataField extends CActiveRecord
{
    /**
     * @return string
     */
    public function getInputType()
    {
        switch ($this->type) {
            case 'select':
            case 'boolean' :
                return 'dropdown';
            default:
                return 'text';
        }
    }

    /**
     * @return array
     */
    public function getOptionsList()
    {
        if ($this->type == 'boolean') {
            return array(
                0 => 'нет',
                1 => 'да',
            );
        } else {
            $listData = array();
            $lines = explode("\n", $this->options);

            foreach ($lines as $line) {
                $listData[trim($line)] = trim($line);
            }

            return $listData;
        }
    }

    /**
     * Удалить поле
     *
     * @return bool
     */
    public function delete()
    {
        CustomerDataValue::model()->deleteAllByAttributes(array('field_id' => $this->id));
        return parent::delete();
    }

    public function tableName()
    {
        return 'customer_data_field';
    }

    public function rules()
    {
        return array(
            array('slug, name, mandatory, fillable, type', 'required'),
            array('slug', 'unique'),
            array('hint', 'length', 'max' => 64),
            array('position', 'numerical', 'integerOnly' => true),
            array('mandatory, fillable', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 1),
            array('slug, name', 'length', 'max' => 64),
            array('type, options', 'safe'),
            array('id, slug, name, type, mandatory, hint, fillable', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'customerDataValues' => array(self::HAS_MANY, 'CustomerDataValue', 'field_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'slug' => 'Техническое имя',
            'name' => 'Название',
            'hint' => 'Подсказка для пользователя',
            'mandatory' => 'Обязательно для заполнения',
            'type' => 'Тип',
            'position' => 'Позиция в списке',
            'options' => 'Варианты выбора',
            'fillable' => 'Заполняется пользователем',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
