<?php

/**
 * This is the model class for table "form".
 *
 * The followings are the available columns in table 'form':
 * @property integer $id
 * @property string $name
 * @property string $slug
 *
 * The followings are the available model relations:
 * @property FormField[] $formFields
 * @property FormFill[] $formFills
 * @property FormFillValue[] $formFillValues
 */
class Form extends CActiveRecord
{
	public function tableName()
	{
		return 'form';
	}

	public function rules()
	{
		return array(
			array('name, slug', 'required'),
			array('name', 'length', 'max'=>64),
			array('slug', 'length', 'max'=>32),
            array('message', 'safe'),
			array('id, name, slug', 'safe', 'on'=>'search'),
		);
	}


	public function relations()
	{
		return array(
			'formFields' => array(self::HAS_MANY, 'FormField', 'form_id'),
			'formFills' => array(self::HAS_MANY, 'FormFill', 'form_id', 'order' => 'fill_time desc'),
            'formFillsNew' => array(self::HAS_MANY, 'FormFill', 'form_id', 'condition' => 'is_new=1', 'order' => 'fill_time desc'),
			'formFillValues' => array(self::HAS_MANY, 'FormFillValue', 'form_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
			'slug' => 'Техническое имя',
            'message' => 'Сообщение после заполнения',
		);
	}

	public function search()
	{
        $criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slug',$this->slug,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
