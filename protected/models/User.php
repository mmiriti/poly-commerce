<?php

/**
 * Class User
 *
 * @property integer $id
 * @property integer $user_group_id
 * @property string $email
 * @property string $password
 * @property string $password_again
 * @property string $name
 * @property string $fullName
 *
 * @property UserGroup $userGroup
 * @property Customer $customer
 * @property Customer[] $customers
 * @property User[] $managers
 *
 */
class User extends CActiveRecord
{
    public $password;
    public $password_again;

    /**
     * Full name
     *
     * @return string
     */
    public function getFullName()
    {
        if ($this->customer != null) {
            $full_name = $this->customer->getField('full_name');
            if ($full_name == null) {
                $first_name = $this->customer->getField('first_name');
                $last_name = $this->customer->getField('last_name');
                $middle_name = $this->customer->getField('middle_name');
                $patronymic = $this->customer->getField('patronymic');

                $full_name = ($first_name == null ? '' : $first_name) . ($middle_name == null ? '' : ' ' . $middle_name) . ($patronymic == null ? '' : ' ' . $patronymic) . ($last_name == null ? '' : ' ' . $last_name);

                if ($full_name != '') {
                    return $full_name;
                } else {
                    if ($this->name != '') {
                        return $this->name;
                    } else {
                        return $this->email;
                    }
                }
            } else {
                return $full_name;
            }
        } else {
            if ($this->name != '') {
                return $this->name;
            } else {
                return $this->email;
            }
        }
    }

    public function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return array(
            array('email, password', 'required', 'on' => 'login'),
            array('email, password, password_again', 'required', 'on' => 'reg'),
            array('password', 'length', 'min' => 4),
            array('email', 'unique', 'on' => 'reg'),
            array('password_again', 'compare', 'compareAttribute' => 'password', 'on' => 'reg'),
            array('email', 'email'),
            array('user_group_id', 'numerical', 'integerOnly' => true),
            array('email', 'length', 'max' => 255),
            array('name', 'length', 'max' => 128),
            array('pwd_hash', 'length', 'max' => 32),
            array('id, user_group_id, email, pwd_hash, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Получить текущего пользователя или null если пользователь не залогинился
     *
     * @return User
     */
    public static function current()
    {
        if (Yii::app()->user->isGuest) {
            return null;
        } else {
            $user = User::model()->findByPk(Yii::app()->user->id);
            return $user;
        }
    }

    /**
     * Залогинить пользователя
     *
     * @return bool
     */
    public function login()
    {
        $userId = new UserIdentity($this->email, $this->password);
        if ($userId->authenticate()) {
            Yii::app()->user->login($userId);
            return true;
        } else {
            if ($userId->errorCode == CUserIdentity::ERROR_PASSWORD_INVALID) {
                $this->addError('password', 'Неверный пароль');
            }

            if ($userId->errorCode == CUserIdentity::ERROR_USERNAME_INVALID) {
                $this->addError('email', 'Пользователь не зарегистрирован');
            }

        }
        return false;
    }

    /**
     * Сгенерировать хеш пароля при регистрации и добавлении нового пользователя
     *
     * @return bool
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if (in_array($this->scenario, array('reg', 'insert'))) {
                $this->pwd_hash = md5($this->password);
            }
            return true;
        }

        return false;
    }

    public function relations()
    {
        return array(
            'userGroup' => array(self::BELONGS_TO, 'UserGroup', 'user_group_id'),
            'customer' => array(self::HAS_ONE, 'Customer', 'user_id'),
            'managerCustomers' => array(self::HAS_MANY, 'ManagerCustomer', 'manager_id'),
            'customers' => array(self::HAS_MANY, 'Customer', array('user_id' => 'id'), 'through' => 'managerCustomers'),
            'customerManagers' => array(self::HAS_MANY, 'ManagerCustomer', 'customer_id'),
            'managers' => array(self::HAS_MANY, 'User', array('customer_id' => 'id'), 'through' => 'customerManagers'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_group_id' => 'Группа пользователей',
            'email' => 'Email',
            'pwd_hash' => 'Pwd Hash',
            'password' => 'Пароль',
            'password_again' => 'Пароль еще раз',
            'name' => 'Имя',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_group_id', $this->user_group_id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('pwd_hash', $this->pwd_hash, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
