<?php

/**
 * Class Currency
 *
 * @property $id
 * @property $full_name
 * @property $sign
 * @property $alpha_3
 */
class Currency extends CActiveRecord
{
    /**
     * @param $alpha3
     * @return Currency
     */
    public function findByAlpha3($alpha3)
    {
        return $this->findByAttributes(array('alpha_3' => $alpha3));
    }

    public function tableName()
    {
        return 'currency';
    }

    public function rules()
    {
        return array(
            array('full_name, sign, alpha_3', 'required'),
            array('full_name', 'length', 'max' => 64),
            array('sign', 'length', 'max' => 5),
            array('alpha_3', 'length', 'max' => 3),
            array('alpha_3', 'unique'),
            array('id, full_name, sign, alpha_3', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'itemPrices' => array(self::HAS_MANY, 'ItemPrice', 'currency_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'full_name' => 'Название',
            'sign' => 'Символ',
            'alpha_3' => 'Обозначение ALPHA-3',
        );
    }

    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('full_name', $this->full_name, true);
        $criteria->compare('sign', $this->sign, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
