<?php

class GroupParameter extends CActiveRecord
{
	public function tableName()
	{
		return 'group_parameter';
	}

	public function rules()
	{
		return array(
			array('group_id, parameter_id', 'numerical', 'integerOnly'=>true),
			array('id, group_id, parameter_id', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'parameter' => array(self::BELONGS_TO, 'Parameter', 'parameter_id'),
			'group' => array(self::BELONGS_TO, 'CatalogItemGroup', 'group_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'parameter_id' => 'Parameter',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('parameter_id',$this->parameter_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
