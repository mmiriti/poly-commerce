<?php

/**
 * This is the model class for table "item_option_variant".
 *
 * @property integer $id
 * @property integer $option_id
 * @property string $string
 * @property string $value
 *
 * @property CatalogItemOptionVariantAvailable[] $catalogItemOptionVariantAvailables
 * @property ItemOption $option
 * @property OrderItemOptionVariant[] $orderItemOptionVariants
 */
class ItemOptionVariant extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'item_option_variant';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('string, value', 'required'),
            array('option_id', 'numerical', 'integerOnly' => true),
            array('string', 'length', 'max' => 255),
            array('id, option_id, string, value', 'safe', 'on' => 'search'),
        );
    }

    public function delete()
    {
        foreach ($this->catalogItemOptionVariantAvailables as $av) {
            $av->delete();
        }
        parent::delete();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'catalogItemOptionVariantAvailables' => array(self::HAS_MANY, 'CatalogItemOptionVariantAvailable', 'option_variant_id'),
            'option' => array(self::BELONGS_TO, 'ItemOption', 'option_id'),
            'orderItemOptionVariants' => array(self::HAS_MANY, 'OrderItemOptionVariant', 'option_variant'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'option_id' => 'Option',
            'string' => 'Читаемое значение',
            'value' => 'Техническое значение',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('option_id', $this->option_id);
        $criteria->compare('string', $this->string, true);
        $criteria->compare('value', $this->value, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ItemOptionVariant the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
