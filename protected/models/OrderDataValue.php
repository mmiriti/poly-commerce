<?php

class OrderDataValue extends CActiveRecord
{
	public function setValue($value)
	{
		switch ($this->field->type) 
		{
			case 'string':
			$this->value_string = $value;
			break;

			case 'number':
			$this->value_number = $value;
			break;

			case 'boolean':
			$this->value_boolean = $value;
			break;
		}	
	}

	public function getValue()
	{
		switch ($this->field->type) 
		{
			case 'string':
			return $this->value_string;
			break;

			case 'number':
			return $this->value_number;
			break;

			case 'boolean':
			return $this->value_boolean;
			break;
		}
	}

	public function tableName()
	{
		return 'order_data_value';
	}

	public function rules()
	{
		return array(
			array('order_id, field_id, value_boolean', 'numerical', 'integerOnly'=>true),
			array('value_number', 'numerical'),
			array('value_string', 'safe'),
			array('id, order_id, field_id, value_string, value_number, value_boolean', 'safe', 'on'=>'search'),
			);
	}

	public function relations()
	{
		return array(
			'field' => array(self::BELONGS_TO, 'OrderDataField', 'field_id'),
			'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
			);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_id' => 'Order',
			'field_id' => 'Field',
			'value_string' => 'Value String',
			'value_number' => 'Value Number',
			'value_boolean' => 'Value Boolean',
			);
	}

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('field_id',$this->field_id);
		$criteria->compare('value_string',$this->value_string,true);
		$criteria->compare('value_number',$this->value_number);
		$criteria->compare('value_boolean',$this->value_boolean);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderDataValue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
