<?php

/**
 * This is the model class for table "order_item_option_variant".
 *
 * The followings are the available columns in table 'order_item_option_variant':
 * @property integer $id
 * @property integer $order_item_id
 * @property integer $option_variant
 *
 * The followings are the available model relations:
 * @property ItemOptionVariant $optionVariant
 * @property OrderItem $orderItem
 */
class OrderItemOptionVariant extends CActiveRecord
{
	public function tableName()
	{
		return 'order_item_option_variant';
	}

	public function rules()
	{
		return array(
			array('order_item_id, option_variant', 'numerical', 'integerOnly'=>true),
			array('id, order_item_id, option_variant', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'optionVariant' => array(self::BELONGS_TO, 'ItemOptionVariant', 'option_variant'),
			'orderItem' => array(self::BELONGS_TO, 'OrderItem', 'order_item_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'order_item_id' => 'Order Item',
			'option_variant' => 'Option Variant',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('order_item_id',$this->order_item_id);
		$criteria->compare('option_variant',$this->option_variant);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
