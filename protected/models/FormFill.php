<?php

/**
 * This is the model class for table "form_fill".
 *
 * The followings are the available columns in table 'form_fill':
 * @property integer $id
 * @property string $fill_time
 * @property integer $form_id
 * @property integer $user_id
 * @property integer $is_new
 *
 * The followings are the available model relations:
 * @property Form $form
 * @property FormFillValue[] $formFillValues
 */
class FormFill extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'form_fill';
    }

    public function delete()
    {
        foreach ($this->formFillValues as $value) {
            $value->delete();
        }
        parent::delete();
    }

    public function getFieldValue($field_slug)
    {
        foreach ($this->formFillValues as $value) {
            if ($value->field->slug == $field_slug) {
                return $value->value;
            }
        }
    }

    public function rules()
    {
        return array(
            array('fill_time', 'safe'),
            array('form_id, user_id, is_new', 'numerical', 'integerOnly' => true),
            array('id, fill_time, form_id, user_id, is_new', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'form' => array(self::BELONGS_TO, 'Form', 'form_id'),
            'formFillValues' => array(self::HAS_MANY, 'FormFillValue', 'fill_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'fill_time' => 'Fill Time',
            'form_id' => 'Form',
            'user_id' => 'User',
            'is_new' => 'Is New',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('fill_time', $this->fill_time, true);
        $criteria->compare('form_id', $this->form_id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('is_new', $this->is_new);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FormFill the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
