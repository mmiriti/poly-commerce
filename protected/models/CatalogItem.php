<?php

/**
 * Class CatalogItem
 *
 * Фактически центральная сущность всей системы - эта сущность
 * отвечает за каждый отдельный товар в базе
 *
 * @property integer $id
 * @property integer $document_id
 *
 * @property CatalogItemGroup $group
 * @property ItemGalleryItem[] $galleryItems
 * @property CatalogCategory[] $catalogCategories
 *
 * @property float $surcharge
 * @property string $readable_price
 * @property float $priceValue
 * @property float $priceSurchargedValue
 * @property Currency $currency
 * @property ItemPrice $price
 * @property Document $document
 */
class CatalogItem extends ExtandableModel
{
    private $_surcharge = null;

    /**
     * @param $parameter Parameter
     * @param $value ItemParameterValue
     * @return null
     */
    private static function getParamValues($parameter, $value)
    {
        if ($parameter != null) {
            $value_field = 'value_string';

            if ($parameter->type == 'number') {
                $value_field = 'value_number';
            }

            if ($parameter->type == 'bool') {
                $value_field = 'value_bool';
            }

            if ($parameter->type == 'classifier') {
                $value_field = 'value_number';
            }

            return ItemParameterValue::model()->findAllByAttributes(array('parameter_id' => $parameter->id, $value_field => $value));
        } else {
            return array();
        }
    }

    /**
     * Найти записи по указанному значению параметра
     *
     * @param $slug
     * @param $value
     * @return array
     */
    public static function findByParameter($slug, $value)
    {
        $result = array();

        $parameter = Parameter::model()->findByAttributes(array('slug' => $slug));

        if ($parameter != null) {
            $values = self::getParamValues($parameter, $value);

            foreach ($values as $v) {
                $result[] = $v->catalogItem;
            }
        }

        return $result;
    }

    /**
     * @param $parameters
     * @return array|CActiveRecord|CActiveRecord[]|mixed|null
     */
    public static function findAllByParameters($parameters)
    {
        $hasId = array();
        $num = 0;

        foreach ($parameters as $slug => $value) {
            $parameter = Parameter::model()->findByAttributes(array('slug' => $slug));
            if ($parameter != null) {
                $hasId[$num] = array();

                $values = self::getParamValues($parameter, $value);

                foreach ($values as $v) {
                    $hasId[$num][] = $v->catalogItem->id;
                }

                $num++;
            }
        }

        if (count($hasId) == 1) {
            return $hasId[0];
        } else {
            if (count($hasId) == 2) {
                $foundIDs = array_intersect($hasId[0], $hasId[1]);
            } else {
                $foundIDs = call_user_func("array_intersect", $hasId);
            }
        }

        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $foundIDs);

        return CatalogItem::model()->findAll($criteria);
    }

    /**
     * Получить значение параметра
     * @param $param_id_or_slug
     * @return array|mixed|null
     */
    public function parameterValue($param_id_or_slug)
    {
        if (is_numeric($param_id_or_slug)) {
            $param_value = ItemParameterValue::model()->findByAttributes(array('catalog_item_id' => $this->id, 'parameter_id' => $param_id_or_slug));
        } else {
            $param = Parameter::model()->findByAttributes(array('slug' => $param_id_or_slug));
            if ($param != null) {
                $param_value = ItemParameterValue::model()->findByAttributes(array('catalog_item_id' => $this->id, 'parameter_id' => $param->id));
            } else {
                return null;
            }

        }
        if ($param_value == null) {
            return null;
        } else {
            return $param_value->valueReadable;
        }
    }

    /**
     * Задать значение параметра
     *
     * @param $paramSlug
     * @param $newValue
     */
    public function parameterSetValue($paramSlug, $newValue)
    {
        $parameter = Parameter::model()->findByAttributes(array('slug' => $paramSlug));

        $value = ItemParameterValue::model()->findByAttributes(array('parameter_id' => $parameter->id, 'catalog_item_id' => $this->id));

        if ($value == null) {
            $value = new ItemParameterValue('create');
            $value->parameter_id = $parameter->id;
            $value->catalog_item_id = $this->id;
            $value->save();
        }

        $value->value = $newValue;
        $value->save();
    }

    /**
     * Возвращает цену в удобном для чтения виде
     *
     * @return string
     */
    public function getReadable_price()
    {
        if ($this->price == null) {
            return Options::get('no_price_label', 'цена не указана', 'Если цена не указана');
        } else {
            return number_format($this->priceValue) . ' ' . $this->price->currency->sign;
        }
    }

    public function getSurcharge()
    {
        if ($this->_surcharge == null) {
            $category_surcharge = null;
            $user = User::current();

            // FIXME Тут возникает проблема из-за того, что товар может находится в разных категориях. Это значние не должно быть однозначным для товара...
            foreach ($this->catalogCategories as $category) {
                if (($user != null) && ($user->customer != null)) {
                    $customerCategorySurcharge = CustomerCategorySurcharge::model()->findByAttributes(array('customer_id' => $user->customer->id, 'category_id' => $category->id));
                    if ($customerCategorySurcharge != null) {
                        $category_surcharge = $customerCategorySurcharge->surcharge;
                        break;
                    }
                }
                if ($category->surcharge != null) {
                    $category_surcharge = $category->surcharge;
                    break;
                }
            }

            $user_surcharge = null;

            if (($user != null) && ($user->customer != null)) {
                $user_surcharge = $user->customer->surcharge;
            }

            if ($category_surcharge == null) {
                if ($user_surcharge == null) {
                    $final_surcharge = Options::get('global_surcharge', 1);
                } else {
                    $final_surcharge = $user_surcharge;
                }
            } else {
                $final_surcharge = $category_surcharge;
            }

            $this->_surcharge = floatval($final_surcharge);
        }

        return $this->_surcharge;
    }

    /**
     * Получить числовое значение цены с учетом всех возможных наценок
     * @return float|int
     */
    public function getPriceValue()
    {
        if ($this->price == null) {
            return 0;
        } else {
            return floatval($this->price->value);
        }
    }

    /**
     * Получить числовое значение цены с наценкой
     *
     * @return float
     */
    public function getPriceSurchargedValue()
    {
        return floatval($this->priceValue * $this->surcharge);
    }

    /**
     * Получить объект валюты
     *
     * @return Currency|null
     */
    public function getCurrency()
    {
        if ($this->price != null) {
            return $this->price->currency;
        } else {
            return null;
        }
    }

    /**
     * Получить текущую цену
     *
     * @return null
     */
    public function getPrice()
    {
        if ($this->priceDynamics != null) {
            return $this->priceDynamics[0];
        } else {
            return null;
        }
    }

    /**
     * Задать ткущую цену используя валюту по умолчанию
     *
     * @param $value
     */
    public function setPrice($value)
    {
        if (($this->price == null) || ($this->price->value != $value)) {
            $price = new ItemPrice();
            $price->item_id = $this->id;
            $price->value = $value;

            $currency_alpha3 = Options::get('default_currency', 'EUR', 'Валюта по умолчанию');

            $currency = Currency::model()->findByAttributes(array('alpha_3' => $currency_alpha3));
            if ($currency == null) {
                $currency = Currency::model()->find();
            }

            if ($currency != null) {
                $price->currency_id = $currency->id;
                $price->save();
            }
        }
    }

    /**
     * Получить цену с конвертацией в нужную валюту
     *
     * @param $currency
     * @return float|null
     */
    public function readPrice($currency)
    {
        if ($this->price != null) {
            if ($this->price->currency->alpha_3 == $currency) {
                return $this->price->value * $this->surcharge;
            } else {
                $convertTo = Currency::model()->findByAttributes(array('alpha_3' => $currency));
                if ($convertTo != null) {
                    $exchange = CurrencyExchange::model()->findByAttributes(array('currency_from_id' => $this->price->currency->id, 'currency_to_id' => $convertTo->id));
                    if ($exchange != null) {
                        return (($this->price->value * $this->surcharge) * $exchange->ratio);
                    }
                } else {
                    return $this->price->value * $this->surcharge;
                }
            }
        } else {
            return null;
        }
    }

    /**
     * Удалить
     *
     * TODO Delete to recycle bin?
     *
     * @return bool
     */
    public function delete()
    {
        SearchQueryResult::model()->deleteAllByAttributes(array('catalog_item_id' => $this->id));
        ItemParameterValue::model()->deleteAllByAttributes(array('catalog_item_id' => $this->id));
        ItemGalleryItem::model()->deleteAllByAttributes(array('item_id' => $this->id));
        CatalogItemCategory::model()->deleteAllByAttributes(array('catalog_item_id' => $this->id));
        CatalogItemOptionVariantAvailable::model()->deleteAllByAttributes(array('catalog_item_id' => $this->id));

        $orderItems = OrderItem::model()->findAllByAttributes(array('item_id' => $this->id));

        ItemPrice::model()->deleteAllByAttributes(array('item_id' => $this->id));

        foreach ($orderItems as $orderItem) {
            $orderItem->delete();
        }

        $document = $this->document;

        if (parent::delete()) {
            if ($document != null) {
                $document->delete();
            }
            return true;
        } else {
            return false;
        }
    }

    public function tableName()
    {
        return 'catalog_item';
    }

    public function rules()
    {
        return $this->extendRules(array(
            array('group_id, name', 'required'),
            array('group_id, document_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('update_time', 'safe'),
            array('id, create_time, update_time, group_id, name, document_id', 'safe', 'on' => 'search'),
        ));
    }

    public function isVariantAvailable($variantID)
    {
        foreach ($this->availableOptionVariants as $v) {
            if ($v->option_variant_id == $variantID) {
                return true;
            }
        }

        return false;
    }

    public function relations()
    {
        return array(
            'group' => array(self::BELONGS_TO, 'CatalogItemGroup', 'group_id'),
            'priceDynamics' => array(self::HAS_MANY, 'ItemPrice', 'item_id', 'order' => 'create_time desc'),
            'galleryItems' => array(self::HAS_MANY, 'ItemGalleryItem', 'item_id'),
            'catalogItemCategories' => array(self::HAS_MANY, 'CatalogItemCategory', 'catalog_item_id'),
            'catalogCategories' => array(self::HAS_MANY, 'CatalogCategory', array('catalog_category_id' => 'id'), 'through' => 'catalogItemCategories'),
            'document' => array(self::BELONGS_TO, 'Document', 'document_id'),
            'availableOptionVariants' => array(self::HAS_MANY, 'CatalogItemOptionVariantAvailable', 'catalog_item_id'),
        );
    }

    public function attributeLabels()
    {
        return $this->extendAttributeLabels(array(
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'group_id' => 'Группа товаров',
            'document_id' => 'Документ',
            'name' => 'Наименование',
        ));
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('group_id', $this->group_id);
        $criteria->compare('document_id', $this->document_id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
