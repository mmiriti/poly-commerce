<?php

/**
 * This is the model class for table "customer_category_surcharge".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $category_id
 * @property double $surcharge
 *
 * @property CatalogCategory $category
 * @property Customer $customer
 */
class CustomerCategorySurcharge extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_category_surcharge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('customer_id, category_id', 'numerical', 'integerOnly'=>true),
			array('surcharge', 'numerical'),
			array('customer_id, category_id, surcharge', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'category' => array(self::BELONGS_TO, 'CatalogCategory', 'category_id'),
			'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'customer_id' => 'Customer',
			'category_id' => 'Category',
			'surcharge' => 'Surcharge',
		);
	}

	/**
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('surcharge',$this->surcharge);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomerCategorySurcharge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
