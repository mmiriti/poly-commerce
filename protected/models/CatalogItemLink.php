<?php

/**
 * This is the model class for table "catalog_item_link".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $catalog_item_1_id
 * @property integer $catalog_item_2_id
 *
 * @property CatalogItem $catalogItem2
 * @property CatalogItem $catalogItem1
 */
class CatalogItemLink extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'catalog_item_link';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('position, catalog_item_1_id, catalog_item_2_id', 'numerical', 'integerOnly' => true),
            array('id, position, catalog_item_1_id, catalog_item_2_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'catalogItem2' => array(self::BELONGS_TO, 'CatalogItem', 'catalog_item_2_id'),
            'catalogItem1' => array(self::BELONGS_TO, 'CatalogItem', 'catalog_item_1_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'position' => 'Position',
            'catalog_item_1_id' => 'Catalog Item 1',
            'catalog_item_2_id' => 'Catalog Item 2',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('position', $this->position);
        $criteria->compare('catalog_item_1_id', $this->catalog_item_1_id);
        $criteria->compare('catalog_item_2_id', $this->catalog_item_2_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
