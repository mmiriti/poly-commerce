<?php

/**
 * This is the model class for table "letter_template".
 *
 * @property integer $id
 * @property string $create_time
 * @property string $slug
 * @property string $name
 * @property string $subject
 * @property string $content
 */
class LetterTemplate extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'letter_template';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('slug, name, subject, content', 'required'),
            array('slug', 'length', 'max' => 64),
            array('name, subject', 'length', 'max' => 255),
            array('id, create_time, slug, name, subject, content', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'create_time' => 'Create Time',
            'slug' => 'Ярлык (техническое имя)',
            'name' => 'Название',
            'subject' => 'Тема письма',
            'content' => 'Шаблон',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('content', $this->content, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
