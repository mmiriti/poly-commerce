<?php

/**
 * Class Customer
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $create_time
 * @property float $surcharge
 *
 * @property Order[] $orders
 * @property float $ordersTotal
 * @property User[] $managers
 */
class Customer extends CActiveRecord
{
    /**
     * Количество клиентов без менеджеров
     *
     * @todo Оптимизировать
     *
     * @return int
     */
    public static function unattachedCount()
    {
        $all = Customer::model()->findAll();
        $result = 0;

        foreach ($all as $customer) {
            if (count($customer->managers) == 0) {
                $result++;
            }
        }

        return $result;
    }

    /**
     * Задать значение полю
     *
     * @param $slug
     * @param $new_value
     */
    public function setField($slug, $new_value)
    {
        $field = CustomerDataField::model()->findByAttributes(array('slug' => $slug));

        if ($field !== null) {
            $value = CustomerDataValue::model()->findByAttributes(array('customer_id' => $this->id, 'field_id' => $field->id));
            if ($value !== null) {
                $value->value = $new_value;
                $value->save();
            } else {
                $value = new CustomerDataValue;
                $value->field_id = $field->id;
                $value->customer_id = $this->id;
                $value->value = $new_value;
                $value->save();
            }
        }
    }

    /**
     * Получить значение поля
     *
     * @param $slug
     * @return array|mixed|null
     */
    public function getField($slug)
    {
        $field = CustomerDataField::model()->findByAttributes(array('slug' => $slug));
        if ($field !== null) {
            $value = CustomerDataValue::model()->findByAttributes(array('customer_id' => $this->id, 'field_id' => $field->id));
            if ($value !== null) {
                return $value->value;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Сумма всех заказов
     *
     * @return int
     */
    public function getOrdersTotal()
    {
        $total = 0;

        foreach ($this->orders as $order) {
            $total += $order->total;
        }

        return $total;
    }

    public function tableName()
    {
        return 'customer';
    }

    public function rules()
    {
        return array(
            array('user_id', 'numerical', 'integerOnly' => true),
            array('surcharge', 'numerical'),
            array('id, user_id, create_time', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'customerDataValues' => array(self::HAS_MANY, 'CustomerDataValue', 'customer_id'),
            'orders' => array(self::HAS_MANY, 'Order', 'customer_id'),
            'managerCustomers' => array(self::HAS_MANY, 'ManagerCustomer', 'customer_id'),
            'managers' => array(self::HAS_MANY, 'User', array('manager_id' => 'id'), 'through' => 'managerCustomers'),
            'categorySurcharge' => array(self::BELONGS_TO, 'CustomerCategorySurcharge', 'customer_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'create_time' => 'Create Time',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('create_time', $this->create_time, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
