<?php

/**
 * This is the model class for table "form_field".
 *
 * The followings are the available columns in table 'form_field':
 * @property integer $id
 * @property integer $form_id
 * @property string $name
 * @property integer $required
 *
 * The followings are the available model relations:
 * @property Form $form
 * @property FormFillValue[] $formFillValues
 */
class FormField extends CActiveRecord
{
    public function tableName()
    {
        return 'form_field';
    }

    public function rules()
    {
        return array(
            array('name, slug, required', 'required'),
            array('form_id, required', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 64),
            array('id, form_id, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'form' => array(self::BELONGS_TO, 'Form', 'form_id'),
            'formFillValues' => array(self::HAS_MANY, 'FormFillValue', 'field_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'form_id' => 'Форма',
            'name' => 'Название',
            'required' => 'Обязательный параметр',
            'slug' => 'Техническое имя',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('form_id', $this->form_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('required', $this->required);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FormField the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
