<?php

/**
 * This is the model class for table "order_data_field".
 *
 * The followings are the available columns in table 'order_data_field':
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $type
 *
 * The followings are the available model relations:
 * @property OrderDataValue[] $orderDataValues
 */
class OrderDataField extends CActiveRecord
{
	public function tableName()
	{
		return 'order_data_field';
	}

	public function rules()
	{
		return array(
			array('slug, name, type, mandatory', 'required'),
			array('slug', 'unique'),
            array('hint', 'length', 'max' => 64),
            array('mandatory', 'numerical', 'integerOnly' => true, 'min' => 0, 'max' => 1),
			array('slug, name', 'length', 'max'=>64),
			array('type', 'length', 'max'=>7),
			array('id, slug, name, type', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'orderDataValues' => array(self::HAS_MANY, 'OrderDataValue', 'field_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название',
            'hint' => 'Подсказка для пользователя',
            'slug' => 'Техническое имя',
            'type' => 'Тип',
            'mandatory' => 'Обязательное для заполнения',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
