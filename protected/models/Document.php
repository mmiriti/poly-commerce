<?php

/**
 * Class Document
 *
 * @property string $keywords
 * @property string $description
 * @property string $html
 */
class Document extends CActiveRecord
{
    public function tableName()
    {
        return 'document';
    }

    public function rules()
    {
        return array(
            array('html', 'required'),
            array('keywords, description', 'safe'),
            array('id, create_time, keywords, description, html', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'catalogItems' => array(self::HAS_MANY, 'CatalogItem', 'document_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'create_time' => 'Create Time',
            'keywords' => 'Ключевые слова (Keywords)',
            'description' => 'Описание (Description)',
            'html' => 'Текст',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('keywords', $this->keywords, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('html', $this->html, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
