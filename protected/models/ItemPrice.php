<?php

/**
 * Class ItemPrice
 *
 * @property int $id
 * @property int $item_id
 * @property int $currency_id
 * @property string $create_time
 * @property float $value
 * @property string $comment
 *
 * @property CatalogItem $item
 * @property Currency $currency
 */
class ItemPrice extends CActiveRecord
{
    public function tableName()
    {
        return 'item_price';
    }

    public function rules()
    {
        return array(
            array('currency_id, item_id, value', 'required'),
            array('item_id, currency_id', 'numerical', 'integerOnly' => true),
            array('value', 'numerical'),
            array('comment', 'length', 'max' => 255),
            array('id, item_id, currency_id, create_time, value, comment', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'CatalogItem', 'item_id'),
            'currency' => array(self::BELONGS_TO, 'Currency', 'currency_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'item_id' => 'Товар',
            'currency_id' => 'Валюта',
            'create_time' => 'Дата создания',
            'value' => 'Значение цены',
            'comment' => 'Комментарий к цене',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('currency_id', $this->currency_id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('value', $this->value);
        $criteria->compare('comment', $this->comment, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
