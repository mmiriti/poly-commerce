<?php

/**
 * Class OrderStatus
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property integer $default
 */
class OrderStatus extends CActiveRecord
{
    public function tableName()
    {
        return 'order_status';
    }

    public function rules()
    {
        return array(
            array('slug, name', 'required'),
            array('slug', 'unique'),
            array('default', 'safe'),
            array('slug, name', 'length', 'max' => 64),
            array('id, slug, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'orders' => array(self::HAS_MANY, 'Order', 'status_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'default' => 'По умолчанию',
            'slug' => 'Техническое имя',
            'name' => 'Статус',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('default', $this->default, true);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
