<?php

/**
 * This is the model class for table "currency_exchange".
 *
 * @property integer $id
 * @property integer $currency_from_id
 * @property integer $currency_to_id
 * @property double $ratio
 *
 * @property Currency $currencyTo
 * @property Currency $currencyFrom
 */
class CurrencyExchange extends CActiveRecord
{
    public static function getRatio($from, $to)
    {
        $currency_from = Currency::model()->findByAttributes(array('alpha_3' => $from));

        if ($currency_from != null) {
            $currency_to = Currency::model()->findByAttributes(array('alpha_3' => $to));
            if ($currency_to != null) {
                $exchange = CurrencyExchange::model()->findByAttributes(array('currency_from_id' => $currency_from->id, 'currency_to_id' => $currency_to->id));
                if ($exchange != null) {
                    return $exchange->ratio;
                }
            }
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'currency_exchange';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('currency_from_id, currency_to_id, ratio', 'required'),
            array('currency_from_id, currency_to_id', 'numerical', 'integerOnly' => true),
            array('ratio', 'numerical'),
            array('id, currency_from_id, currency_to_id, ratio', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'currencyTo' => array(self::BELONGS_TO, 'Currency', 'currency_to_id'),
            'currencyFrom' => array(self::BELONGS_TO, 'Currency', 'currency_from_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'currency_from_id' => 'Валюта из',
            'currency_to_id' => 'Валюта в',
            'ratio' => 'Соотношение',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('currency_from_id', $this->currency_from_id);
        $criteria->compare('currency_to_id', $this->currency_to_id);
        $criteria->compare('ratio', $this->ratio);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
