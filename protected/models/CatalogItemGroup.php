<?php

/**
 * Class CatalogItemGroup
 *
 * @property Parameter[] $parameters
 */
class CatalogItemGroup extends CActiveRecord
{
    public function tableName()
    {
        return 'catalog_item_group';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'min' => 2, 'max' => 64),
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'items' => array(self::HAS_MANY, 'CatalogItem', 'group_id'),
            'groupParameters' => array(self::HAS_MANY, 'GroupParameter', 'group_id'),
            'parameters' => array(self::HAS_MANY, 'Parameter', array('parameter_id' => 'id'), 'through' => 'groupParameters'),
            'groupOptions' => array(self::HAS_MANY, 'GroupOption', 'group_id'),
            'options' => array(self::HAS_MANY, 'ItemOption', array('option_id' => 'id'), 'through' => 'groupOptions'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
