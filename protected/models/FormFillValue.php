<?php

/**
 * This is the model class for table "form_fill_value".
 *
 * The followings are the available columns in table 'form_fill_value':
 * @property integer $id
 * @property integer $fill_id
 * @property integer $form_id
 * @property integer $field_id
 * @property string $value
 *
 * The followings are the available model relations:
 * @property FormField $field
 * @property FormFill $fill
 * @property Form $form
 */
class FormFillValue extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'form_fill_value';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('fill_id, form_id, field_id', 'numerical', 'integerOnly'=>true),
			array('value', 'safe'),
			array('id, fill_id, form_id, field_id, value', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'field' => array(self::BELONGS_TO, 'FormField', 'field_id'),
			'fill' => array(self::BELONGS_TO, 'FormFill', 'fill_id'),
			'form' => array(self::BELONGS_TO, 'Form', 'form_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fill_id' => 'Fill',
			'form_id' => 'Form',
			'field_id' => 'Field',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fill_id',$this->fill_id);
		$criteria->compare('form_id',$this->form_id);
		$criteria->compare('field_id',$this->field_id);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormFillValue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
