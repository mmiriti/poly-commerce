<?php

/**
 * Class Order
 *
 * @property string $create_time
 * @property OrderStatus $status
 * @property OrderItem[] $orderItems
 */
class Order extends CActiveRecord
{
    public function setField($slug, $new_value)
    {
        $field = OrderDataField::model()->findByAttributes(array('slug' => $slug));

        if ($field !== null) {
            $value = OrderDataValue::model()->findByAttributes(array('field_id' => $field->id, 'order_id' => $this->id));

            if ($value !== null) {
                $value->value = $new_value;
                $value->save();
            } else {
                $value = new OrderDataValue;
                $value->order_id = $this->id;
                $value->field_id = $field->id;
                $value->value = $new_value;
                $value->save();
            }
        }
    }

    public function getField($slug)
    {
        $field = OrderDataField::model()->findByAttributes(array('slug' => $slug));
        if ($field !== null) {
            $value = OrderDataValue::model()->findByAttributes(array('order_id' => $this->id, 'field_id' => $field->id));
            if ($value !== null) {
                return $value->value;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getTotal()
    {
        $total = 0;

        foreach ($this->orderItems as $item) {
            $total += $item->price->value;
        }

        return $total;
    }

    public function calcTotal($currency)
    {
        $total = 0;
        foreach ($this->orderItems as $item) {
            $total += $item->item->readPrice($currency) * $item->quantity;
        }
        return $total;
    }

    public function tableName()
    {
        return 'order';
    }

    public function rules()
    {
        return array(
            array('customer_id, status_id', 'numerical', 'integerOnly' => true),
            array('id, create_time, customer_id, status_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
            'status' => array(self::BELONGS_TO, 'OrderStatus', 'status_id'),
            'orderDataValues' => array(self::HAS_MANY, 'OrderDataValue', 'order_id'),
            'orderItems' => array(self::HAS_MANY, 'OrderItem', 'order_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'create_time' => 'Create Time',
            'customer_id' => 'Customer',
            'status_id' => 'Status',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('customer_id', $this->customer_id);
        $criteria->compare('status_id', $this->status_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
