<?php

/**
 * This is the model class for table "structure_item".
 *
 * The followings are the available columns in table 'structure_item':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $position
 * @property string $name
 *
 * The followings are the available model relations:
 * @property StructureBlock[] $structureBlocks
 * @property StructureItem $parent
 * @property StructureItem[] $structureItems
 */
class StructureItem extends CActiveRecord
{
    public function delete()
    {
        foreach ($this->structureBlocks as $block) {
            $block->delete();
        }

        foreach ($this->structureItems as $child) {
            $child->delete();
        }
        return parent::delete();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'structure_item';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('slug', 'unique'),
            array('parent_id, position', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('layout', 'length', 'max' => 64),
            array('id, parent_id, position, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'structureBlocks' => array(self::HAS_MANY, 'StructureBlock', 'item_id'),
            'parent' => array(self::BELONGS_TO, 'StructureItem', 'parent_id'),
            'structureItems' => array(self::HAS_MANY, 'StructureItem', 'parent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'parent_id' => 'Parent',
            'position' => 'Позиция сортировки',
            'name' => 'Название',
            'slug' => 'Техническое имя',
            'layout' => 'Шаблон',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('position', $this->position);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return StructureItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
