<?php

/**
 * @property integer $id
 * @property integer $group_id
 * @property integer $parent_id
 * @property string $title
 * @property string $info
 * @property string $data
 *
 * @property Classifier $parent
 * @property Classifier[] $classifiers
 * @property ClassifierGroup $group
 */
class Classifier extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'classifier';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title', 'required'),
            array('group_id, parent_id', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('info, data', 'safe'),
            array('id, group_id, parent_id, title, info, data', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'Classifier', 'parent_id'),
            'classifiers' => array(self::HAS_MANY, 'Classifier', 'parent_id'),
            'group' => array(self::BELONGS_TO, 'ClassifierGroup', 'group_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'group_id' => 'Group',
            'parent_id' => 'Родительское значение',
            'title' => 'Значение',
            'info' => 'Дополнительная информация',
            'data' => 'Данные',
        );
    }

    /**
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('group_id', $this->group_id);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('info', $this->info, true);
        $criteria->compare('data', $this->data);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
