<?php

/**
 * Class ItemParameterValue
 *
 * @property integer $id
 * @property integer $catalog_item_id
 * @property integer $parameter_id
 * @property string $value_string
 * @property float $value_number
 * @property integer $value_bool
 *
 * @property mixed $value
 * @property string $valueReadable
 */
class ItemParameterValue extends CActiveRecord
{
    public function tableName()
    {
        return 'item_parameter_value';
    }

    public function rules()
    {
        return array(
            array('catalog_item_id, parameter_id, value_bool', 'numerical', 'integerOnly' => true),
            array('value_number', 'numerical'),
            array('value_string', 'length', 'max' => 255),
            array('id, catalog_item_id, parameter_id, value_string, value_number, value_bool', 'safe', 'on' => 'search'),
        );
    }

    public function getValue()
    {
        switch ($this->parameter->type) {
            case 'string':
                return $this->value_string;
            case 'number':
                return $this->value_number;
            case 'bool':
                return $this->value_bool;
            case'classifier':
                return intval($this->value_number);
                break;
        }
    }

    public function setValue($value)
    {
        switch ($this->parameter->type) {
            case 'string':
                $this->value_string = $value;
                break;
            case 'number':
                $this->value_number = $value;
                break;
            case 'bool':
                $this->value_bool = $value;
                break;
            case 'classifier':
                $this->value_number = $value;
                break;
        }
    }

    public function getValueReadable()
    {
        switch ($this->parameter->type) {
            case 'classifier':
                $classifierValue = Classifier::model()->findByPk($this->value);
                if ($classifierValue != null) {
                    return $classifierValue->title;
                }
                break;
            default:
                return $this->value;
        }
    }

    public function relations()
    {
        return array(
            'parameter' => array(self::BELONGS_TO, 'Parameter', 'parameter_id'),
            'catalogItem' => array(self::BELONGS_TO, 'CatalogItem', 'catalog_item_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'catalog_item_id' => 'Catalog Item',
            'parameter_id' => 'Parameter',
            'value_string' => 'Value String',
            'value_number' => 'Value Number',
            'value_bool' => 'Value Bool',
        );
    }

    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('catalog_item_id', $this->catalog_item_id);
        $criteria->compare('parameter_id', $this->parameter_id);
        $criteria->compare('value_string', $this->value_string, true);
        $criteria->compare('value_number', $this->value_number);
        $criteria->compare('value_bool', $this->value_bool);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $className
     * @return ItemParameterValue
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
