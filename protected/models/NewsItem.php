<?php

/**
 * This is the model class for table "news_item".
 *
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $publish_date_time
 * @property integer $author_id
 * @property integer $document_id
 * @property string $title
 *
 * @property Document $document
 * @property User $author
 */
class NewsItem extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title, publish_date_time', 'required'),
			array('author_id, document_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('update_time, publish_date_time', 'safe'),
			array('id, create_time, update_time, publish_date_time, author_id, document_id, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'document' => array(self::BELONGS_TO, 'Document', 'document_id'),
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
			'publish_date_time' => 'Дата публикации',
			'author_id' => 'Автор',
			'document_id' => 'Document',
			'title' => 'Заголовок',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('publish_date_time',$this->publish_date_time,true);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('document_id',$this->document_id);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NewsItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
