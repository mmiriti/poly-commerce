<?php

class OrderItem extends CActiveRecord
{
    public function tableName()
    {
        return 'order_item';
    }

    public function rules()
    {
        return array(
            array('order_id, item_id, price_id, quantity', 'numerical', 'integerOnly' => true),
            array('id, order_id, item_id, price_id, quantity', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'price' => array(self::BELONGS_TO, 'ItemPrice', 'price_id'),
            'item' => array(self::BELONGS_TO, 'CatalogItem', 'item_id'),
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
            'orderItemVariants' => array(self::HAS_MANY, 'OrderItemOptionVariant', 'order_item_id'),
            'variants' => array(self::HAS_MANY, 'ItemOptionVariant', array('option_variant' => 'id'), 'through' => 'orderItemVariants'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'order_id' => 'Order',
            'item_id' => 'Item',
            'price_id' => 'Price',
            'quantity' => 'Quantity',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('order_id', $this->order_id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('price_id', $this->price_id);
        $criteria->compare('quantity', $this->quantity);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
