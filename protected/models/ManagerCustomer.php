<?php

/**
 * This is the model class for table "manager_customer".
 *
 * @property integer $manager_id
 * @property integer $customer_id
 *
 * @property Customer $customer
 * @property User $manager
 */
class ManagerCustomer extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'manager_customer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('manager_id, customer_id', 'numerical', 'integerOnly' => true),
            array('manager_id, customer_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
            'manager' => array(self::BELONGS_TO, 'User', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'manager_id' => 'Manager',
            'customer_id' => 'Customer',
        );
    }

    /**
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('customer_id', $this->customer_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
