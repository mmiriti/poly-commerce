<?php

/**
 * This is the model class for table "structure_block".
 *
 * The followings are the available columns in table 'structure_block':
 * @property integer $id
 * @property integer $item_id
 * @property string $type
 * @property string $title
 * @property string $data
 * @property array $udata
 *
 * The followings are the available model relations:
 * @property StructureItem $item
 */
class StructureBlock extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'structure_block';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('slug', 'unique'),
            array('item_id', 'numerical', 'integerOnly' => true),
            array('type', 'length', 'max' => 7),
            array('title', 'length', 'max' => 255),
            array('data', 'safe'),
            array('id, item_id, type, title, data', 'safe', 'on' => 'search'),
        );
    }

    public function getUdata()
    {
        return $this->data == null ? array() : unserialize($this->data);
    }

    public function setUdata($value)
    {
        $this->data = serialize($value);
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'StructureItem', 'item_id'),
            'galleryItems' => array(self::HAS_MANY, 'StructureBlockGalleryItem', 'block_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'item_id' => 'Item',
            'type' => 'Тип',
            'title' => 'Заголовок',
            'data' => 'Данные',
            'slug' => 'Техническое имя'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('item_id', $this->item_id);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('data', $this->data, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return StructureBlock the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
