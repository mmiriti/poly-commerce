<?php

/**
 * This is the model class for table "options".
 *
 * The followings are the available columns in table 'options':
 * @property integer $id
 * @property string $option
 * @property string $option_name
 * @property string $option_value
 * @property string $option_type
 * @property string $category
 */
class Options extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'options';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('option, option_value', 'required'),
			array('option', 'unique'),
			array('option', 'length', 'max'=>32),
            array('category', 'length', 'max' => 40),
			array('option_name', 'length', 'max'=>255),
            array('editable', 'safe'),
			array('id, option, option_name, option_value, option_type, editable, category', 'safe', 'on'=>'search'),
			);
	}

	public static function get($option, $default = '', $name = null, $type = null)
	{
		$opt = self::model()->findByAttributes(array('option' => $option));
		if($opt == null)
		{
			$opt = new Options('create');
			$opt->option = $option;
			$opt->option_value = $default;
			$opt->option_name = ($name === null ? $option : $name);
			if($type != null)
				$opt->option_type = $type;

			$opt->save();

			return $default;
		}else
		{
			return $opt->option_value;
		}
	}

    public static function set($option, $value)
    {
        $opt = self::model()->findByAttributes(array('option' => $option));
        if($opt == null)
        {
            $opt = new Options('create');
            $opt->option = $option;
        }

        $opt->option_value = $value;
        $opt->save();

        return $opt;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'option' => 'Option',
			'option_name' => 'Option Name',
			'option_value' => 'Option Value',
			'option_type' => 'Option Type',
			);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('option',$this->option,true);
		$criteria->compare('option_name',$this->option_name,true);
		$criteria->compare('option_value',$this->option_value,true);
		$criteria->compare('option_type',$this->option_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Options the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
