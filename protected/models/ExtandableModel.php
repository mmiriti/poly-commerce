<?php

/**
 * @property array extra
 */
class ExtandableModel extends CActiveRecord
{
    private $_extra = null;

    /**
     * Getter for @property $extra
     *
     * @return array|null
     */
    public function getExtra()
    {
        if ($this->_extra == null) {
            $extra_file_name = Yii::getPathOfAlias('application.beyond') . '/extra_fields.php';

            if (file_exists($extra_file_name)) {
                $extra_data = include $extra_file_name;
                if (isset($extra_data[$this->tableName()])) {
                    $this->_extra = $extra_data[$this->tableName()];
                } else {
                    $this->_extra = array();
                }
            }
        }

        return $this->_extra;
    }

    /**
     * @param $rules
     * @return mixed
     */
    public function extendRules($rules)
    {
        if (isset($this->extra['rules'])) {
            $rules = array_merge($rules, $this->extra['rules']);
        }
        return $rules;
    }

    /**
     * @param $labels
     * @return mixed
     */
    public function extendAttributeLabels($labels)
    {
        if (isset($this->extra['attributeLabels'])) {
            $labels = $labels + $this->extra['attributeLabels'];
        }
        return $labels;
    }

    public function editableFields()
    {
        if (isset($this->extra['editable'])) {
            return $this->extra['editable'];
        } else {
            return array();
        }
    }
} 