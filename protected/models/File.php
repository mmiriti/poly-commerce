<?php

/**
 * Class File
 *
 * @property integer $id
 * @property string $create_time
 * @property integer $folder_id
 * @property string $mime
 * @property integer $size
 * @property string $original_name
 * @property string $file_path
 * @property string $url
 * @property string $thumb_url
 */
class File extends CActiveRecord
{
    public static function upload($fieldName)
    {
        $uploadedFile = CUploadedFile::getInstanceByName($fieldName);
        if ($uploadedFile != null) {
            $file_name = time() . '_' . mt_rand(10000, 99999) . '.' . $uploadedFile->extensionName;

            if ($uploadedFile->saveAs(Yii::getPathOfAlias('webroot') . '/upload/' . $file_name)) {
                $file = new File();
                $file->original_name = $uploadedFile->name;
                $file->mime = $uploadedFile->type;
                $file->size = $uploadedFile->size;
                $file->file_path = Yii::app()->params['uploadSrc'] . $file_name;
                $file->url = '/' . $file->file_path;

                if ($file->save()) {
                    return $file;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getThumb_url()
    {
        if ($this->thumb_src != null) {
            return $this->thumb_src;
        } else {
            $this->createThumb();

            if ($this->thumb_src != null) {
                return $this->thumb_src;
            } else {
                return $this->url;
            }
        }
    }

    public function isImage()
    {
        return preg_match("#^image/(.+)$#", $this->mime);
    }

    public function createThumb()
    {
        $file_path = Yii::getPathOfAlias('webroot') . $this->url;

        if (file_exists($file_path)) {
            Yii::import('ext.PHPImageWorkshop.*');
            Yii::setPathOfAlias('PHPImageWorkshop', Yii::getPathOfAlias('ext.PHPImageWorkshop') . '/');

            if ($this->thumb_src != null) {
                @unlink(Yii::getPathOfAlias('webroot') . $this->thumb_src);
            }

            $thumb_width = Options::get('thumb_width', 400, 'Ширина миниатюры');
            $thumb_height = Options::get('thumb_height', 300, 'Высота миниатюры');

            $image = PHPImageWorkshop\ImageWorkshop::initFromPath($file_path);
            $image->resizeInPixel($thumb_width, $thumb_height, true, 0, 0, 'MM');

            $tumb_file_name = 'thumb_' . time() . '_' . mt_rand(10000, 99999) . '.png';

            $image->save(Yii::getPathOfAlias('webroot') . '/upload/', $tumb_file_name, false, null, 80);

            $this->thumb_src = '/upload/' . $tumb_file_name;
            $this->save();
        }
    }


    public function delete()
    {
        @unlink(Yii::getPathOfAlias('webroot') . '/' . $this->file_path);
        parent::delete();
    }

    public function tableName()
    {
        return 'file';
    }

    public function rules()
    {
        return array(
            array('size, original_name', 'required'),
            array('folder_id, size', 'numerical', 'integerOnly' => true),
            array('mime', 'length', 'max' => 64),
            array('thumb_src', 'length', 'max' => 128),
            array('original_name, url, file_path', 'length', 'max' => 255),
            array('id, create_time, folder_id, mime, size, original_name, url, file_path, thumb_src', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'folder' => array(self::BELONGS_TO, 'FileFolder', 'folder_id'),
            'itemGalleryItems' => array(self::HAS_MANY, 'ItemGalleryItem', 'file_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'create_time' => 'Create Time',
            'folder_id' => 'Folder',
            'mime' => 'Mime',
            'size' => 'Size',
            'original_name' => 'Original Name',
            'url' => 'Url',
            'file_path' => 'File Path',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('folder_id', $this->folder_id);
        $criteria->compare('mime', $this->mime, true);
        $criteria->compare('size', $this->size);
        $criteria->compare('original_name', $this->original_name, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('file_path', $this->file_path, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
