<?php

/**
 * This is the model class for table "search_query".
 *
 * The followings are the available columns in table 'search_query':
 * @property integer $id
 * @property string $create_time
 * @property string $index_time
 * @property string $query
 * @property integer $public
 * @property string $options
 *
 * @property SearchQueryResult[] $searchQueryResults
 */
class SearchQuery extends CActiveRecord
{
    public static function runQuery($q, $options = null)
    {
        $q = mb_strtolower($q, 'UTF-8');

        $query = SearchQuery::model()->findByAttributes(array('query' => $q, 'options' => serialize($options)));
        /* @var $query SearchQuery */

        if ($query == null) {
            $query = new SearchQuery();
            $query->query = $q;
            $query->options = $options == null ? null : serialize($options);
            $query->reindex();
        } else {
            if (((time() - strtotime($query->index_time)) > (60 * 60 * 24)) || (count($query->searchQueryResults) == 0)) {
                $query->reindex();
            }
        }

        if ($query != null) {
            $query->count = $query->count + 1;
            $query->save();
        }

        return $query;
    }

    /**
     * Reindex query
     */
    public function reindex()
    {
        if (!$this->isNewRecord) {
            foreach ($this->searchQueryResults as $result) {
                $result->delete();
            }
        }

        $this->index_time = new CDbExpression('NOW()');

        if ($this->save()) {
            $options = $this->options == null ? array(
                'field' => array('name' => 'on')
            ) : unserialize($this->options);

            $query_parts = explode(" ", $this->query);

            $itemCriteria = new CDbCriteria;

            if (isset($options['field'])) {
                foreach ($options['field'] as $field => $set) {
                    foreach ($query_parts as $keyword) {
                        $itemCriteria->compare($field, $keyword, true, 'OR');
                    }
                }
            }

            if (isset($options['param'])) {
                $itemIds = array();
                foreach ($options['param'] as $param_slug => $set) {
                    $parameter = Parameter::model()->findByAttributes(array('slug' => $param_slug));
                    if ($parameter != null) {
                        $valueCriteria = new CDbCriteria();

                        foreach ($query_parts as $keyword) {
                            $valueCriteria->compare('value_bool', $keyword, true, 'OR');
                            $valueCriteria->compare('value_number', $keyword, true, 'OR');
                            $valueCriteria->compare('value_string', $keyword, true, 'OR');
                        }
                        $valueCriteria->compare('parameter_id', $parameter->id, false, 'AND');

                        $values = ItemParameterValue::model()->findAll($valueCriteria);

                        foreach ($values as $value) {
                            $itemIds[] = intval($value->catalog_item_id);
                        }
                    }
                }

                $itemCriteria->addInCondition('id', $itemIds, 'OR');
            }

            $result_items = CatalogItem::model()->findAll($itemCriteria);

            foreach ($result_items as $item) {
                $newResult = new SearchQueryResult;
                $newResult->query_id = $this->id;
                $newResult->catalog_item_id = $item->id;
                $newResult->relevance = 1;
                $newResult->save();
            }
        } else {
            var_dump($this->errors);
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'search_query';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('query', 'required'),
            array('public, count', 'numerical', 'integerOnly' => true),
            array('query', 'length', 'max' => 128),
            array('index_time, options', 'safe'),
            array('id, create_time, index_time, query, public', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'searchQueryResults' => array(self::HAS_MANY, 'SearchQueryResult', 'query_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'create_time' => 'Время первого запроса',
            'index_time' => 'Время последнего индексирования',
            'query' => 'Строка запроса',
            'public' => 'Публичный',
            'count' => 'Количество запросов'
        );
    }

    /**
     * Search
     *
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('index_time', $this->index_time, true);
        $criteria->compare('query', $this->query, true);
        $criteria->compare('public', $this->public);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $className active record class name.
     * @return SearchQuery the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
