<?php

/**
 * Class Parameter
 *
 * @property string $type
 * @property int $classifier_id
 *
 * @property ClassifierGroup $classifier
 */
class Parameter extends CActiveRecord
{
    public static $types = array(
        'string' => 'Строка',
        'number' => 'Число',
        'bool' => 'Логическое значение',
        'classifier' => 'Классификатор',
    );

    public function delete()
    {
        ItemParameterValue::model()->deleteAllByAttributes(array('parameter_id' => $this->id));
        parent::delete();
    }

    public function tableName()
    {
        return 'parameter';
    }

    public function rules()
    {
        return array(
            array('name, type', 'required'),
            array('name', 'length', 'max' => 255),
            array('type', 'length', 'max' => 32),
            array('slug', 'length', 'max' => 16),
            array('slug', 'unique'),
            array('classifier_id', 'numerical', 'integerOnly' => true),
            array('id, name, type, classifier_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'groupParameters' => array(self::HAS_MANY, 'GroupParameter', 'parameter_id'),
            'itemParameterValues' => array(self::HAS_MANY, 'ItemParameterValue', 'parameter_id'),
            'classifier' => array(self::BELONGS_TO, 'ClassifierGroup', 'classifier_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название параметра',
            'slug' => 'Техническое название',
            'type' => 'Тип параметра',
            'classifier_id' => 'Классификатор',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('slug', $this->name, true);
        $criteria->compare('classifier_id', $this->classifier_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param string $className
     * @return Parameter
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
