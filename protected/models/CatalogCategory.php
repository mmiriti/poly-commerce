<?php

/**
 * Class CatalogCategory
 *
 * @property string $name
 *
 * @property CatalogCategory $parent
 * @property CatalogCategory[] $catalogCategories
 * @property CatalogItem[] $items
 * @property float $surcharge
 * @property File $file
 */
class CatalogCategory extends CActiveRecord
{
    public function tableName()
    {
        return 'catalog_category';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'min' => 1),
            array('parent_id, file_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('slug', 'length', 'max' => 32),
            array('update_time, surcharge', 'safe'),
            array('id, create_time, update_time, parent_id, name, slug, surcharge, file_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'parent' => array(self::BELONGS_TO, 'CatalogCategory', 'parent_id'),
            'catalogCategories' => array(self::HAS_MANY, 'CatalogCategory', 'parent_id', 'order' => 'name'),
            'catalogItemCategories' => array(self::HAS_MANY, 'CatalogItemCategory', 'catalog_category_id'),
            'items' => array(self::HAS_MANY, 'CatalogItem', array('catalog_item_id' => 'id'), 'through' => 'catalogItemCategories'),
            'document' => array(self::BELONGS_TO, 'Document', 'document_id'),
            'file' => array(self::BELONGS_TO, 'File', 'file_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'parent_id' => 'Родительская категория',
            'name' => 'Название',
            'slug' => 'Ярылк',
            'surcharge' => 'Наценка',
            'file_id' => 'Файл',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('surcharge', $this->surcharge);
        $criteria->compare('file_id', $this->file_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function breadcrumbs()
    {
        $result = array($this);

        $p = $this->parent;

        while ($p != null) {
            array_unshift($result, $p);
            $p = $p->parent;
        }

        return $result;
    }
}
