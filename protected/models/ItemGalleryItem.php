<?php

/**
 * This is the model class for table "item_gallery_item".
 *
 * The followings are the available columns in table 'item_gallery_item':
 * @property integer $id
 * @property string $create_time
 * @property integer $position
 * @property string $title
 * @property integer $item_id
 * @property integer $file_id
 *
 * The followings are the available model relations:
 * @property File $file
 * @property CatalogItem $item
 */
class ItemGalleryItem extends CActiveRecord
{
	public function tableName()
	{
		return 'item_gallery_item';
	}

	public function rules()
	{
		return array(
			array('position, item_id, file_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('id, create_time, position, title, item_id, file_id', 'safe', 'on'=>'search'),
		);
	}

	public function delete()
	{		
		parent::delete();
		$this->file->delete();
	}

	public function relations()
	{
		return array(
			'file' => array(self::BELONGS_TO, 'File', 'file_id'),
			'item' => array(self::BELONGS_TO, 'CatalogItem', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Create Time',
			'position' => 'Position',
			'title' => 'Title',
			'item_id' => 'Item',
			'file_id' => 'File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('file_id',$this->file_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemGalleryItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
