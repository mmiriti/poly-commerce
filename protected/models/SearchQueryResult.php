<?php

/**
 * This is the model class for table "search_query_result".
 *
 * The followings are the available columns in table 'search_query_result':
 * @property integer $id
 * @property integer $query_id
 * @property integer $catalog_item_id
 * @property double $relevance
 *
 * The followings are the available model relations:
 * @property CatalogItem $catalogItem
 * @property SearchQuery $query
 */
class SearchQueryResult extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'search_query_result';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('query_id, catalog_item_id', 'required'),
			array('query_id, catalog_item_id', 'numerical', 'integerOnly'=>true),
			array('relevance', 'numerical'),
			array('id, query_id, catalog_item_id, relevance', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catalogItem' => array(self::BELONGS_TO, 'CatalogItem', 'catalog_item_id'),
			'query' => array(self::BELONGS_TO, 'SearchQuery', 'query_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'query_id' => 'Query',
			'catalog_item_id' => 'Catalog Item',
			'relevance' => 'Relevance',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('query_id',$this->query_id);
		$criteria->compare('catalog_item_id',$this->catalog_item_id);
		$criteria->compare('relevance',$this->relevance);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SearchQueryResult the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
