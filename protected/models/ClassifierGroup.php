<?php

/**
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string $description
 *
 * @property Classifier[] $classifiers
 */
class ClassifierGroup extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'classifier_group';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, slug', 'required'),
            array('slug', 'unique'),
            array('name', 'length', 'max' => 255),
            array('description', 'safe'),
            array('id, name, description', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'classifiers' => array(self::HAS_MANY, 'Classifier', 'group_id', 'condition' => 'parent_id is null'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'slug' => 'Техническое имя',
            'name' => 'Название классификатора',
            'description' => 'Описание',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('slug', $this->slug, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
