<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 7/8/14
 * Time: 1:43 PM
 */
class NewsController extends Controller
{
    public function actionRead($id)
    {
        $newsItem = NewsItem::model()->findByPk($id);
        if ($newsItem != null) {
            $this->render('read', array(
                'item' => $newsItem
            ));
        } else {
            throw new CHttpException(404, 'Не найдено');
        }
    }
} 