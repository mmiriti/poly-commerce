<?php

class CatalogController extends Controller
{
    private function fillBreadcrumbs($start)
    {
        if ($start != null) {
            $c = $start;

            do {
                $this->breadcrumbs[$c->name] = array('catalog/index', 'category_id' => $c->id);
                $c = $c->parent;
            } while ($c != null);

            $this->breadcrumbs = array_reverse($this->breadcrumbs);
        }
    }

    public function actionIndex($category_id = null, $page = 0, $itemsPerPage = 25)
    {
        $criteria = new CDbCriteria(array(
            'limit' => $itemsPerPage,
        ));

        if ($category_id != null) {
            $category = CatalogCategory::model()->with('items')->findByPk($category_id);
            if ($category != null) {
                $this->breadcrumbs = array($category->name);
                $this->fillBreadcrumbs($category->parent);

                $catalogItems = CatalogItemCategory::model()->findAllByAttributes(array('catalog_category_id' => $category->id));

                $ids = array();

                foreach ($catalogItems as $i) {
                    $ids[] = $i->catalog_item_id;
                }

                $criteria->addInCondition('id', $ids);
            } else {
                throw new CHttpException(404, "Категория не найдена");
            }
        } else {
            $category = new CatalogCategory;
            $category->name = 'Все товары';
            $this->breadcrumbs = array($category->name);
        }

        $items = new CActiveDataProvider(CatalogItem::model(), array(
            'criteria' => $criteria
        ));

        $items->pagination->pageSize = $itemsPerPage;
        $items->pagination->currentPage = $page;

        $this->render('index', array(
            'category_id' => $category_id,
            'category' => $category,
            'items' => $items,
            'page' => $page,
            'itemsPerPage' => $itemsPerPage,
        ));
    }

    /**
     * Catalog item
     *
     * @param $id
     * @param $parent
     * @throws CHttpException
     */
    public function actionItem($id, $parent = null)
    {
        if ($parent != null) {
            $catalogCategory = CatalogCategory::model()->findByPk($parent);
        } else {
            $catalogCategory = new CatalogCategory();
        }

        $catalogItem = CatalogItem::model()->findByPk($id);

        $this->breadcrumbs = array($catalogItem->name);

        $this->fillBreadcrumbs($catalogCategory);

        if ($catalogItem != null) {
            $this->render('item', array('item' => $catalogItem, 'category' => $catalogCategory));
        } else {
            throw new CHttpException(404, "Товар не найден");
        }
    }
}