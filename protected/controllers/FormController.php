<?php

class FormController extends Controller
{
    public function actionPost()
    {
        if (isset($_POST['FormField'])) {
            foreach ($_POST['FormField'] as $form_slug => $formFields) {
                $form = Form::model()->findByAttributes(array('slug' => $form_slug));

                if ($form != null) {
                    $fill = new FormFill();
                    $fill->form_id = $form->id;

                    if ($fill->save()) {
                        foreach ($formFields as $fieldSlug => $value) {
                            $formField = FormField::model()->findByAttributes(array('slug' => $fieldSlug, 'form_id' => $form->id));
                            if ($formField != null) {
                                $formFieldValue = new FormFillValue();
                                $formFieldValue->field_id = $formField->id;
                                $formFieldValue->fill_id = $fill->id;
                                $formFieldValue->value = $value;
                                $formFieldValue->save();
                            }
                        }
                        $this->render('post', array('form' => $form));
                    } else {
                        throw new CHttpException(500, print_r($fill->getErrors(), true));
                    }
                } else {
                    throw new CHttpException(500, 'Форма не найдена');
                }
            }
        } else {
            throw new CHttpException(404, 'Страница не найдена');
        }
    }
}