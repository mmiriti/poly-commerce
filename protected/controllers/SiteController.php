<?php

class SiteController extends Controller
{
    public $layout = '//layouts/index';
    public $defaultAction = 'structure';

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionStructure($slug = 'index')
    {
        if (!isset($_GET['slug'])) {
            $_GET['slug'] = $slug;
        }
        $structure = StructureItem::model()->findByAttributes(array('slug' => $slug));

        if ($structure->layout != null) {
            if (isset(Theme::current()->info['layouts'][$structure->layout])) {
                $this->layout = '//layouts/' . $structure->layout;
            }
        }

        if ($structure != null) {
            $this->render('structure', array(
                'structure' => $structure
            ));
        } else {
            throw new CHttpException(404, 'Страница не найдена');
        }
    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}
