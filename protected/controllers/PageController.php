<?php

class PageController extends Controller
{
	public function actionIndex($slug)
	{
		$page = Page::model()->findByAttributes(array('slug' => $slug));
		$rootPages = Page::model()->findAllByAttributes(array('page_id' => null));

		if($page != null)
		{
			$this->render('index', array(
				'page' => $page,
				'rootPages' => $rootPages,
				)
			);
		} else 
		{
			throw new CHttpException(404, 'Страница не найдена');
		}
	}
}