<?php

/**
 * Class CartController
 */
class CartController extends Controller
{
    /**
     * Index
     */
    public function actionIndex()
    {
        $items = array();

        if (isset(Yii::app()->session['cart']['items'])) {
            foreach (Yii::app()->session['cart']['items'] as $cart_item) {
                $item = CatalogItem::model()->findByPk($cart_item['item_id']);
                if ($item != null) {
                    $view_item = array('item' => $item, 'qty' => $cart_item['qty'], 'variants' => array());

                    if ((isset($cart_item['variants'])) && (is_array($cart_item['variants']))) {
                        foreach ($cart_item['variants'] as $variant_id) {
                            $variant = ItemOptionVariant::model()->findByPk($variant_id);
                            $view_item['variants'][] = $variant;
                        }
                    }

                    $items[] = $view_item;
                }
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('_table', array(
                    'items' => $items
                )
            );
        } else {
            $this->render('index', array(
                    'items' => $items
                )
            );
        }
    }

    /**
     * Checkout cart
     *
     * @throws CHttpException
     */
    public function actionCheckout()
    {
        $user = null;

        if (Yii::app()->user->isGuest) {

            if (isset($_POST['User'])) {
                $user = User::model()->findByAttributes(array('email' => $_POST['User']['email']));
                if ($user == null) {
                    $user = new User;
                    $user->attributes = $_POST['User'];
                    $user->save();
                }
            }
        } else {
            $user = User::model()->findByPk(Yii::app()->user->id);
        }

        if ($user != null) {
            $customer = Customer::model()->findByAttributes(array('user_id' => $user->id));
        } else {
            $customer = null;
        }

        if ($customer == null) {
            $customer = new Customer;

            if (($user != null) && ($user->id)) {
                $customer->user_id = $user->id;
            }

            $customer->save();
        }

        if (($customer != null) && ($customer->id)) {
            if (isset($_POST['CustomerData'])) {
                foreach ($_POST['CustomerData'] as $slug => $value) {
                    $customer->setField($slug, $value);
                }
            }
        }

        if (($customer != null) && ($customer->id)) {
            $default_status = OrderStatus::model()->findByAttributes(array('default' => 1));

            if ($default_status != null) {
                $order = new Order;
                $order->customer_id = $customer->id;
                $order->status_id = $default_status->id;
                if ($order->save()) {
                    if (isset(Yii::app()->session['cart']['items'])) {
                        foreach (Yii::app()->session['cart']['items'] as $itemData) {
                            $item = CatalogItem::model()->findByPk($itemData['item_id']);

                            $orderItem = new OrderItem;
                            $orderItem->order_id = $order->id;
                            $orderItem->item_id = $item->id;
                            $orderItem->price_id = $item->price->id;
                            $orderItem->quantity = $itemData['qty'];
                            if ($orderItem->save()) {
                                if ((isset($itemData['variants'])) && (is_array($itemData['variants']))) {
                                    foreach ($itemData['variants'] as $variant_id) {
                                        $orderItemVariant = new OrderItemOptionVariant();
                                        $orderItemVariant->order_item_id = $orderItem->id;
                                        $orderItemVariant->option_variant = $variant_id;
                                        $orderItemVariant->save();
                                    }
                                }
                            }
                        }
                    }

                    if (isset($_POST['OrderData'])) {
                        foreach ($_POST['OrderData'] as $slug => $value) {
                            $order->setField($slug, $value);
                        }
                    }

                    Yii::app()->session['cart'] = array('items' => array());
                } else {

                }
            }
            $this->render('checkout');
        } else {
            throw new CHttpException(500, 'Не удалось распознать клиента');
        }
    }

    /**
     * Add item to cart
     *
     * @param $item_id
     * @param $qty
     */
    public function actionAdd($item_id, $qty)
    {
        $variants = Yii::app()->request->getPost('variants', null);

        if (isset(Yii::app()->session['cart']['items'])) {
            $cartItems = Yii::app()->session['cart']['items'];
        } else {
            $cartItems = array();
        }

        $cartItems[] = array(
            'item_id' => $item_id,
            'qty' => $qty,
            'variants' => $variants
        );

        Yii::app()->session['cart'] = array('items' => $cartItems);

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array('result' => 'success', 'variants' => $variants));
        } else {
            $this->redirect(array('index'));
        }
    }

    /**
     * Inc quantity of the item
     *
     * @param $item_id
     * @param int $qty
     */
    public function actionInc($item_id, $qty = 1)
    {
        if (isset(Yii::app()->session['cart']['items'])) {
            $items = Yii::app()->session['cart']['items'];

            foreach ($items as &$item) {
                if ($item['item_id'] == $item_id) {
                    $item['qty'] += $qty;
                    break;
                }
            }

            Yii::app()->session['cart'] = array('items' => $items);
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array('result' => 'success'));
        } else {
            $this->redirect(array('index'));
        }
    }

    /**
     * Decrese item quantity
     *
     * @param $item_id
     * @param int $qty
     */
    public function actionDec($item_id, $qty = 1)
    {
        if (isset(Yii::app()->session['cart']['items'])) {
            $items = Yii::app()->session['cart']['items'];

            for ($i = count($items) - 1; $i >= 0; $i--) {
                if ($items[$i]['item_id'] == $item_id) {
                    $items[$i]['qty'] -= $qty;

                    if ($items[$i]['qty'] <= 0) {
                        unset($items[$i]);
                    }
                    break;
                }
            }

            Yii::app()->session['cart'] = array('items' => array_values($items));
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array('result' => 'success'));
        } else {
            $this->redirect(array('index'));
        }
    }

    /**
     * Set quantity
     *
     * @param $item_id
     * @param $qty
     */
    public function actionSetQty($item_id, $qty)
    {
        if (isset(Yii::app()->session['cart']['items'])) {
            $items = Yii::app()->session['cart']['items'];

            for ($i = count($items) - 1; $i >= 0; $i--) {
                if ($items[$i]['item_id'] == $item_id) {
                    $items[$i]['qty'] = $qty;

                    if ($items[$i]['qty'] <= 0) {
                        unset($items[$i]);
                    }
                    break;
                }
            }

            Yii::app()->session['cart'] = array('items' => array_values($items));
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array('result' => 'success'));
        } else {
            $this->redirect(array('index'));
        }
    }

    /**
     * Remove item form the cart
     *
     * @param $item_id
     */
    public function actionRemove($item_id)
    {
        if (isset(Yii::app()->session['cart']['items'])) {
            $items = Yii::app()->session['cart']['items'];

            for ($i = count($items) - 1; $i >= 0; $i--) {
                if ($items[$i]['item_id'] == $item_id) {
                    unset($items[$i]);
                }
            }

            Yii::app()->session['cart'] = array('items' => array_values($items));
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array('result' => 'success'));
        } else {
            $this->redirect(array('index'));
        }
    }

    /**
     * Proxy action to display cart widget
     */
    public function actionWidget()
    {
        $this->renderPartial('widget');
    }
}