<?php

class SearchController extends Controller
{
    public function actionIndex($q = null, $query_id = null, $itemsPerPage = 25, $page = 0)
    {
        if ($q != null) {
            if (trim($q) != "") {
                if (isset($_GET['options'])) {
                    $options = $_GET['options'];
                } else {
                    $options = null;
                }

                $query = SearchQuery::runQuery($q, $options);
            } else {
                throw new CHttpException(400, 'Неправильный запрос');
            }
        } else {
            $query = SearchQuery::model()->findByPk($query_id);
        }

        if ($query != null) {
            if (!Yii::app()->user->isGuest) {
                $users_search = new SearchQueryUser();
                $users_search->user_id = User::current()->id;
                $users_search->query_id = $query->id;
                $users_search->save();
            }

            $ids = array();

            foreach ($query->searchQueryResults as $result) {
                $ids[] = $result->catalog_item_id;
            }

            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $ids);

            $items = new CActiveDataProvider(CatalogItem::model(), array(
                'criteria' => $criteria,
            ));

            $items->pagination->pageSize = $itemsPerPage;
            $items->pagination->currentPage = $page;

            $this->render('index', array(
                    'query' => $query,
                    'items' => $items,
                    'page' => $page,
                    'itemsPerPage' => $itemsPerPage
                )
            );
        } else {
            throw new CHttpException(500, 'Произошла ошибка.');
        }
    }
}